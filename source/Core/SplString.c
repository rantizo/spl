// SplString.c -- String functions
// 
// Standardized Platform Layer
//

#include "SplString.h"
#include "SplExport.h"
#include "SplErrors.h"


// Extensions to standard strstr()

// Find string specifying length of second string
SOEXPORT
char *SplStrnStr(const char *_pszStr,const char *_pszPat, size_t _cPatMax)
{
	size_t nLenSrc = strlen(_pszStr);
	size_t nLenPat = strnlen(_pszPat,_cPatMax);

	for (size_t cSrc=0; *_pszStr && cSrc < nLenSrc && nLenPat <= nLenSrc-cSrc; cSrc++,_pszStr++)
		if (strncmp(_pszStr,_pszPat,nLenPat) == 0)
			return (char*)_pszStr;

	return NULL;
}

// Find string specifying length of first string
SOEXPORT
char *SplStrStrn(const char *_pszStr, size_t _cStrMax, const char *_pszPat)
{
	size_t nLenSrc = strnlen(_pszStr,_cStrMax);
	size_t nLenPat = strlen(_pszPat);

	for (size_t cSrc=0; *_pszStr && cSrc < nLenSrc && nLenPat <= nLenSrc-cSrc; cSrc++,_pszStr++)
		if (strncmp(_pszStr,_pszPat,nLenPat) == 0)
			return (char*)_pszStr;

	return NULL;
}

// Find string specifying length of both strings
SOEXPORT
char *SplStrnStrn(const char *_pszStr, size_t _cStrMax, const char *_pszPat, size_t _cPatMax)
{
	size_t nLenSrc = strnlen(_pszStr,_cStrMax);
	size_t nLenPat = strnlen(_pszPat,_cPatMax);

	for (size_t cSrc=0; *_pszStr && cSrc < nLenSrc && nLenPat <= nLenSrc-cSrc; cSrc++,_pszStr++)
		if (strncmp(_pszStr,_pszPat,nLenPat) == 0)
			return (char*)_pszStr;

	return NULL;
}

// SplItoA -- Convert integer to character string
//
// Replacement for _itoa() which is not available on all platforms
//
size_t SplItoA(
long _nVal,         // Number to convert
char *_pszDst,   	// -> result string
int _nBase  		// Number Base for conversion
)
{
    size_t nLen = 1;

    if (_nVal < (long) SPL_ABS(_nBase)) {

    	if (_nBase == -10 && _nVal < 0) {
    		*_pszDst++ = '-';
    		++nLen;
    	}

        if (_nVal < 10)
            *_pszDst++ = (char) _nVal + '0';
        else
            *_pszDst++ = (char) _nVal + 87;

        *_pszDst = '\0';

        return nLen;
    }

    nLen = SplItoA(_nVal / (long) _nBase, _pszDst, _nBase);

    SplItoA(_nVal % _nBase, _pszDst, _nBase);

    return nLen + 1;
}

#if 0
// SplNtoA -- Convert integer to character string
//
// Convert unsigned long integer to ASCII in specified buffer.  The buffer
// pointer is updated to point one past the last character placed in the
// buffer.  The Function value returns the number of characters placed in
// the buffer.  The string is NOT terminated with an EOS character.
//
uint32_t SplNtoA(
long _nVal,         // Number to convert
char *_pszDst,   	// -> Addr of string buffer
uint32_t _nBase,  	// Number Base for conversion
size_t _nWidth, 	 // Field width for lead zeros
bool _bCaps      	// Use upper case for hex
)
{
    size_t nLen;

    if (_nVal < (long) _nBase) {
    	// Pre-fill entire field with zeros
        for (nLen = 1; nLen < _nWidth; nLen++)  /* Insert leading zeros */
            *_pszDst++ = '0';

        if (_nVal < 10)
            *_pszDst++ = (char) _nVal + '0';
        else
            *_pszDst++ = (char) _nVal + (_bCaps ? 55 : 87);

        return nLen;
    }

    _nWidth = (_nWidth==0) ? 0 : _nWidth-1;

    nLen = SplNtoA(_nVal / (long) _nBase, pStr, _nBase, _nWidth, _bCaps);

    SplNtoA(_nVal % _nBase, pStr, _nBase, 1, _bCaps);

    return nLen + 1;
}

#endif
