// SplMutex..c --  Mutex related functions
// 
// Standardized Platform Layer
//

#include "SplTypes.h"
#include "SplExport.h"
#include "SplErrors.h"


// SplMutexCreate -- Create a Mutext
//
SOEXPORT
SplErc SplMutexCreate( SplMutex *mutex )
{
#if defined (_WIN32)
	SplErc retVal = SplErc_Failure;

	// Create an unnamed mutex
	return (*mutex = CreateMutex(NULL, FALSE, NULL)) != NULL ? SplErc_OK : SplErc_Failure;

#elif defined (_LINUX) || defined (__APPLE__)
	// Set as a recursive mutex, allowing a single thread to multiply lock
	// This will require the equivalent number of unlocks to release
	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);

	return (pthread_mutex_init((pthread_mutex_t*)mutex, &mutex_attr) ? SplErrorFailure : SplErrorOk);

#elif defined(ESP_PLATFORM)
	return ((*mutex = xSemaphoreCreateRecursiveMutex()) != NULL) ? SplErc_OK : SplErc_Failure;
	
#endif
}


// SplMutexDelete -- Delete a Mutext
//
SOEXPORT
SplErc SplMutexDelete(SplMutex *mutex)
{
	SplErc retVal = SplErc_Failure;

#if defined (_WIN32)
	retVal = (CloseHandle(*mutex) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX) || defined (__APPLE__)
	// pthreads require the mutex to be unlocked before
	// destroying, other platforms don't seem to
	// We can continually try to unlock which we will try here
	// although it could just be that it is in use in a pthread_cond_timedwait() or pthread_cond_wait()
	int result;
	while ((result = pthread_mutex_destroy((pthread_mutex_t*)mutex)) == EBUSY) {
		pthread_mutex_unlock((pthread_mutex_t*)mutex);
	}
	retVal = (result ? SplErrorFailure : SplErrorOk);

#elif defined(ESP_PLATFORM)
	vSemaphoreDelete(*mutex);
	retVal = SplErc_OK;

#endif
	return retVal;
}

#if 0	// ESP32 -- RecursiveMutex not supported from ISR

// SplMutexLockFromISR -- Lock a mutex from an ISR
//
// If timeout_ms is SPL_INFINITE, block indefinitely
//
SOEXPORT
SplResult SplMutexLockFromISR(SplMutex *mutex,uint32_t timeout_ms)
{
	SplResult result = SplLockFailed;

#if defined (_WIN32)
	result = (SplResult)WaitForSingleObject(*mutex, (timeout_ms != SPL_INFINITE) ? timeout_ms : INFINITE);

#elif defined(_LINUX) || defined(__APPLE__)

	// JDD TODO, verify this isn't being used and remove it from the spec.  It complicates
	// the spec when cross platform consistency is required, and the timeout functionality
	// is more suited to a SplWait* api.
	if (timeout_ms) {

#if defined (_LINUX)
		struct timespec timeout;
		if (clock_gettime(CLOCK_REALTIME, &timeout) == -1) {
			return SplLockFailed;
		}

#elif defined (__APPLE__)
		struct timespec timeout;
		struct timeval tv;
		if (gettimeofday(&tv, NULL) == -1) {
			return SplLockFailed;
		}
		// Convert timeval to timespec
		timeout.tv_sec = tv.tv_sec;
		timeout.tv_nsec = tv.tv_usec * 1000;

#endif // __APPLE__

		// Convert ms wait to nanoseconds
		timeout.tv_sec += (timeout_ms / 1000);
		timeout.tv_nsec += ((timeout_ms % 1000) * 1000000);
		if (timeout.tv_nsec > 999999999) {
			timeout.tv_sec++;
			timeout.tv_nsec -= 1000000000;
		}

		// Perform the lock.
		int result = SplLockFailed;

#ifdef __APPLE__
		// OSX doesn't support timedlock
		result = pthread_mutex_lock((pthread_mutex_t*)mutex);

#elif _ARCH_ARM
		// The ARM library we are using doesn't support timed operations
		result = pthread_mutex_lock(mutex);

#else
		// Vanilla linux
		result = pthread_mutex_timedlock(mutex, &timeout);

#endif

		if (result) {
			// Unsuccessful
			if (errno == ETIMEDOUT) {
				// Unable to acquire lock before timeout expired
				result = SplLockTimeout;
			}
			else {
				result = SplLockFailed;
			}
		}
		else {
			result = SplLockAcquired;
		}
	}
	else {
		result = (pthread_mutex_lock((pthread_mutex_t*)mutex) ? SplLockFailed : SplLockAcquired);
	}

#elif defined(ESP_PLATFORM)
	result = (xSemaphoreTakeFromISR(*mutex, SPL_INFINITE) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

	return result;
}
#endif


// SplMutexLock -- Lock a mutex (Not from an ISR)
//
// If timeout_ms is SPL_INFINITE, block indefinitely
//
SOEXPORT
SplResult SplMutexLock( SplMutex *mutex, uint32_t timeout_ms )
{
  SplResult result = SplLockFailed;

#if defined (_WIN32)
	result = (SplResult)WaitForSingleObject(*mutex, (timeout_ms != SPL_INFINITE) ? timeout_ms : INFINITE);

#elif defined(_LINUX) || defined(__APPLE__)
  
  // JDD TODO, verify this isn't being used and remove it from the spec.  It complicates
  // the spec when cross platform consistency is required, and the timeout functionality
  // is more suited to a SplWait* api.
	if (timeout_ms) {

#if defined (_LINUX)
    struct timespec timeout;
		if (clock_gettime(CLOCK_REALTIME, &timeout) == -1) {
      return SplLockFailed;
		}

#elif defined (__APPLE__)
    struct timespec timeout;
    struct timeval tv;
		if (gettimeofday(&tv, NULL) == -1) {
			return SplLockFailed;
		}
		// Convert timeval to timespec
		timeout.tv_sec = tv.tv_sec;
		timeout.tv_nsec = tv.tv_usec * 1000;

#endif // __APPLE__
    
		// Convert ms wait to nanoseconds
		timeout.tv_sec += (timeout_ms / 1000);
		timeout.tv_nsec += ((timeout_ms % 1000) * 1000000);
		if (timeout.tv_nsec > 999999999) {
			timeout.tv_sec++;
			timeout.tv_nsec -= 1000000000;
		}
    
    // Perform the lock.
    int result = SplLockFailed;

#ifdef __APPLE__
    // OSX doesn't support timedlock
		result = pthread_mutex_lock((pthread_mutex_t*)mutex);

#elif _ARCH_ARM
		// The ARM library we are using doesn't support timed operations
		result = pthread_mutex_lock(mutex);

#else
    // Vanilla linux
		result = pthread_mutex_timedlock(mutex, &timeout);

#endif
    
    if(result) {
			// Unsuccessful
			if (errno == ETIMEDOUT) {
				// Unable to acquire lock before timeout expired
				result = SplLockTimeout;
			} else {
				result = SplLockFailed;
			}
		} else {
			result = SplLockAcquired;
		}
	} else {
		result = (pthread_mutex_lock((pthread_mutex_t*)mutex) ? SplLockFailed : SplLockAcquired);
	}

#elif defined(ESP_PLATFORM)
	result = (xSemaphoreTakeRecursive(*mutex,SPL_INFINITE) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

  return result;
}


// SplMutexTryLock -- Try to lock a mutex (Not from an ISR)
//
// Returns immediately
//
SOEXPORT
SplResult SplMutexTryLock( SplMutex *mutex )
{
	SplResult result;

#if defined (_WIN32)
	// SplResult directly maps to wait results
	result = WaitForSingleObject(*mutex, 0) == WAIT_OBJECT_0 ? SplLockAcquired : SplLockFailed;

#elif defined (_LINUX) || defined (__APPLE__)
	result = (pthread_mutex_trylock((pthread_mutex_t*)mutex) ? SplLockFailed : SplLockAcquired);

#elif defined(ESP_PLATFORM)
	result = (xSemaphoreTakeRecursive(*mutex,0) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

	return result;
}

#if 0	// ESP32 -- RecursiveMutex not supported from ISR

// SplMutexTryLockFromISR -- Try to lock a mutex from an ISR
//
// Returns immediately
//
SOEXPORT
SplResult SplMutexTryLock(SplMutex *mutex)
{
	SplResult result;

#if defined (_WIN32)
	// SplResult directly maps to wait results
	result = WaitForSingleObject(*mutex, 0) == WAIT_OBJECT_0 ? SplLockAcquired : SplLockFailed;

#elif defined (_LINUX) || defined (__APPLE__)
	result = (pthread_mutex_trylock((pthread_mutex_t*)mutex) ? SplLockFailed : SplLockAcquired);

#elif defined(ESP_PLATFORM)
	result = (xSemaphoreTakeFromISR(*mutex, 0) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

	return result;
}


// SplMutexUnlockFromISR -- Unlock a Mutex from an ISR
//
// NOTE: There maybe scheduling differences between platforms when a mutex is unlocked and there
// is another thread of the same priority waiting on the same mutex. For example, on the ESP32
// platform the thread releasing the lock will not yeild to the waiting thread.
//
SOEXPORT
SplErc SplMutexUnlock( SplMutex *mutex )
{
	SplErc retVal = SplErc_Failure;

#if defined (_WIN32)
	retVal = (ReleaseMutex(*mutex) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX) || defined (__APPLE__)
	retVal = (pthread_mutex_unlock((pthread_mutex_t*)mutex) ? SplErrorFailure : SplErrorOk);

#elif defined(ESP_PLATFORM)
	retVal = (xSemaphoreGiveFromISR(*mutex) == pdPASS) ? SplErc_OK : SplErc_Failure;
#endif
	return retVal;
}
#endif

// SplMutexUnlock -- Unlock a Mutext (Not from an ISR)
//
// NOTE: There maybe scheduling differences between platforms when a mutex is unlocked and there
// is another thread of the same priority waiting on the same mutex. For example, on the ESP32
// platform the thread releasing the lock will not yeild to the waiting thread.
//
SOEXPORT
SplErc SplMutexUnlock(SplMutex *mutex)
{
	SplErc retVal = SplErc_Failure;

#if defined (_WIN32)
	retVal = (ReleaseMutex(*mutex) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX) || defined (__APPLE__)
	retVal = (pthread_mutex_unlock((pthread_mutex_t*)mutex) ? SplErrorFailure : SplErrorOk);

#elif defined(ESP_PLATFORM)
	retVal = (xSemaphoreGiveRecursive(*mutex) == pdPASS) ? SplErc_OK : SplErc_Failure;
	// This is not the best fix since it will yield even if no thread is waiting on the same mutex
	// but it does simulate the behavior on Windows.
	taskYIELD();
#endif
	return retVal;
}

