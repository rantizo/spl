// SplSocket.c -- IP sockets
// 
// Standardized Platform Layer
//
__asm__(".symver fcntl,fcntl@GLIBC_2.4");

#include "SplSocket.h"
#include "SplErrors.h"

// SplSocketSetLastError
//
SOEXPORT
SplErc SplSocketSetLastError(SplErc _erc)
{
#if defined(_WIN32)
	WSASetLastError(_erc);

#elif defined(_LINUX) || defined(__APPLE__) || defined(SPL_PLATFORM)
	errno = _erc;

#endif
	return SPL_SOCKET_ERROR;
}

// SplSocketInit
//
SOEXPORT
SplErc SplSocketInit(  )
{
#if defined(_WIN32)
	WSADATA wsaData;
	if (WSAStartup(0x0202, &wsaData) != 0)
		return SplErc_Failure;

#elif defined(_LINUX) || defined(__APPLE__)
	// Nothing to do here...
#endif
	return SplErc_OK;
}


// SplSocketCleanup
//
SOEXPORT
SplErrorCode SplSocketCleanup(  )
{
#if defined(_WIN32)
	WSACleanup();

#elif defined(_LINUX) || defined(__APPLE__)
	// Nothing to do here...
#endif
	return SplErc_OK;
}


// SplSocketCreate
//
SOEXPORT
SplErc SplSocketCreate(SplSocket *_pSocketRet, int32_t _family, int32_t _type, int32_t _protocol)
{
	*_pSocketRet = socket(_family, _type, _protocol); 

	if (*_pSocketRet == SPL_INVALID_SOCKET) 
		return SplGetLastSocketError();

	return SplErc_OK;
}


// SplSocketSetNonBlocking
//
SOEXPORT
SplErc SplSocketSetNonBlocking( SplSocket _socket, BOOL _bNoBlocking )
{
#if defined(_WIN32)
	//-------------------------
	// Set the socket I/O mode: In this case FIONBIO
	// enables or disables the blocking mode for the 
	// socket based on the numerical value of iMode.
	// If iMode = 0, blocking is enabled; 
	// If iMode != 0, non-blocking mode is enabled.
	u_long iMode = !!_bNoBlocking;
	if (ioctlsocket(_socket, FIONBIO, &iMode) == SOCKET_ERROR) 
		return SplGetLastSocketError();

#elif defined(_LINUX) || defined(__APPLE__)  || defined(ESP_PLATFORM)
	fcntl(_socket, F_SETFL, O_NONBLOCK);

#endif
	return SplErc_OK;
}


// SplSocketBind
//
SOEXPORT
SplErc SplSocketBind(SplSocket _socket, const struct sockaddr *_paddr, int32_t _addrlen )
{
	if (bind(_socket, _paddr, _addrlen) == SPL_SOCKET_ERROR) 
		return SplGetLastSocketError();

	return SplErc_OK;
}


// SplSocketConnect
//
SOEXPORT
SplErc SplSocketConnect( SplSocket _socket, const struct sockaddr *_paddr, int32_t _addrlen )
{
    if (connect(_socket, _paddr, _addrlen) == SPL_SOCKET_ERROR)
		return SplGetLastSocketError();

	return SplErc_OK;
}


// SplSocketShutdown
//
SOEXPORT
SplErc SplSocketShutdown( SplSocket _socket, int32_t _how )
{
    if (shutdown(_socket, _how) == SPL_SOCKET_ERROR)
		return SplGetLastSocketError();

	return SplErc_OK;

}


// SplSocketListen
//
SOEXPORT
SplErc SplSocketListen( SplSocket _socket, int32_t _backlog )
{
    if (listen(_socket, _backlog) == SPL_SOCKET_ERROR)
		return SplGetLastSocketError();

	return SplErc_OK;
}


// SplSocketAccept
//
SOEXPORT
SplErc SplSocketAccept( SplSocket *_pSocketRet, SplSocket _listeningSock, struct sockaddr* _paddr, socklen_t* _addrlen )
{
	*_pSocketRet = accept(_listeningSock, _paddr, _addrlen);
	if (*_pSocketRet == SPL_INVALID_SOCKET)
		return SplGetLastSocketError();

	return SplErc_OK;
}


// SplSocketDontFragment
//
void SplSocketDontFragment(SplSocket _socket, BOOL _bNoFrag) 
{
#if defined(_WIN32) || defined(_LINUX) 
	int32_t optName;
	DWORD bDontFragment;
#endif
    
#if defined(_WIN32)
	optName = IP_DONTFRAGMENT;
	if (_bNoFrag)
		bDontFragment = 1;
	else
		bDontFragment = 0;

#elif defined (__APPLE__)
	// Mac OSX doesn't support this option

#elif defined (_LINUX)
	optName = IP_MTU_DISCOVER;
	if (_bNoFrag)
		bDontFragment = IP_PMTUDISC_DO;
	else
		bDontFragment = IP_PMTUDISC_DONT;

#elif  defined(ESP_PLATFORM)
	// ESP32 doesn't support this option

#endif

#if defined(_WIN32) || defined(_LINUX)
	SplSocketSetOpt(_socket, IPPROTO_IP, optName, (void*)&bDontFragment, sizeof(DWORD));

#endif
}


// SplSocketSend
//
SOEXPORT
SplResult SplSocketSend( SplSocket _socket, const void *_pBuffer, uint32_t _len, int32_t _flags)
{
	SplResult retVal;

	// Only supported send flag at this time is don't fragment
	if ((_flags != 0) && (_flags != SplSocketSendFlagNoFrag))
 		return SplSocketSetLastError(SplErc_SocketInvalidArguments);
 
	if (_flags & SplSocketSendFlagNoFrag)
		SplSocketDontFragment(_socket, 1);		// Set don't fragment flag on socket

	retVal = send(_socket, (SplSocketSendBuffer*)_pBuffer, _len, 0);

	if (_flags & SplSocketSendFlagNoFrag)
		SplSocketDontFragment(_socket, 0);		// Clear don't fragment flag on socket

	return retVal;
}


// SplSocketRecv
//
SOEXPORT
SplResult SplSocketRecv( SplSocket _socket, void *_pBuffer, uint32_t _len , int32_t _flags)
{
	// Flags are currently not supported
	if (_flags != 0)
		return SplSocketSetLastError(SplErc_SocketInvalidArguments);

	return recv(_socket, (SplSocketRecvBuffer*)_pBuffer, _len, 0);
}


// SplSocketSendTo
//
SOEXPORT
SplResult SplSocketSendTo( SplSocket _socket, const void *_pBuffer, uint32_t _len, int32_t _flags, const struct sockaddr *_pTo , socklen_t _nToLen)
{
	SplResult retVal;

	// Only supported send flag at this time is don't fragment
	if ((_flags != 0) && (_flags != SplSocketSendFlagNoFrag))
 		return SplSocketSetLastError(SplErc_SocketInvalidArguments);
 
	if (_flags & SplSocketSendFlagNoFrag)
		SplSocketDontFragment(_socket, 1);		// Set don't fragment flag on socket

	retVal = sendto(_socket, (SplSocketSendBuffer*)_pBuffer, _len, 0, _pTo, _nToLen);

	if (_flags & SplSocketSendFlagNoFrag)
		SplSocketDontFragment(_socket, 0);		// Clear don't fragment flag on socket

	return retVal;
}


// SplSocketRecvFrom
//
SOEXPORT
SplResult SplSocketRecvFrom( SplSocket _socket, void *_pBuffer, uint32_t _len, int32_t _flags, struct sockaddr* _pFrom, socklen_t* _nFromLen)
{
	// Flags are currently not supported
	if (_flags != 0)
		return SplSocketSetLastError(SplErc_SocketInvalidArguments);

    return recvfrom(_socket, (SplSocketRecvBuffer*)_pBuffer, _len, 0, _pFrom, _nFromLen);
}


// SplSocketClose
//
SOEXPORT
SplErc SplSocketClose( SplSocket _socket )
{
#if defined(_WIN32)
	return (closesocket(_socket) == SOCKET_ERROR) ? SplErc_Failure : SplErc_OK;
#elif defined(_LINUX) || defined(__APPLE__) || defined(ESP_PLATFORM)
	return (close(_socket) == 0) ? SplErc_OK : SplErc_Failure;
#endif
}


// SplSocketGetOpt
//
SOEXPORT
SplErc SplSocketGetOpt( SplSocket _socket, int32_t _nLevel, int32_t _optName, void *_pOptVal, socklen_t *_pOptLen )
{
//#if defined(_WIN32)
	if (getsockopt(_socket, _nLevel, _optName, (SplSocketRecvBuffer*)_pOptVal, _pOptLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#if 0 //elif defined(_LINUX) || defined(__APPLE__)
	if (getsockopt(_socket, _nLevel, _optName, _pOptVal, _pOptLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#endif
	return SplErc_OK;
}


// SplSocketSetOpt
//
SOEXPORT
SplErc SplSocketSetOpt( SplSocket _socket, int32_t _nLevel, int32_t _optName, const void *_pOptVal, socklen_t _optLen )
{
//#if defined(_WIN32)
	if (setsockopt(_socket, _nLevel, _optName, (SplSocketSendBuffer*)_pOptVal, _optLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#if 0 //elif defined(_LINUX) || defined(__APPLE__)
	if (setsockopt(_socket, _nLevel, _optName, _pOptVal, _optLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#endif
		return SplErc_OK;
}


// SplSocketGetSockname
//
SOEXPORT
SplErc SplSocketGetSockname( SplSocket _socket, struct sockaddr *_pName, socklen_t *_pNameLen )
{
//#if defined(_WIN32)
	if (getsockname(_socket, _pName, _pNameLen) == SPL_SOCKET_ERROR) 
		return SplErc_Failure;

#if 0	//elif defined(_LINUX) || defined(__APPLE__)
	if (getsockname(_socket, _pName, _pNameLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#endif
	return SplErc_OK;
}


// SplSocketGetPeername
//
SOEXPORT
SplErc SplSocketGetPeername( SplSocket _socket, struct sockaddr *_pName, socklen_t *_pNameLen )
{
//#if defined(_WIN32)
	if (getpeername(_socket, _pName, _pNameLen) == SPL_SOCKET_ERROR) 
		return SplErc_Failure;

#if 0	//elif defined(_LINUX) || defined(__APPLE__)
	if (getpeername(_socket, _pName, _pNameLen) == SPL_SOCKET_ERROR)
		return SplErc_Failure;

#endif
	return SplErc_OK;
}


// SplSocketGetHostByName
//
SOEXPORT
struct hostent* SplSocketGetHostByName(const char *_pszName)
{
	if (!_pszName)
		return NULL;
  
	return gethostbyname(_pszName);
}

#if !defined(ESP_PLATFORM)

// SplSocketGetHostByAddr
//
SOEXPORT
struct hostent* SplSocketGetHostByAddr( const char *_pszAddr, int32_t _len, int32_t _type )
{
	return gethostbyaddr(_pszAddr, _len, _type);
}
#endif

// SplSocketGetPort
//
SOEXPORT
SplErc SplSocketGetPort(SplSocket _socket, uint16_t *_pnPortRet)
{
	struct sockaddr_in sin;

#ifdef _WIN32
	int addrlen = sizeof(sin);
#else
	socklen_t addrlen = sizeof(sin);
#endif

	if (getsockname(_socket, (struct sockaddr *)&sin, &addrlen) != 0)
		return SplErc_Failure;

	if (sin.sin_family == AF_INET) {
		struct sockaddr_in* pSock = (struct sockaddr_in*)(void*)&sin; 
		*_pnPortRet = pSock->sin_port;
	}
	else {
		struct sockaddr_in6* pSock = (struct sockaddr_in6*)(void*)&sin;
  		*_pnPortRet = pSock->sin6_port;
	}
	return SplErc_OK;
}


// SplSocketGetAddrInfo
//
SOEXPORT
SplErc SplSocketGetAddrInfo(const char *_pszHostName, const char *_pszServerName, const SplAddrInfo *_pHints, SplAddrInfo **_ppAddrInfoRet)
{
	return getaddrinfo(_pszHostName, _pszServerName, _pHints, _ppAddrInfoRet);
}


// SplSocketFreeAddrInfo
//
SOEXPORT
void SplSocketFreeAddrInfo(SplAddrInfo *_pAddrInfo )
{
	freeaddrinfo(_pAddrInfo);
}


// SplSocketSelect
//
// if timeout_ms is 0 (SPL_INFINITE), wait infinitely
SOEXPORT
SplErc SplSocketSelect( SplSocketSet *_pSet, uint32_t _timeout_ms)
{
	if (_pSet == NULL)
		return SplErc_SocketInvalidArguments;

#if defined(_WIN32)
	int32_t wait_result = WSAWaitForMultipleEvents( _pSet->socket_count, _pSet->mapped_events, FALSE, _timeout_ms, FALSE);

	if (wait_result == WSA_WAIT_TIMEOUT) 
		return SplErc_SocketSelectTimeout;
 
	if (wait_result == WSA_WAIT_FAILED)
		return SplErc_SocketSelectFailed;
 
	if (wait_result == WSA_WAIT_EVENT_0) {
		// wait_result returns the smallest event index, thus if it is equal to 0 the cancelation was signaled
		ResetEvent(_pSet->mapped_events[0]);
		return SplErc_SocketSelectCanceled;
	} 
	
	if (wait_result == WAIT_IO_COMPLETION) {
		// Alertable means sometimes we get scheduled and an overlapped completion callback occurs.
		// We have nothing more to do, just wait again.
		return SplErc_SocketSelectSignaled;
	} 

	ResetEvent(_pSet->mapped_events[wait_result]);	//*WRONG* The event is reset by calling WSAEnumNetworkEvents()

	return SplErc_SocketSelectSignaled;


#elif defined(_LINUX) || defined(__APPLE__)
	int readysocks;
    struct timeval timeout; 
	int32_t sig[100];

	// If you set the fields in your struct timeval to 0, select() will timeout immediately, 
	// effectively polling all the file descriptors in your sets. If you set the parameter timeout to NULL, it will never timeout, 
	// and will wait until the first file descriptor is ready. 
    timeout.tv_sec = (int)(_timeout_ms/1000);
	timeout.tv_usec = (_timeout_ms - timeout.tv_sec*1000)*1000;

	// Issue the select
    _pSet->readset = _pSet->desiredReadSet;
    _pSet->writeset = _pSet->desiredWriteSet;
    _pSet->exceptset = _pSet->desiredExceptSet;

	readysocks = select((_pSet->maxSock)+1, &(_pSet->readset), &(_pSet->writeset), &(_pSet->exceptset), (_timeout_ms == SPL_INFINITE) ? NULL : &timeout);

	if (readysocks < 0)
        return SplErc_SocketSelectFailed;

	if (readysocks == 0) 
		return SplErc_SocketSelectTimeout;

	if (FD_ISSET(_pSet->cancel_signal[0], &_pSet->readset)) {
        ssize_t rc = read(_pSet->cancel_signal[0], sig, sizeof(sig));
        SPL_UNUSED(rc);
		return SplErc_SocketSelectCanceled;
	}

    return SplErc_SocketSelectSignaled;

#elif defined(ESP_PLATFORM)
	int readysocks;
	struct timeval timeout; 
///	int32_t sig[100];

	// If you set the fields in your struct timeval to 0, select() will timeout immediately, 
	// effectively polling all the file descriptors in your sets. If you set the parameter timeout to NULL, it will never timeout, 
	// and will wait until the first file descriptor is ready. 
	timeout.tv_sec = (int)(_timeout_ms / 1000);
	timeout.tv_usec = (_timeout_ms - timeout.tv_sec * 1000) * 1000;

	// Issue the select
	_pSet->readset = _pSet->desiredReadSet;
	_pSet->writeset = _pSet->desiredWriteSet;
	_pSet->expectset = _pSet->desiredExceptSet;

	readysocks = select((_pSet->maxSock) + 1, &(_pSet->readset), &(_pSet->writeset), &(_pSet->exceptset), (_timeout_ms == SPL_INFINITE) ? NULL : &timeout);

	if (readysocks < 0)
		return SplErc_SocketSelectFailed;

	if (readysocks == 0) 
		return SplErc_SocketSelectTimeout;

	if (_pSet->bCancelled) {
		_pSet->bCancelled = false;
		return SplErc_SocketSelectCanceled;
	}
	
	return SplErc_SocketSelectSignaled;
#endif
}


// SplSocketSelectCancel
//
SOEXPORT
SplErc SplSocketSelectCancel( SplSocketSet *_pSet )
{
#if defined(_WIN32)
	if (_pSet)
		SetEvent(_pSet->mapped_events[0]);

#elif defined(_LINUX) || defined(__APPLE__)
	int32_t sig = 1;
	ssize_t rc;
    SPL_UNUSED(rc);
	if (_pSet) {
		//_SplLogWrite(SPL_LOG_DIAG, "SPL", "SplSocketSelectCancel: writing to pipe %d", _pSet->cancel_signal[1]);
        rc = write(_pSet->cancel_signal[1], &sig, sizeof(sig));
         //_SplLogWrite(SPL_LOG_DIAG, "SPL", "SplSocketSelectCancel: returning after writing to pipe %d", _pSet->cancel_signal[1]);
	}
	
#elif defined(ESP_PLATFORM)
	lwip_cancelselect(_pSet->socket_array[1]);	//**TRN** Work-around added to ESP32 BSP since it does not have eventFd or real select().
	
#endif
	return SplErc_OK;
}


// .SplSocketEnumEvents -- Returns enabled socket events
//
SOEXPORT
SplErc SplSocketEnumEvents(SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents *_pfEventsRet)
{
    uint32_t index;
	// See if the socket is already in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket) {
			break;
		}
	}
	if (index >= _pSet->socket_count)
		return SplErc_Failure;

	*_pfEventsRet = _pSet->events[index];

	return SplErc_OK;
}


// SplSocketSetCreate
//
SOEXPORT
SplErc SplSocketSetCreate( SplSocketSet **_ppSetRet )
{
	*_ppSetRet = (SplSocketSet *)malloc(sizeof(SplSocketSet));
#if defined(_WIN32)
	// Use the first array entry for our cancelation signal
	memset(*_ppSetRet, 0, sizeof(SplSocketSet));
	(*_ppSetRet)->socket_array[0] = 0;
	(*_ppSetRet)->mapped_events[0] = WSACreateEvent();
	(*_ppSetRet)->socket_count = 1;

#elif defined(_LINUX) || defined(__APPLE__)
    (*_ppSetRet)->socket_count = 1;
	// Zero our read and write sets 
    FD_ZERO(&((*_ppSetRet)->desiredReadSet));
    FD_ZERO(&((*_ppSetRet)->desiredWriteSet));
    FD_ZERO(&((*_ppSetRet)->desiredExceptSet));
    FD_ZERO(&((*_ppSetRet)->readset));
	FD_ZERO(&((*_ppSetRet)->writeset));
	FD_ZERO(&((*_ppSetRet)->exceptset));
	// Create the cancel signal.  We are using the self-pipe trick to cancel.
    int rc = pipe((*_ppSetRet)->cancel_signal);
    SPL_UNUSED(rc);
    SplSocketSetNonBlocking( (*_ppSetRet)->cancel_signal[0], TRUE );
    SplSocketSetNonBlocking( (*_ppSetRet)->cancel_signal[1], TRUE );
//	_SplLogWrite(SPL_LOG_DIAG, "SPL", "SplSocketSetCreate: created self pipe %d %d", (*set)->cancel_signal[0], (*set)->cancel_signal[1]);
	FD_SET((*_ppSetRet)->cancel_signal[0], &((*_ppSetRet)->desiredReadSet));
	// Initialize maxSock to the cancel signal
	(*_ppSetRet)->maxSock = (*_ppSetRet)->cancel_signal[0];

#elif defined(ESP_PLATFORM)
	(*_ppSetRet)->socket_count = 1;
	(*_ppSetRet)->socket_count = false;
	// Zero our read and write sets 
	FD_ZERO(&((*_ppSetRet)->desiredReadSet));
	FD_ZERO(&((*_ppSetRet)->desiredWriteSet));
	FD_ZERO(&((*_ppSetRet)->desiredExceptSet));
	FD_ZERO(&((*_ppSetRet)->readset));
	FD_ZERO(&((*_ppSetRet)->writeset));
	FD_ZERO(&((*_ppSetRet)->exceptset));

#endif
	return SplErc_OK;
}


// SplSocketSetDelete
//
SOEXPORT
SplErc SplSocketSetDelete( SplSocketSet *_pSet )
{
	SplErc retVal = SplErc_OK;

#if defined(_WIN32)
	uint32_t index;
	// Walk through the array
	for (index = 0; index < _pSet->socket_count; index++) {
		// Returns non-zero on success
		if (WSACloseEvent(_pSet->mapped_events[index]) == 0)
			retVal = SplErc_Failure;
	}

#elif defined(_LINUX) || defined(__APPLE__)
//	_SplLogWrite(SPL_LOG_DIAG, "SPL", "SplSocketSetDelete: deleting self pipe %d %d", _pSet->cancel_signal[0], _pSet->cancel_signal[1]);
	close(_pSet->cancel_signal[0]);
	close(_pSet->cancel_signal[1]);

#elif defined(ESP_PLATFORM)

#endif
	free(_pSet);

	return retVal;
}


// SplSocketSetClear
//
SOEXPORT
SplErc SplSocketSetClear( SplSocketSet *_pSet )
{
	SplErrorCode retVal = SplErc_OK;
	uint32_t index;

#if defined(_WIN32)

	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		_pSet->socket_array[index] = 0;
		// Returns non-zero on success
		if (WSACloseEvent(_pSet->mapped_events[index]) == 0)
			retVal = SplErc_Failure;
	}
	// Leave the size as one for the cancelation signal
	_pSet->socket_count = 1;

#elif defined(_LINUX) || defined(__APPLE__)
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++)
		_pSet->socket_array[index] = 0;

	_pSet->socket_count = 1;

	// Zero our read and write sets 
    FD_ZERO(&(_pSet->desiredReadSet));
    FD_ZERO(&(_pSet->desiredWriteSet));
    FD_ZERO(&(_pSet->desiredExceptSet));
    FD_ZERO(&(_pSet->readset));
	FD_ZERO(&(_pSet->writeset));
	FD_ZERO(&(_pSet->exceptset));

	// Set the cancel signal in the read set
    FD_SET(_pSet->cancel_signal[0], &(_pSet->desiredReadSet));
	// Initialize maxSock to the cancel signal
    _pSet->maxSock = _pSet->cancel_signal[0];

#elif defined(ESP_PLATFORM)
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++)
		_pSet->socket_array[index] = 0;

	_pSet->socket_count = 1;
	_pSet->bCancelled = false;
	
	// Zero our read and write sets 
	FD_ZERO(&(_pSet->desiredReadSet));
	FD_ZERO(&(_pSet->desiredWriteSet));
    FD_ZERO(&(_pSet->desiredExceptSet));
	FD_ZERO(&(_pSet->readset));
	FD_ZERO(&(_pSet->writeset));
	FD_ZERO(&(_pSet->exceptset));

#endif
	return retVal;
}


// SplSocketSetAdd
//
SOEXPORT
SplErc SplSocketSetAdd( SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents _events )
{
	uint32_t index;

	// We must have an event
	if (_events == SplSocketEventNone)
		return SplErc_Failure;

#if defined(_WIN32)
	// Check if the the socket is already in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	 if (index >=MAX_SOCKETS_PER_SET)
		return SplErc_SocketSetFull;

	if (index >= _pSet->socket_count) {
		// The socket wasn't already in the set, Add it
		_pSet->socket_count++;
		_pSet->socket_array[index] = _socket;
		_pSet->events[index] = _events;		// Remember enabled events
		_pSet->mapped_events[index] = WSACreateEvent();
	}
	// SplSocketEvents are equivalent to FD_XXX
    // Windows auto-sets non-blocking
	WSAEventSelect(_socket, _pSet->mapped_events[index], _events);

#elif defined(_LINUX) || defined(__APPLE__)
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	if (index >= MAX_SOCKETS_PER_SET)
		return SplErc_SocketSetFull;

	if (index >= _pSet->socket_count) {
		_pSet->socket_count++;
		_pSet->socket_array[index] = _socket;
	}
	_pSet->events[index] = _events;		// Remember enabled events
    SplSocketSetNonBlocking(_socket,TRUE);

	// Map reads and accepts to the read fd_set
    if ((_events & SplSocketEventRead) || (_events & SplSocketEventAccept))
        FD_SET(_socket, &(_pSet->desiredReadSet));

    if (_events & SplSocketEventWrite)
        FD_SET(_socket, &(_pSet->desiredWriteSet));

    if (_events & SplSocketEventException)
         FD_SET(_socket, &(_pSet->desiredExceptSet));

    _pSet->maxSock = SPL_MAX(_pSet->maxSock, _socket);
	FD_SET(_pSet->cancel_signal[0], &(_pSet->desiredReadSet));

#elif defined(ESP_PLATFORM)
	_pSet->bCancelled = false;
	
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	if (index >= MAX_SOCKETS_PER_SET)
		return SplErc_SocketSetFull;

	if (index >= _pSet->socket_count) {
		_pSet->socket_count++;
		_pSet->socket_array[index] = _socket;
		_pSet->events[index] = _events;		// Remember enabled events
	}
	SplSocketSetNonBlocking(_socket, TRUE);

		// Map reads and accepts to the read fd_set
	if ((_events & SplSocketEventRead) || (_events & SplSocketEventAccept))
		FD_SET(_socket, &(_pSet->desiredReadSet));

	if (_events & SplSocketEventWrite)
		FD_SET(_socket, &(_pSet->desiredWriteSet));

	if (_events & SplSocketEventException)
		FD_SET(_socket, &(_pSet->desiredExceptSet));

	_pSet->maxSock = SPL_MAX(_pSet->maxSock, _socket);

#endif

	return SplErc_OK;
}


// SplSocketSetRemove
//
SOEXPORT
SplErc SplSocketSetRemove( SplSocketSet *_pSet, SplSocket _socket )
{
	uint32_t index;

#if defined(_WIN32)
	// See if the socket is already in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	if (index < _pSet->socket_count) {
		// We found the socket, clear the events
		// TODO - we are just turning off the event so it isn't actually removed from the array
		// We need a better map to allow us to add and remove sockets and events with ease
		_pSet->events[index] = 0;
		WSAEventSelect(_socket, _pSet->mapped_events[index], 0);
	}

#elif defined(_LINUX) || defined(__APPLE__) || defined(ESP_PLATFORM)
	// See if the socket is already in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	if (index < _pSet->socket_count)
		_pSet->events[index] = 0;

	// Clear the socket from both the read and write fd_sets
    FD_CLR(_socket, &(_pSet->desiredReadSet));
    FD_CLR(_socket, &(_pSet->desiredWriteSet));
    FD_CLR(_socket, &(_pSet->desiredExceptSet));
    FD_CLR(_socket, &(_pSet->readset));
	FD_CLR(_socket, &(_pSet->writeset));
	FD_CLR(_socket, &(_pSet->exceptset));
	// We should lower the maxSock here, but we don't have the info
#endif

	return SplErc_OK;
}


// SplGetSocketEvents
//
SOEXPORT
SplErc SplSocketGetEvents( SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents *_pEventsRet )
{
	uint32_t index;

	*_pEventsRet = 0;

#if defined(_WIN32)
	WSANETWORKEVENTS network_event;

	// Find the socket in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}

	if (index < _pSet->socket_count) {
		WSAEnumNetworkEvents(_socket, _pSet->mapped_events[index], &network_event);	
		*_pEventsRet = network_event.lNetworkEvents;
	 	return SplErc_OK;
	}

#elif defined(_LINUX) || defined(__APPLE__) || defined(ESP_PLATFORM)
	// Find the socket in the set
	// Skip the cancelation signal
	for (index = 1; index < _pSet->socket_count; index++) {
		if (_pSet->socket_array[index] == _socket)
			break;
	}
	if (index >= _pSet->socket_count)
        return SplErc_Failure;

    SplSocketEvents events = _pSet->events[index];

	// Map reads and accepts to the read fd_set
    if ((events & SplSocketEventRead) || (events & SplSocketEventAccept)) {
		if (FD_ISSET(_socket, &(_pSet->readset))!=0)
            *_pEventsRet |= (events & (SplSocketEventRead | SplSocketEventAccept));

        FD_CLR(_socket, &(_pSet->readset));
    }
    if (events & SplSocketEventWrite) {
		if (FD_ISSET(_socket, &(_pSet->writeset))!=0)
            *_pEventsRet |= (events & SplSocketEventWrite);

        FD_CLR(_socket, &(_pSet->writeset));
    }
    if (events & SplSocketEventException) {
		if (FD_ISSET(_socket, &(_pSet->exceptset))!=0)
            *_pEventsRet |= (events & SplSocketEventException);

        FD_CLR(_socket, &(_pSet->exceptset));
    }

#endif
    return SplErc_OK;
}
