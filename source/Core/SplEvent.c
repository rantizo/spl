// SplEvent.c -- Event related functions
// 
// Standardized Platform Layer
//

#include "SplEvent.h"

#if defined(_LINUX)
#include <time.h>
#endif

// SplEventCreate
//
SOEXPORT
SplErc SplEventCreate( SplEvent *event )
{
#if defined (_WIN32)
	SplErc retVal = SplErc_Failure;

	*event = CreateEvent( NULL,FALSE,FALSE,NULL );
	if (*event != NULL)
		retVal = SplErc_OK;

	return retVal;

#elif defined (_LINUX)
	// Note that the semaphore is initialized as non-shared between processes
	// All access must only occur between threads of a single process
	*event = (sem_t *)malloc(sizeof(sem_t));
	//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventCreate: %p / %p", event, *event);
	if (*event == NULL) 
		return SplErc_Failure;
	return sem_init(*event, 0, 0);

#elif defined(__APPLE__)
	// We are using pthread_cond_t variables with a binary state (bool)
	// to build a binary semaphore (event)
	// Note that we use a single bool as the signal, so stacking of
	// signals is not allowed.  For example, if no one is waiting and
	// the signal is set twice, it will only be triggered once
	// This maps to the behavior of auto-reset events on Windows,
	// but needs to be further investigated with sem_t on Linux
	int rc;
	*event = (SplEvent)malloc(sizeof(**event));
	if (event == NULL) 
		return SplErc_Failure;
	
	(*event)->signaled = false;

	rc = pthread_mutex_init((pthread_mutex_t*)&(*event)->lock, NULL);
	if (rc) {
		free(*event);
		return SplErc_Failure;
	}
	// Use NULL for attr since BSD implementation only supports defaults
	rc = pthread_cond_init(&(*event)->cond, NULL);
	if (rc) {
		free(*event);
		return SplErc_Failure;
	}
	return SplErc_OK;
	
	
#elif defined (ESP_PLATFORM)

	*event = xSemaphoreCreateBinary();
	xSemaphoreTake(*event,0);

	//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventCreate: %p / %p", event, *event);
	return (*event == NULL) ? SplErc_Failure : SplErc_OK;

#endif
}


// SplEventWait
//
// If timeout_ms is SPL_INFINITE , block indefinitely
// We do not currently have a trywait
//
SOEXPORT
SplResult SplEventWait(SplEvent *event, uint32_t timeout_ms)
{
	SplResult ret = SplWaitFailed;

	// Make sure event is a valid pointer or else just return with default fail code.
	if (NULL != event) {
		if (NULL == *event) {
			// Oops, event pointer points to an invalid event handle.  Log it and return with default fail code.
			//_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventWait called with NULL SplEvent handle value!  %p", event);
		}
		else {

#if defined (_WIN32)
			// SplResult directly maps to wait results
			return (SplResult)WaitForSingleObject(*event, timeout_ms);

#elif defined (_LINUX)
			struct timespec timeout;
			int sem_result;

			//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventWait: %p / %p", event, *event);
			if (timeout_ms != 0 && timeout_ms != SPL_INFINITE) {
				if (clock_gettime(CLOCK_REALTIME, &timeout) == -1) {
					return SplWaitFailed;
				}
				// Convert ms wait to nanoseconds
				timeout.tv_sec += (timeout_ms / 1000);
				timeout.tv_nsec += ((timeout_ms % 1000) * 1000000);
				if (timeout.tv_nsec > 999999999) {
					timeout.tv_sec++;
					timeout.tv_nsec -= 1000000000;
				}
				do {
					sem_result = sem_timedwait(*event, &timeout);
				} while (sem_result && errno == EINTR);

				if (sem_result) {
					// Unsuccessful
					if (errno == ETIMEDOUT) {
						// Unable to acquire lock before timeout expired
						return SplWaitTimeout;
					}
					else {
						return SplWaitFailed;
					}
				}
				else {
					return SplWaitAcquired;
				}
			}
			else {
				do {
					if (timeout_ms == 0)
						sem_result = sem_trywait(*event);
					else
						sem_result = sem_wait(*event);

				} while (sem_result && errno == EINTR);

				if (sem_result) {
					// Unsuccessful
					if (errno == ETIMEDOUT) {
						return SplWaitTimeout;
					}
					else {
						return SplWaitFailed;
					}
				}
				else {
					return SplWaitAcquired;
				}
			}

#elif defined(__APPLE__)
			struct timespec timeout;
			struct timeval tv;
			int wait_result = 0;

			if (timeout_ms != SPL_INFINITE) {
				if (gettimeofday(&tv, NULL) == -1) {
					return SplWaitFailed;
				}

				// Convert timeval to timespec
				timeout.tv_sec = tv.tv_sec;
				timeout.tv_nsec = tv.tv_usec * 1000;

				// Convert ms wait to nanoseconds
				timeout.tv_sec += (timeout_ms / 1000);
				timeout.tv_nsec += ((timeout_ms % 1000) * 1000000);
				if (timeout.tv_nsec > 999999999) {
					timeout.tv_sec++;
					timeout.tv_nsec -= 1000000000;
				}
				pthread_mutex_lock((pthread_mutex_t*)&(*event)->lock);
				while (!(*event)->signaled && wait_result == 0) {
					wait_result = pthread_cond_timedwait(&(*event)->cond, (pthread_mutex_t*)&(*event)->lock, &timeout);
				}
				if (wait_result) {
					pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
					// Unsuccessful
					if (wait_result == ETIMEDOUT) {
						// Unable to acquire lock before timeout expired
						return SplWaitTimeout;
					}
					else {
						return SplWaitFailed;
					}
				}
				else {
					(*event)->signaled = false;
					pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
					return SplWaitAcquired;
				}
			}
			else {
				pthread_mutex_lock((pthread_mutex_t*)&(*event)->lock);
				while (!(*event)->signaled && wait_result == 0) {
					wait_result = pthread_cond_wait(&(*event)->cond, (pthread_mutex_t*)&(*event)->lock);
				}
				if (wait_result) {
					pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
					// Unsuccessful
					if (wait_result == ETIMEDOUT) {
						return SplWaitTimeout;
					}
					else {
						return SplWaitFailed;
					}
				}
				else {
					(*event)->signaled = false;
					pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
					return SplWaitAcquired;
				}
			}

#elif defined(ESP_PLATFORM)
			// Note: No return code for invalid semaphore, it just crashes unless configASSERT is defined which exits
			return xSemaphoreTake(*event, pdMS_TO_TICKS(timeout_ms)) == pdPASS ? SplWaitAcquired : SplWaitTimeout;

#endif
		} // end if(*event != NULL)
	} // end if(event != NULL)
	return ret;
}



// SplEventSignal
//
SOEXPORT
SplErc SplEventSignal(SplEvent *event)
{
	SplErc ret = SplErc_Failure;

	// Make sure event is a valid pointer or else just return with default fail code.
	if (NULL != event) {
		if (NULL == *event) {
			// Oops, event pointer points to an invalid event handle.  Log it and return with default fail code.
			//_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventSet called with NULL event handle value: %p!", event);
		}
		else {
#if defined (_WIN32)
			ret = (SetEvent(*event) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX)
			//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventSet: %p / %p", event, *event);
			ret = sem_post(*event);

#elif defined(__APPLE__)
			if (pthread_mutex_lock((pthread_mutex_t*)&(*event)->lock) == 0) {
				(*event)->signaled = true;
				ret = (pthread_cond_signal(&(*event)->cond) ? SplSetFailed : SplErc_OK);
				pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
			}
			else {
				_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventSet failed to lock mutex!");
			}

#elif defined(ESP_PLATFORM)
			ret = xSemaphoreGive(*event);

#endif
		} // end if(*event != NULL)
	} // end if(event != NULL)
	return ret;
}

// SplEventSignalFromISR
//
SOEXPORT
SplErc SplEventSignalFromISR(SplEvent *event)
{
	SplErc ret = SplErc_Failure;

	// Make sure event is a valid pointer or else just return with default fail code.
	if (NULL != event) {
		if (NULL == *event) {
			// Oops, event pointer points to an invalid event handle.  Log it and return with default fail code.
			//_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventSet called with NULL event handle value: %p!", event);
		}
		else {
#if defined (_WIN32)
			ret = (SetEvent(*event) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX)
			//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventSet: %p / %p", event, *event);
			ret = sem_post(*event);

#elif defined(__APPLE__)
			if (pthread_mutex_lock((pthread_mutex_t*)&(*event)->lock) == 0) {
				(*event)->signaled = true;
				ret = (pthread_cond_signal(&(*event)->cond) ? SplSetFailed : SplErc_OK);
				pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
			}
			else {
				_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventSet failed to lock mutex!");
			}

#elif defined(ESP_PLATFORM)
			BaseType_t xHigherPriorityTaskWoken = pdFALSE;
			ret = xSemaphoreGiveFromISR(*event,&xHigherPriorityTaskWoken);

#endif
		} // end if(*event != NULL)
	} // end if(event != NULL)
	return ret;
}


// SplEventReset
//
SOEXPORT
SplErc SplEventReset(SplEvent *event)
{
	SplErc ret = SplErc_Failure;

	if (NULL != event) {

#if defined (_WIN32)
		ret = (ResetEvent(*event) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX)
		// Lock the semaphore if not locked (i.e. positive)
		sem_trywait(*event);
		ret = SplErc_OK;

#elif defined(__APPLE__)
		pthread_mutex_lock((pthread_mutex_t*)&(*event)->lock);
		// Reset signaled flag
		(*event)->signaled = false;
		pthread_mutex_unlock((pthread_mutex_t*)&(*event)->lock);
		ret = SplErc_OK;

#elif defined(ESP_PLATFORM)
		ret = (xSemaphoreTake(*event, 0) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif
	}
	return ret;
}


// SplEventDelete
//
SOEXPORT
SplErc SplEventDelete(SplEvent *event)
{
	SplErc ret = SplErc_Failure;

	// Make sure event is a valid pointer or else just return with default fail code.
	if (NULL != event) {
		if (NULL == *event) {
			// Oops, event pointer points to an invalid event handle.  Log it and return with default fail code.
			//_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventDelete called with NULL event handle value: %p!", event);
		}
		else {

#if defined (_WIN32)
			return (CloseHandle(*event) ? SplErc_OK : SplErc_Failure);

#elif defined (_LINUX)
			int rc;
			//_SplLogWrite(SPL_LOG_WARNING, "SPL", "SplEventDelete: %p / %p", event, *event);
			rc = sem_destroy(*event);
			free(*event);
			*event = NULL;
			return rc;

#elif defined(__APPLE__)
			int rc;
			rc = pthread_mutex_destroy((pthread_mutex_t*)&(*event)->lock);
			rc |= pthread_cond_destroy(&(*event)->cond);
			free(*event);
			*event = NULL;
			return rc;

#elif defined(ESP_PLATFORM)
			// Must not delete if there are any threads currently blocked on it
			vSemaphoreDelete(*event);
			ret = SplErc_OK;
#endif
		} // end if(*event != NULL)
	} // end if(event != NULL)
	return ret;
}


// SplEventIsSignaled -- Wait check with no timeout (not infinite)
//
SOEXPORT
SplResult SplEventIsSignaled(SplEvent *event)
{
	SplResult ret = SplWaitFailed;
	// Make sure event is a valid pointer or else just return with default fail code.
	if (NULL != event) {
		if (NULL == *event) {
			// Oops, event pointer points to an invalid event handle.  Log it and return with default fail code.
			//_SplLogWrite(SPL_LOG_ERROR, "SPL", "SplEventDelete called with NULL event handle value: %p!", event);
		}
		else {

#if defined (_WIN32)
			// SplResult directly maps to wait results
			return (SplResult)WaitForSingleObject(*event, 0);

#elif defined (_LINUX)
			struct timespec timeout;
			timeout.tv_nsec = 0;
			timeout.tv_sec = 0;

			int sem_result = sem_timedwait(*event, &timeout);
			if (sem_result) {
				return SplWaitFailed;
			}
			else {
				return SplWaitAcquired;
			}
#elif defined(__APPLE__)
#if !defined(SPL_OSX_USE_MP_FUNCTIONS)
			return (*event)->signaled ? SplWaitAcquired : SplWaitFailed;
#else
			// Waits for either forever (kDurationForever) or the given timeout
			OSStatus opVal = MPWaitForEvent(*event, NULL, 0);
			if (opVal == 0) {
				return SplWaitAcquired;
			}
			else {
				return SplWaitFailed;
			}
#endif

#elif defined(ESP_PLATFORM)
			ret = (xSemaphoreTake(*event, 0) == pdPASS) ? SplWaitAcquired : SplWaitFailed;


#endif
		} // end if(*event != NULL)
	} // end if(event != NULL)
	return ret;
}

