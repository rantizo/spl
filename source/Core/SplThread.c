// SplThread.c -- Threading related functions
//
// Standardized Platform Layer
//

#include "SplThread.h"
#include "SplAtomic.h"
#include "SplCritical.h"

#define MAXTHREADNAME 32

#if defined(_LINUX)
#define SPL_THREAD_SCHEDULING_POLICY SCHED_OTHER
#elif defined(__APPLE__)
#define SPL_THREAD_SCHEDULING_POLICY SCHED_OTHER
#endif


#if defined(_WIN32)

#pragma pack(push,8)
typedef struct tagTHREADNAME_INFO {
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)
const DWORD MS_VC_EXCEPTION = 0x406D1388;

#endif


// SplThreadSetName -- Set OS thread name for debugging
//
void SplThreadSetName(SplThreadIdType threadId, const char* name) 
{
	if (name != NULL) {

#if defined(_WIN32)
		{
			THREADNAME_INFO info;
			info.dwType = 0x1000;
			info.szName = name;
			info.dwThreadID = threadId;
			info.dwFlags = 0;
			__try
			{
				RaiseException( MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(ULONG_PTR), (ULONG_PTR*)&info );
			}
			__except(EXCEPTION_EXECUTE_HANDLER)
			{
			}
		}

#elif defined(__APPLE__)
		// On Apple, we can only set on the current thread, not on another thread
		pthread_setname_np(name);

#elif defined(_LINUX) 
		// On Linux, this only seems to work from within the thread

		//prctl(PR_SET_NAME, name, NULL, NULL, NULL);
#endif
	}
}


// SplThreadWrapper -- Thread entry/exit wrapper function
//
// This thread wrapper function allows the same threadProc type on all platforms
// This wrapper is required for Apple to be able to set ThreadName
//
#if defined(_WIN32)
	DWORD WINAPI SplThreadWrapper(void* _pContext) {
		
#elif defined(_LINUX) || defined(__APPLE__)
	void* SplThreadWrapper(void* _pContext) {
	
#elif defined(ESP_PLATFORM)
	void SplThreadWrapper(void *_pContext) {
	
#endif

#if defined(_WIN32)
	SplThreadIdType threadId = GetCurrentThreadId();
	
#elif defined(_LINUX) || defined(__APPLE__)
	SplThreadIdType threadId = pthread_self();
	
#elif defined(ESP_PLATFORM)
	SplThreadIdType threadId = (SplThreadIdType)xTaskGetCurrentTaskHandle();
		
#endif
	
	SplThreadWrapperContext* pContext = (SplThreadWrapperContext*)_pContext;
	
	// On apple, this needs to be done from the current thread
	SplThreadSetName(threadId, pContext->szName);

	// Call the requested function
	if (pContext->threadProc) 
		pContext->threadProc(pContext->parameter);

	// free our context so we don't leak
	free(pContext);

#if 0	// ThreadMap not supported
	SplThreadRemoveFromThreadMap(threadId);
#endif

#if defined(ESP_PLATFORM)
	vTaskDelete(NULL); // Delete self

#else
	return 0;

#endif
}


// SplThreadCreate -- 
//
// Creates a thread for function start_address with parameter passed to it
// thread receives a newly allocate SplThread reference
// This reference must be destroyed when done being used by calling SplReleaseThread()
//
SOEXPORT
SplErc SplThreadCreate(SplThread **_ppThread,
						SplThreadProc _threadProc,
						void *_parameter, 
						const char *_pszName,					
						size_t _nStackSize	// In bytes
			)
							  
{
#ifdef SPL_USE_THREADMAP
	SplThreadIdType threadId;
#endif
	// Context is deleted at function exit
	// Does not delete _parameter

	SplThreadWrapperContext* pContext = (SplThreadWrapperContext*)malloc(sizeof(SplThreadWrapperContext));
	pContext->threadProc = _threadProc;
	pContext->parameter = _parameter;
	if (_pszName) {
		strncpy(pContext->szName, _pszName, sizeof(pContext->szName)-1);
		pContext->szName[sizeof(pContext->szName)-1] = 0;
	} 
	else
		memset(pContext->szName, 0, sizeof(pContext->szName));

	*_ppThread = (SplThread*)malloc(sizeof(SplThread));
	
#if defined(_WIN32)
//-------------//
// WIN32
//-------------//
	(*_ppThread)->thread_handle = CreateThread( NULL,	// default security descriptor
										  _nStackSize,	// default stack size
										  SplThreadWrapper,
										  pContext,
										  0,	// run immediately after creation
										  &(*_ppThread)->thread_id );
	
	// CreateThread returns handle on success, NULL on failure
	if ((*_ppThread)->thread_handle == NULL) {
		free(pContext);
		free(*_ppThread);
		*_ppThread = NULL;

		return SplErc_Failure;
	}
#ifdef SPL_USE_THREADMAP
	threadId =(*_ppThread)->thread_id;
#endif

#elif defined(_LINUX) || defined(__APPLE__)
//-------------//
// LINUX-APPLE -- _nStackSize not supported
//-------------//

	// pthread_create returns 0 on success, non-zero on failure
	if ( pthread_create( *_ppThread,
						  NULL,	// default attributes
						  SplThreadWrapper,
						  pContext 
						  ) ) {
						  
		free(pContext);
		free(*_ppThread);
		*_ppThread = NULL;

		return SplErc_Failure;
	}
#ifdef SPL_USE_THREADMAP
	threadId = *(*_ppThread);
#endif

#elif defined(ESP_PLATFORM)
//-------------//
// ESP32
//-------------//
	if (xTaskCreate(
		SplThreadWrapper,
		_pszName == NULL ? "" : _pszName,
		_nStackSize == 0 ? configMINIMAL_STACK_SIZE : ((_nStackSize + (sizeof(StackType_t) - 1)) / sizeof(StackType_t)),	// stack size in stack width units
		pContext,
		ESP_TASK_MAIN_PRIO,	// default priority
		&(*_ppThread)->thread_handle) != pdPASS) {
	
	
		free(pContext);
		free(*_ppThread);
		*_ppThread = NULL;

		return SplErc_Failure;
	}
	(*_ppThread)->thread_id = (SplThreadIdType)(*_ppThread)->thread_handle; // No ThreadId in FreeRTOS, use handle as id
#ifdef SPL_USE_THREADMAP
	threadId = (*_ppThread)->thread_id;
#endif

#endif	// platform

#if 0	// No Threadmap support
	// Add to thread name map
	SplThreadAddToThreadMap(threadId, name);
#endif
#if 0	//** Moved to SplThreadWrapper()
	// On Windows and Linux, we can set the thread name for another thread
	// On Mac, we need to do it in the wrapper function
#if defined(_WIN32) 
	SplThreadSetName(threadId, name);
#endif
#endif

	return SplErc_OK;
}


// SplThreadRelease
//
// Finished with the SplThread reference
// Free up resources as necessary
//
SOEXPORT
SplErc SplThreadRelease(SplThread *thread)
{
	SplErc retVal = SplErc_OK;

	if (thread != NULL) {
#if defined(_WIN32)
		// Because this API is used for not only freeing created threads,
		// but also for freeing handles to threads obtained in other ways
		// such as SplGetCurrentThread, we don't check to see if we can 
		// close the thread handle successfully here.  We still properly
		// freed this thread handle once we've deleted the memory associated
		// with it.
		CloseHandle(thread->thread_handle);
#elif defined(_LINUX) || defined(__APPLE__)

#elif defined(ESP_PLATFORM)

#endif	// platform

		free(thread);
	}
	return retVal;
}


// SplThreadGetCurrent -- Returns a pointer to a newly allocated SplThread reference
//
// This reference must be destroyed when done being used by calling
// SplThreadRelease()
//
// WINDOWS: SplThread->thread_handle contains a pseudo handle
//
// ESP32: Uses the handle as the thread_id
//
SOEXPORT
SplThread * SplThreadGetCurrent()
{
	SplThread *thread = (SplThread*)malloc(sizeof(SplThread));

#if defined(_WIN32)
	thread->thread_handle = GetCurrentThread();
	thread->thread_id = GetCurrentThreadId();

#elif defined(_LINUX) || defined(__APPLE__)
	*thread = pthread_self();

#elif defined(ESP_PLATFORM)
	thread->thread_handle = xTaskGetCurrentTaskHandle();
	thread->thread_id = (SplThreadIdType)xTaskGetCurrentTaskHandle();

#endif
	return thread;
}


// SplThreadIsSelf
//
SOEXPORT
int SplThreadIsSelf(SplThread *pThread)
{
#if defined(_WIN32)
	return (GetCurrentThreadId() == pThread->thread_id) ? 1 : 0;
	
#elif defined(_LINUX) || defined(__APPLE__)
	return (pthread_self() == *pThread) ? 1 : 0;
	
#elif defined(ESP_PLATFORM)
	return pThread->thread_id == (SplThreadIdType)xTaskGetCurrentTaskHandle() ? 1 : 0;
	
#endif
}


// SplThreadGetID
//
SOEXPORT
int32_t SplThreadGetID(SplThread *thread)
{
  int32_t result = -1;
	
  if (thread){
	  
#if defined(_WIN32) || defined(ESP_PLATFORM)
    result = thread->thread_id;

#else
    result = (intptr_t)*thread;
	  
#endif
  }
  return result;
}

// SplThreadJoin -- Block execution until the specified thread terminates
//
SOEXPORT
SplErc SplThreadJoin(SplThread *thread)
{
	SplErc retVal = SplErc_Failure;

	if (thread != NULL) {
	  
#if defined(_WIN32)
    // Returns zero on success
    if (WaitForSingleObject(thread->thread_handle, INFINITE) == WAIT_OBJECT_0)
		retVal = SplErc_OK;
	  
#elif defined(_LINUX) || defined(__APPLE__)
    // Returns zero on success
    if (pthread_join(*thread, NULL) == 0)
      retVal = SplErc_OK;
	  
#elif defined(ESP_PLATFORM)
	  //* NOT SUPPORTED YET*

#endif	// platform
	}
	return retVal;
}


// SplThreadIsRunning
//
SOEXPORT 
int SplThreadIsRunning(SplThread *thread)
{
   if(thread == NULL)
      return 0;
	
#if defined(_WIN32)
   return WaitForSingleObject(thread->thread_handle,0) == WAIT_OBJECT_0 ? 1 : 0;   
	
#elif defined(_LINUX) || defined(__APPLE__)
   return pthread_kill(*thread,0) == ESRCH ? 1 : 0;
	
#elif defined(ESP_PLATFORM)
	return eTaskGetState(thread->thread_handle) != eDeleted ? 1 : 0;
	
#endif	// platformo  
}

#if defined (__APPLE__) && !defined (SPL_OSX_THREAD_PRIO_USE_PTHREAD)

// C99 style initialization doesn't seem to work (not compatible with
// GNU C++) - we will have to use largers structs instead of a nice
// union and can't used designated initializers because of that
enum SplThreadPriorityMode {
	SplThreadPriorityModePthread = 0,
	SplThreadPriorityModePolicy = 1
};

typedef struct _spl_thread_policy_t {
	enum SplThreadPriorityMode mode;
	// For SplThreadPriorityModePolicy
	thread_policy_flavor_t flavor;
	// For SplThreadPriorityModePthread
	int pthread_priority;
	// for THREAD_STANDARD_POLICY {natural_t no_data}
	thread_standard_policy_data_t policy_standard;
	// for THREAD_PRECENDENCE_POLICY {int importance}
	thread_precedence_policy_data_t policy_precedence;
	// for THREAD_TIME_CONSTRAINT_POLICY {u32 period, u32 computation, u32 constraint, bool preemptible}
	// specify all time values in microseconds and we will convert as appropriate
	thread_time_constraint_policy_data_t policy_time_constraint;
} spl_thread_policy_t;

// Mapping of our SplThreadPriority enums to OS specific priorities
static spl_thread_policy_t spl_thread_policy[] = {
	// SplThreadPriorityNonCriticalRendering
	{	SplThreadPriorityModePthread, 
		0,
		31,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityCriticalRendering
	{	SplThreadPriorityModePthread, 
		0,
		50,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityBackground
	{	SplThreadPriorityModePthread, 
		0,
		10,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityDebug
	{	SplThreadPriorityModePthread, 
		0,
		10,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityGui
	{	SplThreadPriorityModePthread, 
		0,
		31,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityNetwork
	{	SplThreadPriorityModePthread, 
		0,
		60,
		{0},
		{0},
		{0, 0, 0, FALSE}
	},
	// SplThreadPriorityAudio
	{	SplThreadPriorityModePolicy, 
		THREAD_TIME_CONSTRAINT_POLICY,
		0,
		{0},
		{0},
		{10000, 1000, 2000, FALSE}
	},
};

#endif


// SplThreadSetPriority -- Set the priority of the thread
//
// Process priority is unaffected
//
SOEXPORT
SplErc SplThreadSetPriority(SplThread *thread, SplThreadPriority priority)
{

#if defined(_WIN32)
	if ((NULL == thread) || (NULL == thread->thread_handle)) {
		return SplErc_Failure;
	}

	// If SetThreadPriority fails, the return value is zero. 
	if (0 == SetThreadPriority(thread->thread_handle, priority)) {
		return SplErc_Failure;
	}
	
#elif defined(ESP_PLATFORM)
	if ((NULL == thread) || (NULL == thread->thread_handle)) {
		return SplErc_Failure;
	}
	vTaskPrioritySet(thread->thread_handle, priority);
	return SplErc_OK;
	
#elif defined(_LINUX) || defined (SPL_OSX_THREAD_PRIO_USE_PTHREAD)
	struct sched_param pthread_param;
	pthread_param.sched_priority = priority;

	return (pthread_setschedparam(*thread, SPL_THREAD_SCHEDULING_POLICY, &pthread_param) ? SplErc_Failure : SplErc_OK);
	
#elif defined(__APPLE__)
	// For pthread priority scheduling
	struct sched_param pthread_param;

	// For thread_policy_sets
	thread_policy_t current_policy = NULL;
	mach_msg_type_number_t current_policy_count;

	// For the time constraint policy calculations
	thread_time_constraint_policy_data_t calculated_policy;
	double clock2abs;
	mach_timebase_info_data_t tbinfo;

	// Allow the use of pthread or scheduling policies
	// We can use relative precedence (THREAD_PRECEDENCE_POLICY)
	// Also available is real time (THREAD_TIME_CONSTRAINT_POLICY) and
	// normal (THREAD_STANDARD_POLICY)
	// We can also use pthread to set up the scheduling.  pthread by default uses SCHED_OTHER but
	// does seem to list a default priority level (of 31)
	if (spl_thread_policy[priority].mode == SplThreadPriorityModePthread) {
		pthread_param.sched_priority = spl_thread_policy[priority].pthread_priority;
		return (pthread_setschedparam(*thread, SPL_THREAD_SCHEDULING_POLICY, &pthread_param) ? SplErc_Failure : SplErc_OK);
	}
	else if (spl_thread_policy[priority].mode == SplThreadPriorityModePolicy) {
		if (spl_thread_policy[priority].flavor == THREAD_STANDARD_POLICY) {
			current_policy = (thread_policy_t)&spl_thread_policy[priority].policy_standard;
			current_policy_count = THREAD_STANDARD_POLICY_COUNT;
		}
		else if (spl_thread_policy[priority].flavor == THREAD_PRECEDENCE_POLICY) {
			current_policy = (thread_policy_t)&spl_thread_policy[priority].policy_precedence;
			current_policy_count = THREAD_PRECEDENCE_POLICY_COUNT;
		}
		else if (spl_thread_policy[priority].flavor == THREAD_TIME_CONSTRAINT_POLICY) {
			(void)mach_timebase_info(&tbinfo);
			clock2abs = ((double)tbinfo.denom / (double)tbinfo.numer) * 1000;
			memcpy(&calculated_policy, &spl_thread_policy[priority].policy_time_constraint, sizeof(thread_time_constraint_policy_data_t));

			calculated_policy.period *= clock2abs;
			calculated_policy.computation *= clock2abs;
			calculated_policy.constraint *= clock2abs;

			current_policy = (thread_policy_t)&calculated_policy;
			current_policy_count = THREAD_TIME_CONSTRAINT_POLICY_COUNT;
		}
		if (!current_policy) {
			return SplErc_Failure;
		}
		return (thread_policy_set(pthread_mach_thread_np(*thread),
			spl_thread_policy[priority].flavor,
			current_policy,
			current_policy_count) ? SplErc_Failure : SplErc_OK);

	}
#endif

	return SplErc_OK;
}


#if 0
// SplThreadIndentStr
//
SOEXPORT
SplErc SplThreadIdentStr(SplThread *thread, char* p, uint32_t cb)
{

	char* ptr;

	if ((p == NULL) || (cb == 0))
		return SplErc_Failure;

	SplThreadGetName(thread, p, cb);

	ptr = p + strlen(p);

#if defined(_WIN32)
	_snprintf_s(ptr, cb - strlen(p), cb - strlen(p), " (0x%08x)", thread->thread_id);

#elif defined(_LINUX) || defined(__APPLE__)
	snprintf(ptr, cb - strlen(p), " (%p)", (void *)*thread);

#elif defined(ESP_PLATFORM)
	// No threadId is appended

#endif

	p[cb - 1] = 0;

	return SplErc_OK;

}
#endif

#if 0	// Removed Threadmap, too heavy weight especially for embedded

static SplAtomic SplThreadState = SplStateUninitialized;
static DWORD numNameMapEntries = 0;
static SplThreadIdType* threadIds = NULL;
static char** threadNames = NULL;
static SplCriticalSection nameMapCrit; // protects the map


									   // SplThreadOpen
									   //
SplErc SplThreadOpen()
{
	// SplAtomicCompareExchange returns destination value *before* the operation
	if (SplStateUninitialized == SplAtomicCompareExchange(&SplThreadState, SplStateOpening, SplStateUninitialized) ||
		SplStateClosed == SplAtomicCompareExchange(&SplThreadState, SplStateOpening, SplStateClosed)) {

		// Create critical for thread name map
		SplCriticalCreate(&nameMapCrit);

		// Allocate thread name map
		numNameMapEntries += 100;
		threadIds = (SplThreadIdType*)calloc(numNameMapEntries, sizeof(DWORD));
		threadNames = (char**)calloc(numNameMapEntries, sizeof(char*));

		SplThreadState = SplStateOpen;
	}

	return SplErc_OK;

}


// SplThreadClose
//
SplErc SplThreadClose()
{
	uint32_t i;

	// SplAtomicCompareExchange returns destination value *before* the operation
	if (SplStateOpen == SplAtomicCompareExchange(&SplThreadState, SplStateClosing, SplStateOpen)) {

		// Delete critical for thread name map
		SplCriticalDelete(&nameMapCrit);

		// Clean up and delete thread name map
		for (i = 0; i < numNameMapEntries; i++) {
			if (threadNames[i])
				free(threadNames[i]);
		}
		free(threadNames);
		free(threadIds);
		numNameMapEntries = 0;

		SplThreadState = SplStateClosed;
	}

	return SplErc_OK;
}


// SplThreadAddToThreadMap
//
SplErc SplThreadAddToThreadMap(SplThreadIdType threadId, const char* name)
{
	const char *threadName = name == NULL ? "" : name;
	uint32_t i;
	SplErc err = SplErc_Failure;

	// Add to thread name map (only if we are initialized)
	if (SplThreadState == SplStateOpen) {

		SplCriticalEnter(&nameMapCrit);

		for (i = 0; i < numNameMapEntries; i++) {
			if (threadIds[i] == 0) {
				// found an open spot
				break;
			}
		}

		if (i == numNameMapEntries) {
			// No room, so grow map
			SplThreadIdType* newThreadIds;
			char** newThreadNames;
			int oldEntryCount = numNameMapEntries;
			numNameMapEntries += 100;
			newThreadIds = (SplThreadIdType*)calloc(numNameMapEntries, sizeof(SplThreadIdType));
			newThreadNames = (char**)calloc(numNameMapEntries, sizeof(char*));
			memcpy(newThreadIds, threadIds, oldEntryCount * sizeof(SplThreadIdType));
			memcpy(newThreadNames, threadNames, oldEntryCount * sizeof(char*));
			free(threadIds);
			free(threadNames);
			threadIds = newThreadIds;
			threadNames = newThreadNames;
		}

		if (i < numNameMapEntries) {
			threadIds[i] = threadId;
			threadNames[i] = (char*)malloc(MAXTHREADNAME);
			strncpy(threadNames[i], threadName, MAXTHREADNAME);
			threadNames[i][MAXTHREADNAME - 1] = 0;
			err = SplErc_OK;
		}

		SplCriticalLeave(&nameMapCrit);

	}

	return err;

}


// SplThreadRemoveFromThreadMap
//
SplErc SplThreadRemoveFromThreadMap(SplThreadIdType threadId)
{
	uint32_t i;
	SplErc err = SplErc_Failure;

	if (SplThreadState == SplStateOpen) {

		SplCriticalEnter(&nameMapCrit);

		for (i = 0; i < numNameMapEntries; i++) {
			if (threadIds[i] == threadId) {
				// found entry
				threadIds[i] = 0;
				free(threadNames[i]);
				threadNames[i] = NULL;
				err = SplErc_OK;
				break;
			}
		}

		SplCriticalLeave(&nameMapCrit);
	}

	return err;
}

// SplThreadGetName -- Get the thread name
//
SOEXPORT
void SplThreadGetName(SplThread *thread, char* p, uint32_t cb)
{
	SplThreadIdType threadId;
	uint32_t i;
	memset(p, 0, cb);
	if (SplThreadState == SplStateOpen) {

		SplCriticalEnter(&nameMapCrit);

#if defined(_WIN32) 
		threadId = thread->thread_id;

#elif defined(_LINUX) || defined(__APPLE__)
		threadId = *thread;

#elif defined(ESP_PLATFORM)
		threadId = thread->thread_id;

#endif

		for (i = 0; i < numNameMapEntries; i++) {
			if (threadIds[i] == threadId) {
				// found entry
				strncpy(p, threadNames[i], cb - 1);
				p[cb - 1] = 0;
				break;
			}
		}

		SplCriticalLeave(&nameMapCrit);
	}

	if (strlen(p) == 0) {
		strcpy(p, "Unknown");
	}
}
#endif


