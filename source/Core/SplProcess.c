// SplProcess.c -- Process manipulation
// 
// Standardized Platform Layer
//
// WARNING: WINDOWS SUPPORT INCOMPLETE!!
//

// SplProcess supported?
#if defined(_WIN32) || defined(_LINUX) || defined(__APPLE__)

#include "SplProcess.h"
#include "SplSleep.h"

// Maximum number of arguments that can be passed
// to a new process
#define MAX_ARGUMENTS 64


// SplProcessCreateV -- Generally used internally, normal API is SplProcessCreate
//
// va_list version of SplProcessCreate
// 
SOEXPORT
SplErc SplProcessCreateV( SplProcess **process,
                                SplProcessSetupInfoStruct *setup_info,
                                const char *cmd_line, 
                                char **const argv)
{
	SplErc retVal = SplErc_OK;
#if defined(_WIN32)
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	LPTSTR cmd_line_copy = NULL;
	int cmd_line_len = 0;
	int cmd_line_pos = 0;
	char space[] = " ";
	char **argv_copy = argv;

	SPL_UNUSED(cmd_line);
	SPL_UNUSED(setup_info);
#elif defined(_LINUX) || defined(__APPLE__)
	int fd_idx;
#endif

	// PROCESS_INFORMATION
	*process= (SplProcess*)malloc(sizeof(SplProcess));

#if defined(_WIN32)

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));

	// Copy the comand line since CreateProcessW can modify cmd_line
	// We won't pass lpApplicationName, just lpCommandLine, so put the application
	// name and all the arguments in one string
#if 0
	if (sizeof(TCHAR) == sizeof(char)) {
		cmd_line_copy = (LPTSTR)malloc(strlen(cmd_line)+1);
		strcpy(cmd_line_copy, cmd_line);
	} else {
		cmd_line_copy = (LPTSTR)malloc(wcstombs(NULL, cmd_line, 0)+1);
		wcstombs(cmd_line_copy, cmd_line, wcstombs(NULL, cmd_line, 0)+1);
	}
#endif
	while (*argv_copy) {
		if (cmd_line_pos) {
			mbstowcs((wchar_t *)cmd_line_copy+cmd_line_pos, &space[0], 1);
			cmd_line_pos++;
		}
		cmd_line_len = mbstowcs(NULL, *argv_copy, 0);
		cmd_line_copy = (LPTSTR)realloc(cmd_line_copy, (cmd_line_pos + cmd_line_len + 1) * sizeof(wchar_t));
		if (cmd_line_copy == NULL) {
			return SplErc_Failure;
		}
		mbstowcs((wchar_t *)cmd_line_copy+cmd_line_pos, *argv_copy, cmd_line_len + 1);
		cmd_line_pos += cmd_line_len;
		*argv_copy++;
	}

	// Set bInheritHandles to true, this makes us not have to call DuplicateHandle explicitly
	// for the IPC handles, but might result in extra duplication
	if ( !CreateProcess( NULL,
					cmd_line_copy,
					NULL,
					NULL,
					TRUE,
					0,
					NULL,
					NULL,
					&si,
					&pi ) ) 
	{
		retVal = SplErc_Failure;
		free(*process);
		*process = NULL;
	} 
	else {
		(*process)->process_handle = pi.hProcess;
		(*process)->process_id = pi.dwProcessId;
	}
	free(cmd_line_copy);

#elif defined(_LINUX) || defined(__APPLE__)
	// Fork!  Looked at using vfork but it sounded
	// like a no win in Linux.  Plus we can handle the
	// varargs processing in the child if we don't
	**process = fork();
	//int myerror = errno;
	switch (**process)
	{
		case -1:
			// Fork failed
			retVal = SplErc_Failure;
			//_SplLogWrite(SPL_LOG_DEBUG, "SPL", "*******FORK FAILED PRIOR TO RUNNING %s! *******\n", cmd_line);
			//_SplLogWrite(SPL_LOG_DEBUG, "SPL", "*******Error: %s *******\n", strerror(myerror));
			break;

		case 0:
			// Child process
			// We need to exec the provided command

			//_SplLogWrite(SPL_LOG_DEBUG, "SPL", "******* CHILD PROCESS FORKED for %s! *******\n", cmd_line);

			// Free the SplProcess structure since we won't be needing it
			free(*process);
			*process = NULL;

			if (NULL != setup_info)
			{
				// If keep_fd1 and keep_fd2 are both less than 0, then they are invalid FD values and we can skip this step.
				if ( (0 != (SplProcessSetupInfoRetainFDs & setup_info->process_setup_purpose)) && (setup_info->keep_fd1 >=0 || setup_info->keep_fd2 >= 0) )
				{
					// We are creating a child process with an IPC pipe, or one that wants to retain just one or two existing
					// file descriptors for some other purpose.  For these cases we will set almost all the open File Descriptors
					// we just inherited so that they are released when we do the execv call.  This method of looping through
					// all possible FD values is kind of gross, hopefully we can come up with a better way to do it at some point.
					// Ideally we would have just opened all our files and sockets with "re" or O_CLOSEXEC.  Here, we mark everything
					// except stdin/stdout/stderr (FDs 0, 1, and 2), and we also preserve the FDs for the specified input keep_fd values.
					// If we are only going to do this for Linux, then we could go through the /proc/pid/fd directory to look for the
					// FD values that we need to care about.  Not sure if that would be portable, and this might be faster anyway.
					for (fd_idx = getdtablesize() - 1; fd_idx > 2; fd_idx--)
					{
						if (fd_idx != setup_info->keep_fd1 && fd_idx != setup_info->keep_fd2)
						{
							// This FD value is not stdin, stdout, stderr, or one of the ones we want to keep.
							int flags = fcntl(fd_idx, F_GETFD);
							if (flags != -1)
							{
								// This FD value represents an existing FD.  Mark it for closure.
								fcntl(fd_idx, F_SETFD, flags | FD_CLOEXEC);
							}
						}
					}
				}

				// Call the child forked callback if present
				if ( (0 != (SplProcessSetupInfoCallback & setup_info->process_setup_purpose)) && (NULL != setup_info->cbChildForked) )
				{
					setup_info->cbChildForked(setup_info->pChildForkedContext);
				}
			}

			// Execute!
			execv(cmd_line, argv);

			// If we got here we failed to exec
			_exit(1);
			break;

		default:
			//_SplLogWrite(SPL_LOG_DEBUG, "SPL", "******* PARENT CONTINUING after fork for %s *******\n", cmd_line);
			// Parent process
			break;
	}
#endif
	return retVal;
}

// SplProcessCreate --  Creates a process (forks)
//
// process receives a newly allocate SplProcess reference
// This reference must be destroyed when done being used by calling
// SplReleaseProcess()
// cmd_line is the name of the executable to run
// it is followed by command line arguments
// and must end with a NULL - (char *)0
//
SOEXPORT
SplErc SplProcessCreate( SplProcess **process,
							const char *cmd_line,
							... )
{

	// We need to convert from variadic function to
	// something we can pass to execv
	va_list argp;
	char *cmd_args_array[MAX_ARGUMENTS];
	int argno = 0;

	// Convert the arguments
	// Prepend the cmd_line again for argv[0]
	va_start(argp, cmd_line);
	cmd_args_array[argno++] = (char *)cmd_line;
	while ((cmd_args_array[argno++] = va_arg(argp, char *)) != (char *)0);
	va_end(argp);

	// Call the va_arg compatible version
	return SplProcessCreateV(process, NULL, cmd_line, cmd_args_array);

}


// SplProcessCreateWithSetupInfo -- Creates a process (forks)
//
// process receives a newly allocate SplProcess reference
// This reference must be destroyed when done being used by calling
// SplReleaseProcess()
// cmd_line is the name of the executable to run
// it is followed by command line arguments
// and must end with a NULL - (char *)0
SOEXPORT
SplErc SplProcessCreateWithSetupInfo( SplProcess **process,
							SplProcessSetupInfoStruct *setup_info,
							const char *cmd_line,
							... )
{

	// We need to convert from variadic function to
	// something we can pass to execv
	va_list argp;
	char *cmd_args_array[MAX_ARGUMENTS];
	int argno = 0;

	// Convert the arguments
	// Prepend the cmd_line again for argv[0]
	va_start(argp, cmd_line);
	cmd_args_array[argno++] = (char *)cmd_line;
	while ((cmd_args_array[argno++] = va_arg(argp, char *)) != (char *)0);
	va_end(argp);

	return SplProcessCreateV(process, setup_info, cmd_line, cmd_args_array);

}

#if 0 //**TRN**
// Create a process and pass necessary parameters
// to it to create a bidirectional IPC channel
// This will add necessary arguments to the end of
// the argument list and can be automatically interpreted
// and have the IPC set up by using SplProcessIpcPipeChildOpen
// (on the child)
// On the parent side the pipe will return opened
SOEXPORT
SplErc SplProcessWithIpcPipeCreate( SplProcess **process,
							SplIpcPipe **ipc_pipe,
							const char *cmd_line,
							... )
{

	SplErc ret;

#if defined(_WIN32)
	// RCB not implemented
	SPL_UNUSED(ret);
	SPL_UNUSED(cmd_line);
	SPL_UNUSED(ipc_pipe);
	SPL_UNUSED(process);

	return SplErc_Failure;
#elif defined(_LINUX) || defined(__APPLE__)
	SplProcessSetupInfoStruct setup_info;
	// We need to convert from variadic function to
	// something we can pass
	va_list argp;
	char *cmd_args_array[MAX_ARGUMENTS];
	char pipe_str[2][32];
	int argno = 0;

	// Convert the arguments
	// Prepend the cmd_line again for argv[0]
	va_start(argp, cmd_line);
	cmd_args_array[argno++] = (char *)cmd_line;
	while ((cmd_args_array[argno++] = va_arg(argp, char *)) != (char *)0);
	va_end(argp);

	// Add the IPC argument(s) to the end
	ret = SplIpcPipeCreate(ipc_pipe);
	if (ret != SplErc_OK) {
		return SplErc_Failure;
	}

	sprintf(pipe_str[0], "%d", (*ipc_pipe)->CHILD_READ);
	sprintf(pipe_str[1], "%d", (*ipc_pipe)->CHILD_WRITE);

	argno--;
	cmd_args_array[argno++] = pipe_str[0];
	cmd_args_array[argno++] = pipe_str[1];
	cmd_args_array[argno++] = NULL;

	setup_info.process_setup_purpose = SplProcessSetupInfoRetainFDs;
	setup_info.keep_fd1 = (*ipc_pipe)->CHILD_READ;
	setup_info.keep_fd2 = (*ipc_pipe)->CHILD_WRITE;

	ret =  SplProcessCreateV( process, &setup_info, cmd_line, cmd_args_array);
	if (ret != SplErc_OK) {
		return SplErc_Failure;
	}

	// We must be the parent, go ahead and open the pipe
	ret = SplIpcPipeOpen(*ipc_pipe, 1);
	if (ret != SplErc_OK) {
		return SplErc_Failure;
	}

	return SplErc_OK;
#else
	return SplErc_Failure;
#endif

}

// The child process created by SplProcessWithIpcPipeCreate should
// call this to automatically set up the IPC pipe
// Call this immediately, before processing and argv arguments
// argc and argv will be modified before returning to remove
// the arguments appended by SplProcessWithIpcPipeCreate
// This function will open the pipes so they are ready for reading and writing
SOEXPORT
SplErc SplProcessIpcPipeChildOpen( SplIpcPipe **ppIpcPipe, int *argc, char **argv)
{

	SplErc ret;

	// Ensure we have at least the number of required arguments
#if defined(_WIN32)
	// RCB not implemented
	SPL_UNUSED(argv);
	SPL_UNUSED(argc);
	SPL_UNUSED(ppIpcPipe);
	ret = SplErc_Failure;
#elif defined(_LINUX) || defined(__APPLE__)
	int pipe_read = 0;
	int pipe_write = 0;

	// We receive two pipe fds as two arguments
	if (*argc < 3) {
		return SplErrorProcessInvalidChildArguments;
	}

	pipe_read = atoi(argv[*argc-2]);
	pipe_write = atoi(argv[*argc-1]);

	// Pop our arguments off the end of the list
	*argc -= 2;

	// RCB TODO, should we modify argv as well?  Is
	// there any expectation of a NULL at the end?

	if (!pipe_read || !pipe_write) {
		return SplErrorProcessInvalidChildArguments;
	}

	// Build a pipe structure based on the fds passed through
	// the command line
	ret = SplIpcPipeBuild(ppIpcPipe, pipe_read, pipe_write, 0);
	if (ret != SplErc_OK) {
		return SplErc_Failure;
	}

	// Open the pipe (0=child)
	ret = SplIpcPipeOpen(*ppIpcPipe, 0);
	if (ret != SplErc_OK) {
		return SplErc_Failure;
	}
#endif

	return SplErc_OK;

}
#endif


// SplProcessGetCurrent -- Returns a pointer to a newly allocated SplProcess reference
//
// This reference must be destroyed when done being used by calling
// SplReleaseProcess()
// WINDOWS: SplProcess->process_handle contains a pseudo handle
SOEXPORT
SplProcess * SplProcessGetCurrent()
{
	SplProcess *process = (SplProcess*)malloc(sizeof(SplProcess));
#if defined(_WIN32)
	process->process_handle = GetCurrentProcess();
	process->process_id = GetCurrentProcessId();
#elif defined(_LINUX) || defined(__APPLE__)
	*process = getpid();
#endif
	return process;
}


// SplProcessIsSelf
//
SOEXPORT
int SplProcessIsSelf(SplProcess *pProcess)
{
	if (pProcess) {
#if defined(_WIN32)
		return (GetCurrentProcessId() == pProcess->process_id);
#elif defined(_LINUX) || defined(__APPLE__)
		return (getpid() == *pProcess);
#endif
	}
	else {
		return 0;
	}
}


// SplProcessJoin -- Block execution until the specified process terminates
//
SOEXPORT
SplErc SplProcessJoin( SplProcess *pProcess )
{
	SplErc retVal = SplErc_OK;

#if defined(_WIN32)
	// Returns zero on success
	if (WaitForSingleObject(pProcess->process_handle, INFINITE) != WAIT_OBJECT_0)
		retVal = SplErc_Failure;
#elif defined(_LINUX) || defined(__APPLE__)
	int status;

	// Wait for completion - we don't get the exit status but could using
	// WEXITSTATUS
	waitpid(*pProcess, &status, 0);
#endif
	return retVal;
}


// SplProcessJoinEx
//
SOEXPORT
SplResult SplProcessJoinEx(SplProcess *pProcess, uint32_t nOptions)
{
	SplResult result = SplWaitTimeout;

#if defined(_WIN32)
	uint32_t nWait = INFINITE;
	if (SplProcessJoinExNoHang == (nOptions & SplProcessJoinExNoHang)) {
		nWait = 0;
	}

	if (WaitForSingleObject(pProcess->process_handle, nWait) == WAIT_OBJECT_0) {
		result = SplWaitAcquired;
	}
#elif defined(_LINUX) || defined(__APPLE__)
	int status = 0;
	pid_t wait_result;
	int32_t nWaitPidOptions = 0;

	if (SplProcessJoinExNoHang == (nOptions & SplProcessJoinExNoHang)) {
		nWaitPidOptions |= WNOHANG;
	}

	// Wait for completion - we don't get the exit status but could using WEXITSTATUS
	wait_result = waitpid(*pProcess, &status, nWaitPidOptions);
	if (0 < wait_result)
		result = SplWaitAcquired;
	else if (0 > wait_result && errno == ECHILD)
		result = SplWaitFailed; // Failure indicates that system says child process no longer exists.
#endif  

	return result;
	}

#if defined(_LINUX) || defined(__APPLE__)

#define MAX_KILL_FUNC_WAIT_MS 10000
#define KILL_FUNC_CHECK_MS      100

// Waits for a process to terminate, then kills it if process doesn't die in time.
// This is just a worker thread function used internally by SplProcessTerminate.
void *kill_func(SplProcess *pProcess)
{
	SplResult result;
	int ms_elapsed;
	char name_str[32]; // Only 15 chars are allowed for custom names at this time.

	if (pProcess != NULL)
	{
		sprintf(name_str, "Kill:%d", *pProcess);
#if defined(__APPLE__)
		// On Apple, we can only set on the current thread, not on another thread
		pthread_setname_np(name_str);
#elif defined(_LINUX)
		// On Linux, this only seems to work from within the thread
		///pthread_setname_np(pthread_self(), name_str);	//*FIX* Not supported on ARM
#endif

		for (ms_elapsed = 0; ms_elapsed < MAX_KILL_FUNC_WAIT_MS; ms_elapsed += KILL_FUNC_CHECK_MS)
		{
			SplSleep(KILL_FUNC_CHECK_MS);
			// Allow system to release child resources, otherwise it will remain in a zombie state.
			result = SplProcessJoinEx(pProcess, SplProcessJoinExNoHang);
			if (SplWaitAcquired == result || SplWaitFailed == result)
			{
				// Either the process has terminated by now (Wait Acquired) or it no longer
				// exists at all (Wait Failed).  Either way, we're done.
				SplProcessRelease(pProcess);
				return NULL;
			}
		}
		// Made it all the way to max wait time and process still isn't dead.  Hard kill it.
		kill(*pProcess, SIGKILL);
		SplProcessJoin(pProcess);
		SplProcessRelease(pProcess);
	}
	return NULL;
}

SOEXPORT
SplErc SplProcessTerminateWithSignal( SplProcess *pProcess, uint32_t nSignal )
{
	SplErc retVal = SplErc_Failure;
	SplProcess *pKillProcess;
	pthread_t kill_thread;

	if (0 == kill(*pProcess, nSignal))
	{
		retVal = SplErc_OK;
		switch (nSignal)
		{
			// For these "Quit" cases, make sure the quit actually happens.
			case SIGTERM:
			case SIGINT:
			case SIGQUIT:
			case SIGABRT:
			case SIGHUP:
				// Allow 1ms right now for termination.  If the process is still runnning after that,
				// we spin up a thread that will wait up to 10 seconds before issuing a SIGKILL.
				SplSleep(1);
				// Allow system to release child resources, otherwise it will remain in a zombie state.
				if (SplWaitAcquired != SplProcessJoinEx(pProcess, SplProcessJoinExNoHang))
				{
					// Darn it, process isn't dead yet.  We have to start our kill thread.
					// Create and save a copy of the process info because our caller will be releasing it.
					// Our thread will then delete this copy when the process finally gets killed.
					pKillProcess = (SplProcess*)malloc(sizeof(SplProcess));
					memcpy(pKillProcess, pProcess, sizeof(SplProcess));
					if (0 == pthread_create(&kill_thread, NULL, (void* (*)(void*))kill_func, pKillProcess))
					{
						// Set detach mode on thread so that it releases all resources when done, no join required.
						pthread_detach(kill_thread);
					}
					else
					{
						// Our kill thread didn't start, so release our copy of the process info now.
						// TBD: Decide what success/failure value to return to caller at this point.
						free(pKillProcess);
					}
				}
				break;
			default:
				// Nothing else to do, just return to caller.
				break;
		}
	}

	return retVal;
}
#endif


// SplProcessTerminate -- Forcibly terminate a running process.
//
SOEXPORT
SplErc SplProcessTerminate( SplProcess *pProcess )
{
	SplErc retVal = SplErc_Failure;

#if defined(_WIN32)
	if (TerminateProcess(pProcess->process_handle, 0))
		retVal = SplErc_OK;
#elif defined(_LINUX) || defined(__APPLE__)
	// For *nix we send SIGTERM first, to give process a chance to clean up after itself.
	// If SIGTERM doesn't kill it, it will be followed up with a SIGKILL.
	retVal = SplProcessTerminateWithSignal(pProcess, SIGTERM);
#endif

	return retVal;

}


// SplProcessExit -- Ends the calling process and all threads
//
// Cleans up any attached DLLs
//
SOEXPORT
void SplProcessExit(int exit_code)
{
#if defined(_WIN32)
	ExitProcess(exit_code);
#elif defined(_LINUX) || defined(__APPLE__)
	exit(exit_code);
#endif
}


// SplProcessRelease -- Finished with the SplThread reference
//
// Free up resources as necessary
//
SOEXPORT
SplErc SplProcessRelease( SplProcess *pProcess )
{
	SplErc retVal = SplErc_OK;

	if (pProcess == NULL)
		return retVal;

#if defined(_WIN32)
	// Returns non-zero on success
	if (CloseHandle(pProcess->process_handle) == 0)
		retVal = SplErc_Failure;
#elif defined(_LINUX) || defined(__APPLE__)

#endif
	free(pProcess);
	return retVal;
}


// RCB TODO - SplProcessPriority work


// SplProcessSetClass --  Windows specific
//
// Eventually remap the whole priority and class scheme?
//
SOEXPORT
SplErc SplProcessSetClass( enum SplProcessClass pclass )
{
	SplErc retVal = SplErc_OK;
#if defined (_WIN32)
	SetPriorityClass(GetCurrentProcess(), pclass);
#endif

#if defined (__APPLE__)
    SPL_UNUSED(pclass);
#endif

#if defined (_LINUX)
    SPL_UNUSED(pclass);
#endif

	return retVal;
}

#endif	// SplProcess supported
