//  SplSemaphore.h -- Semaphores for counting lock synchronization.
//
// Standardized Platform Layer
//

#include "SplSemaphore.h"
#include "SplAtomic.h"
#include "SplErrors.h"

#include <limits.h>

SOEXPORT
SplErc SplSemaphoreCreate(SplSemaphore *semaphore, int initial)
{
	return SplSemaphoreCreateEx(semaphore,initial,INT_MAX);
}

SOEXPORT
SplErc SplSemaphoreCreateEx(SplSemaphore *semaphore, int initial, int max)
{
	SplErrorCode retVal = SplErc_Failure;
	if(semaphore != NULL){
		semaphore->maxCount = max;
		semaphore->count = initial;

#if defined (_WIN32)
		semaphore->semaphore = CreateSemaphore(NULL, initial, max, NULL);
		retVal = semaphore->semaphore ? SplErc_OK : SplErc_Failure;

#elif defined (_LINUX)
		retVal = (sem_init((sem_t*)&semaphore->semaphore,0,initial) == 0) ? SplErc_OK : SplErc_Failure;

#elif defined(__APPLE__)
		// Apple does not implement support for unnamed posix semaphores, so
		// create a named semaphore with a (possibly) unique name.  It's not
		// great, but it will do.
		char name[MAX_PATH] = {0};
		// Really lame name generator.  Use the elapsed time since SPL was started, and the current
		// thread to create unique names.  In theory the elapsed count and thread combination should
		// ensure that no duplicates exist across threads.
		SplThread *currentThread = SplThreadGetCurrent();
		sprintf(name,"/%s/%p/%p","SPLS",(void*)SplElapsedCount(),currentThread);
		SplThreadRelease(currentThread);
		semaphore->semaphore = sem_open(name, O_CREAT | O_EXCL, 0644, initial );
		retVal = sem_unlink(name) == 0 ? SplErc_OK : SplErc_Failure;
		retVal = (semaphore->semaphore == SEM_FAILED) ? SplErc_Failure : retVal;

#elif defined(ESP_PLATFORM)
		retVal = ((semaphore->semaphore = xSemaphoreCreateCounting(max,initial)) != NULL) ? SplErc_OK : SplErc_Failure;

#endif
  }
  return retVal;
}

SOEXPORT
SplErc SplSemaphoreDelete(SplSemaphore *semaphore)
{
  SplErrorCode retVal = SplErc_Failure;

  if (semaphore != NULL) {

#if defined(_WIN32)
    retVal = CloseHandle(semaphore->semaphore) == 0 ? SplErc_Failure : SplErc_OK;

#elif defined (_LINUX)

    retVal = (sem_destroy(&semaphore->semaphore) == 0) ? SplErc_OK : SplErc_Failure;

#elif defined(__APPLE__)
    retVal = sem_close(semaphore->semaphore) == 0 ? SplErc_OK : SplErc_Failure;

#elif defined(ESP_PLATFORM)
	  vSemaphoreDelete(semaphore);
	  retVal = SplErc_OK;

#endif
  }
  return retVal;
}

SOEXPORT
SplResult SplSemaphoreAcquire(SplSemaphore *semaphore) 
{
	SplResult retVal = SplLockFailed;
	if (semaphore != NULL) {

#if defined (_WIN32)
		retVal = WaitForSingleObject(semaphore->semaphore,INFINITE) == WAIT_OBJECT_0 ? SplErc_OK : SplErc_Failure;

#elif defined (_LINUX)
		retVal = sem_wait(&semaphore->semaphore) == 0 ? SplLockAcquired : SplLockFailed; 

#elif defined(__APPLE__)
		retVal = sem_wait(semaphore->semaphore) == 0 ? SplLockAcquired : SplLockFailed;

#elif defined(ESP_PLATFORM)
		retVal = (xSemaphoreTake(semaphore->semaphore, SPL_INFINITE) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif
		if (retVal == SplLockAcquired) {
			SplAtomicDec(&semaphore->count);
		}
	}
	return retVal;
}

SOEXPORT
SplResult SplSemaphoreTryAcquire(SplSemaphore *semaphore) 
{
	SplResult retVal = SplLockFailed;
	if (semaphore != NULL) {

#if defined (_WIN32)
		DWORD status = WaitForSingleObject(semaphore->semaphore, 0);
		switch(status) {
		case WAIT_OBJECT_0:
			retVal = SplLockAcquired;
			break;
		default:
			retVal = SplLockFailed;
		}

#elif defined (_LINUX)
		retVal = sem_trywait(&semaphore->semaphore) == 0 ? SplLockAcquired : SplLockFailed;

#elif  defined(__APPLE__)
		retVal = sem_trywait(semaphore->semaphore) == 0 ? SplLockAcquired : SplLockFailed;

#elif defined(ESP_PLATFORM)
		retVal = (xSemaphoreTake(semaphore->semaphore, 0) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

		if (retVal == SplLockAcquired) {
			SplAtomicDec(&semaphore->count);
		}
	}

	return retVal;
}

SOEXPORT
int SplSemaphoreGetCount(SplSemaphore *semaphore) 
{
	return semaphore != NULL ? semaphore->count : 0;
}

SOEXPORT
SplErc SplSemaphoreRelease(SplSemaphore *semaphore, int count) 
{
	SplErc retVal = SplErc_Failure;

	if (semaphore != NULL) {

#if defined (_WIN32)
		retVal = ReleaseSemaphore(semaphore->semaphore, count, NULL) != 0 ? SplErc_OK : SplErc_Failure;

#elif defined (_LINUX) || defined(__APPLE__)
		int i = 0;
		for(; i < count; i++){
			int count = SplSemaphoreGetCount(semaphore);
			if (count < semaphore->maxCount) {

#if defined(_LINUX)
				retVal = sem_post(&semaphore->semaphore) == 0 ? SplErc_OK : SplErc_Failure;
#else
				retVal = sem_post(semaphore->semaphore) == 0 ? SplErc_OK : SplErc_Failure;
#endif
				if (retVal != SplErc_OK) {
					break;
				}
			 }
		}

#elif defined(ESP_PLATFORM)
		retVal = (xSemaphoreGive(semaphore->semaphore) == pdPASS) ? SplErc_OK : SplErc_Failure;

#endif

		if (retVal == SplErc_OK) {
			SplAtomicAdd(&semaphore->count,count);
		}
	}

	return retVal;
}
