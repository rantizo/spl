// SplFile.c -- File I/O functionality
// 
// Standardized Platform Layer
//

#include "SplFile.h"
#include "SplDll.h"
#include "SplErrors.h"


#if defined(_LINUX) || defined(__APPLE__)
#include <assert.h>
#include <dirent.h>
#include <fnmatch.h>
#include <utime.h>

#elif defined(_WIN32)
#include <sys/utime.h>
#include <Shlwapi.h>

#endif

#if defined(_LINUX) || defined(__APPLE__)
#define __FOPEN		open
#define __FCLOSE	close
#define __COMMIT	fsync
#define __FREAD		read
#define __FWRITE	write
#define __FSEEK		lseek
#define __FSEEK64	lseek64
#define __FDUP		dup
#define __FDUP2		dup2
#define __UTIME64	utime
#define __FUTIME64	futimes
#define __STAT64	stat64
#define __FSTAT64	fstat64

#define __UTIMBUF__	utimbuf
#define __STAT64__	stat64

#elif defined(_WIN32)
#define __FOPEN		_open
#define __FCLOSE	_close
#define __COMMIT	_commit
#define __FREAD		_read
#define __FWRITE	_write
#define __FSEEK		_lseek
#define __FSEEK64	_lseeki64
#define __TELL		_tell
#define __TELL64	_telli64
#define __FDUP		_dup
#define __FDUP2		_dup2
#define __UTIME64	_utime64
#define __FUTIME64	_futime64
#define __STAT64	_stat64
#define __FSTAT64	_fstat64

#define __UTIMBUF__	__utimbuf64
#define __STAT64__	_stat64

#endif


//-------
// Files
//-------


// SplFileOpen
//
SOEXPORT
SplErc SplFileOpen(int *_pFdRet,const char *_pszFileName,int _flags,int _modes)
{
	int fd;

	if ((fd = __FOPEN(_pszFileName,_flags,_modes)) < 0)
		return SplGetLastError();

	*_pFdRet = fd;

	return SplErc_OK;
}


// SplFileClose
//
SOEXPORT
SplErc SplFileClose(int _fd)
{
	return __FCLOSE(_fd) < 0 ? SplGetLastError() : SplErc_OK;
}


// SplFileCommit
//
SOEXPORT
SplErc SplFileCommit(int _fd)
{
	if (__COMMIT(_fd) == -1)
		return SplGetLastError();

	return SplErc_OK;
}


// SplFileRead
//
SOEXPORT
SplErc SplFileRead(int _fd,void *_pBuf,size_t _sBuf,size_t *_pcReadRet)
{
	size_t cRead;

	if ((cRead = __FREAD(_fd,_pBuf,_sBuf)) == -1)
		return SplGetLastError();

	*_pcReadRet = cRead;

	return OK;
}


// SplFileWrite
//
SOEXPORT
SplErc SplFileWrite(int _fd,const void *_pBuf,size_t _cBuf,size_t *_pcWriteRet)
{
	size_t cWrite;

	if ((cWrite = __FWRITE(_fd,_pBuf,_cBuf)) == -1)
		return SplGetLastError();

	*_pcWriteRet = cWrite;

	return OK;
}


// SplFileSeek
//
SOEXPORT
SplErc SplFileSeek(int _fd,off_t  _offset, int _whence, off_t *_pOffsetRet)
{
	off_t offset;

	if ((offset = __FSEEK(_fd,_offset,_whence)) == -1)
		return SplGetLastError();

	*_pOffsetRet = offset;

	return OK;
}

// SplFileSeek64
//
SOEXPORT
SplErc SplFileSeek64(int _fd,off64_t  _offset, int _whence, off64_t *_pOffsetRet)
{
	off64_t offset;

	if ((offset = __FSEEK64(_fd,_offset,_whence)) == -1)
		return SplGetLastError();

	*_pOffsetRet = offset;

	return OK;
}

// SplFileTell
//
SOEXPORT
SplErc SplFileTell(int _fd,off_t *_pOffRet)
{
	return SplFileSeek(_fd,0,SplFileSeekCur,_pOffRet);
}

// SplFileTell64
//
SOEXPORT
SplErc SplFileTell64(int _fd,off64_t *_pOffRet)
{
	return SplFileSeek64(_fd,0,SplFileSeekCur,_pOffRet);
}

// SplFileDup
//
SOEXPORT
SplErc SplFileDup(int _fd, int *_pFdNewRet)
{
	int fdNew;

	if ((fdNew = __FDUP(_fd)) == -1)
		return SplGetLastError();

	*_pFdNewRet = fdNew;

	return OK;
}


// SplFileDup2
SOEXPORT
SplErc SplFileDup2(int _fd, int _fd2, int *_pFdNewRet)
{
	int fdNew;

	if ((fdNew = __FDUP2(_fd,_fd2)) == -1)
		return SplGetLastError();

	*_pFdNewRet = fdNew;

	return OK;
}

// SplFileGetStatus
//
SplErc SplFileGetStatus(const char *_pszFileName, struct SplFileStat64 *_pStatRet)
{
	if (__STAT64(_pszFileName,_pStatRet) == -1)
		return SplGetLastError();

	return SplErc_OK;
}

// SplFileFGetStatus
//
SplErc SplFileGetFStatus(int _fd, struct SplFileStat64 *_pStatRet)
{
	if (__FSTAT64(_fd,_pStatRet) == -1)
		return SplGetLastError();

	return SplErc_OK;
}

// SplFileGetFSize 
//
SOEXPORT
SplErc SplFileGetFSize(int _fd, off64_t *_pSizeRet)
{
	struct __STAT64__ stat;

	if (__FSTAT64(_fd,&stat) == -1)
		return SplGetLastError();

#if defined(_LINUX) || defined(__APPLE__)
	*_pSizeRet = stat.st_blocks * 512;

#elif defined (_WIN32)
	*_pSizeRet = stat.st_size;

#endif

	return SplErc_OK;
}

// SplFileGetSize 
//
SOEXPORT
SplErc SplFileGetSize(const char *_pszFileName, off64_t *_pSizeRet)
{
	struct __STAT64__ stat;

	if (__STAT64(_pszFileName,&stat) == -1)
		return SplGetLastError();

#if defined(_LINUX) || defined(__APPLE__)
	*_pSizeRet = stat.st_blocks * 512;

#elif defined (_WIN32)
	*_pSizeRet = stat.st_size;

#endif

	return SplErc_OK;
}


// SplFileFTruncate
//
SOEXPORT
SplErc SplFileFTruncate(int _fd, off64_t _len)
{
#if defined(_LINUX) || defined(__APPLE__)
	if (ftruncate(_fd,_len) == -1)
		return SplGetLastError();

#elif defined (_WIN32)
	SplErc erc;
	off64_t offset;

	if ((erc = SplFileSeek64(_fd,_len,SplFileSeekSet,&offset)) != SplErc_OK)
		return erc;

	HANDLE hFile;
		
	if ((hFile = (HANDLE)_get_osfhandle(_fd)) == INVALID_HANDLE_VALUE)
		return SplGetLastError();

	SetEndOfFile(hFile);

#endif
	return OK;
}


// SplFileTruncate
SOEXPORT
SplErc SplFileTruncate(const char *_pszFileName, off64_t _len)
{
#if defined(_LINUX) || defined(__APPLE__)
	if (truncate(_pszFileName,_len) == -1)
		return SplGetLastError();

	return SplErc_OK;

#elif defined (_WIN32)
	int fd;
	ERC erc;

	if ((erc = SplFileOpen(&fd,_pszFileName,O_RDWR,0)) != SplErc_OK)
		return erc;

	erc = SplFileFTruncate(fd,_len);

	SplFileClose(fd);

	return erc;

#endif
}


//-----------
// File Time
//-----------


// SplFileGetFTimes
//
SOEXPORT
SplErc SplFileGetFTimes(int _fd, struct SplFileTimes *_pTimes)
{
	struct __STAT64__ stat;

	if (__FSTAT64(_fd,&stat) == -1)
		return SplGetLastError();

	_pTimes->timeAccess = stat.st_atime;
	_pTimes->timeModified = stat.st_mtime;

	return SplErc_OK;
}


// SplFileSetFTime
//
SOEXPORT
SplErc SplFileSetFTimes(int _fd, struct SplFileTimes *_pTimes)
{
#if defined(_LINUX) || defined(__APPLE__)
	struct timeval times[2];

	times[0].tv_sec = (__time_t)(_pTimes->timeAccess / 1000000);
    times[0].tv_usec = (__suseconds_t)(_pTimes->timeAccess % 1000000);
	
    times[1].tv_sec = (__time_t)(_pTimes->timeModified / 1000000);
    times[1].tv_usec = (__suseconds_t)(_pTimes->timeModified % 1000000);

    if (__FUTIME64(_fd,times) == -1)
        return SplGetLastError();

#elif defined (_WIN32)
	struct __UTIMBUF__ times;

	times.actime = _pTimes->timeAccess;
	times.modtime = _pTimes->timeModified;

    if (__FUTIME64(_fd,&times) == -1)
        return SplGetLastError();
#endif
    return OK;
}


// SplFileGetTime
//
SOEXPORT
SplErc SplFileGetTimes(const char *_pszFileName,struct SplFileTimes *_pTimes)
{
	struct __STAT64__ stat;

	if (__STAT64(_pszFileName,&stat) == -1)
		return SplGetLastError();

	_pTimes->timeAccess = stat.st_atime;
	_pTimes->timeModified = stat.st_mtime;

	return SplErc_OK;
}


// SplFileSetTime
SOEXPORT
SplErc SplFileSetTimes(const char *_pszFileName,struct SplFileTimes *_pTimes)
{
	struct __UTIMBUF__ times;

	times.actime = _pTimes->timeAccess;
	times.modtime = _pTimes->timeModified;

	if (__UTIME64(_pszFileName,&times) == -1)
		return SplGetLastError();

	return OK;
}



//---------
// Folders
//---------

// SplFileFolderCreate -- Create the provided Folder
//
// Capable of recursive creation
// Returns SplErc_OK on successful creation or if
// directory already exists
// Returns SplErc_Failure if unable to create directory
//
SOEXPORT
SplErc SplFileFolderCreate(const char *_pszPath,BOOL _bCreateParentFolders)
{
	SplErc erc;
	const char *pPathEnd;
	char szNew[SplMaxPath];
	int32_t new_len;

	if (!_pszPath || (strlen(_pszPath) == 0))
		return SplErc_Failure;

	// Attempt to create the directory
	// On failure descend the path, recursively calling create

#if defined(_WIN32)
	if (CreateDirectoryA(_pszPath, NULL))
		return SplErc_OK;

	if ((erc = GetLastError()) == ERROR_ALREADY_EXISTS)
		return SplErc_OK;

	if (erc == ERROR_PATH_NOT_FOUND && _bCreateParentFolders) {
		// We need to recurse
		// Find last occurance of the path seperator
		if ((pPathEnd = strrchr(_pszPath, '\\')) != NULL) {

			new_len = pPathEnd - _pszPath;
			strncpy(szNew, _pszPath, new_len);
			szNew[new_len] = 0;

			if ((erc = SplFileFolderCreate(szNew,TRUE)) == SplErc_OK) {
				// Now that we have created the directory above us create  what we were called with
				if (CreateDirectoryA(_pszPath, NULL))
					return SplErc_OK;
			}
		}	
	}
	return SplErc_Failure;

#elif defined(_LINUX) || defined(__APPLE__)

	if (mkdir(_pszPath, S_IRWXU))
		return SplErc_OK;

	if (errno == EEXIST)
		return SplErc_OK;

	if (_bCreateParentFolders) {
		// We need to recurse to create parent folders
		// Find last occurance of the path seperator
		if ((pPathEnd = strrchr(_pszPath, '\\')) != NULL) {

			new_len = pPathEnd - _pszPath;
			strncpy(szNew, _pszPath, new_len);
			szNew[new_len] = 0;

			if ((erc = SplFileFolderCreate(szNew,TRUE)) == SplErc_OK) {
				// Now that we have created the directory above us create  what we were called with
				if (mkdir(_pszPath, S_IRWXU))
					return SplErc_OK;
			}
		}
	}
	return SplErc_Failure;

#endif
}


// SplFileFolderDelete
//
SOEXPORT
SplErc SplFileFolderDelete(const char *_pszDirectory)
{
    SplErc err = SplErc_OK;
#if defined(_WIN32)
	
	WIN32_FIND_DATAA fd;
	HANDLE fh;
	char szPat[MAX_PATH];
	char szFile[MAX_PATH];

	strcpy(szPat,_pszDirectory);
	strcat(szPat,"\\*");

	if ((fh = FindFirstFileA(szPat,&fd)) != INVALID_HANDLE_VALUE) {
		do { 
            if(strcmp(fd.cFileName, ".") == 0)
                continue;
            if(strcmp(fd.cFileName, "..") == 0)
                continue;

			strcpy(szFile,_pszDirectory);
			strcat(szFile,"\\");
			strcat(szFile,fd.cFileName);

			SplFileDelete(szFile);
		
		} while (FindNextFileA(fh,&fd));

//		CloseHandle(fh);
	}

	err = SplErc_OK;

#elif defined(_LINUX) || defined(__APPLE__)
    
    err = SplErc_Failure;
    
    // rmdir requires the directory be empty
    struct dirent *dirEntry;
    DIR *pDir = opendir(_pszDirectory);
    
    if(pDir != NULL) {
        char szFullPath[MAX_PATH];

        for(dirEntry = readdir(pDir); dirEntry; dirEntry = readdir(pDir)) {
            if(strcmp(dirEntry->d_name, ".") == 0)
                continue;
            if(strcmp(dirEntry->d_name, "..") == 0)
                continue;

            sprintf(szFullPath, "%s/%s", _pszDirectory, dirEntry->d_name);
            SplFileDelete(szFullPath);
        }
        
        closedir(pDir);
    }
    
    int rmResult = rmdir(_pszDirectory);
    switch(rmResult) {
    case 0:
        err = SplErc_OK;
		break;
            
    case ENOTDIR:
    case ENOTEMPTY:
        // developer error - stop
        assert(0);
        break;
    }
#endif
    return err;
}


// SplFileDelete
//
SOEXPORT
SplErc SplFileDelete(const char *_pszFileName)
{

	SplErc err = SplErc_OK;
#if defined(_WIN32)
	BOOL ret;
    	ret = DeleteFileA( _pszFileName );

	if (ret == 0) 
		err = SplErc_Failure;

#elif defined(_LINUX) || defined(__APPLE__)
	int ret;
	ret = remove(_pszFileName);
	if (ret != 0) 
		err = SplErc_Failure;

#endif
	return err;;
}


// SplFileMove
//
// Behavior differs between platforms if destination file exists
// Windows (regardless of method used) will not overwrite the destination
// while *nix OSes take that liberty with rename()
// We should probably make this consistent  but since it is hard to make it atomic we will pass for now
//
SOEXPORT
SplErc SplFileMove(const char *_pszFrom, const char *_pszTo)
{
	SplErc err = SplErc_OK;
#if defined(_WIN32)
	BOOL ret;
	ret = MoveFileA( _pszFrom, _pszTo );
	if (ret == 0) 
		err = SplErc_Failure;

#elif defined(_LINUX) || defined(__APPLE__)
	int ret;
	ret = rename(_pszFrom, _pszTo);
	if (ret != 0)
		err = SplErc_Failure;

#endif
	return err;
}


// SplFileCopyToPath **BUG** _pszFrom must not include a path!!!
//
// Copy file to a given path (provide the directory only, not a file name)
// Does not fail if "to" file exists
// Can add this functionality later if required, similar to CopyFileA
//
SOEXPORT
SplErc SplFileCopyToPath(const char *_pszFrom, const char *_pszToPath)
{
	int to_path_len = strlen(_pszToPath) + strlen(_pszFrom) + 2;
	SplErc err = SplErc_OK;

	char *to_file = (char *)malloc(to_path_len);
	if (to_file == NULL) {
		err = SplErc_Failure;
		return err;
	}

	memset(to_file, 0, to_path_len);
	strcat(to_file, _pszToPath);
	strcat(to_file, "/");
	strcat(to_file, _pszFrom);

	err = SplFileCopy(_pszFrom, to_file);
	free(to_file);

	return err;
}


// SplFileCopy
//
// Does not fail if "to" file exists
// Can add this functionality later if required, similar to CopyFileA
//
SOEXPORT
SplErc SplFileCopy(const char *_pszFrom, const char *_pszTo)
{
	SplErc err = SplErc_OK;
#if defined(_WIN32)
	BOOL ret;
	ret = CopyFileA( _pszFrom, _pszTo, FALSE );
	if (ret == 0)
		err = SplErc_Failure;

#elif defined(_LINUX) || defined(__APPLE__)
	FILE *f_from = NULL;
	FILE *f_to = NULL;
	char ch;

	f_from = fopen(_pszFrom, "r");
	if (f_from == NULL) {
		err = SplErc_Failure;
		goto copy_exit;
	}

	f_to = fopen(_pszTo, "w");
	if (f_to == NULL) {
		err = SplErc_Failure;
		goto copy_exit;
	}

	// Not the world's most efficient method
	while (!feof(f_from)) {
		ch = fgetc(f_from);
		if (ferror(f_from)) {
			err = SplErc_Failure;
			break;
		}
		if (!feof(f_from)) fputc(ch, f_to);
		if (ferror(f_to)) {
			err = SplErc_Failure;
			break;
		}
	}

copy_exit:
	if (f_from) fclose(f_from);
	if (f_to) fclose(f_to);
#endif
	return err;
}


// SplFileExists
//
// Use 'stat' to determine if a file exists
// This doesn't ensure we have read or write permissions
// And we don't differentiate between a directory and file
// We will go with stat here for Windows as well instead of
// implementing something CreateFile based
SOEXPORT
BOOL SplFileExists(const char *_pszFileName)
{
	struct stat fileInfo;
	int ret;

	ret = stat(_pszFileName, &fileInfo);
	if (ret == 0)
		return TRUE;	// We got the attributes therefore the file exists

	// We couldn't get the attributes, there was either a
	// permissions problem or the file didn't exist
	return FALSE;


}


// SplFolderExists
//
SOEXPORT
BOOL SplFolderExists(const char *_pszFolder)
{
    return SplFileExists(_pszFolder);
}

// SplFileFindInit
//
void SplFileFindInit(SplFileIterator *_pIter)
{
	_pIter->bOpen = FALSE;
	_pIter->szWildcard[0] = _pIter->szFoundPath[0] = 0; 
	_pIter->pszFoundName = _pIter->szFoundPath;

#if defined(_WIN32)
	_pIter->hFind = INVALID_HANDLE_VALUE;
#endif
}

// SplFileFindClose -- Need to close Iterator when stopping search early
//
SOEXPORT
void SplFileFindClose(SplFileIterator *_pIter)
{
#if defined(_LINUX) || (defined(__APPLE__) && !TARGET_OS_IPHONE)
	closedir(_pIter->dd);

#elif defined(_WIN32)
	FindClose(_pIter->hFind);

#endif

	_pIter->bOpen = FALSE;
}

// SplFileFindFirst
//
// Specified path must not end with the path delimiter
//
// Found path/file stored in Iterator.
//
// Do not need to close Iterator if Find returns FALSE
//
SOEXPORT
SplErc SplFileFindFirst(SplFileIterator *_pIter,const char *_pszFolder, const char *_pszWildcard)
{
    
#if defined(_LINUX) || (defined(__APPLE__) && !TARGET_OS_IPHONE)
	SplDllNameCopy(_pIter->szWildcard, _pszWildcard);
	SplDllNameCopy(_pIter->szFoundPath, _pszFolder);
    SplDllNameCat(_pIter->szFoundPath, SplGetPathDelimiterStr());
	_pIter->pszFoundName = _pIter->szFoundPath + SplDllNameLen(_pIter->szFoundPath);

	if ((_pIter->dd = opendir(_pszFolder)) != NULL) {
        if ((_pIter->dirp = readdir(_pIter->dd)) != NULL) {
            if (fnmatch(_pIter->szWildcard, _pIter->dirp->d_name, FNM_PATHNAME) == 0) {
				_pIter->bOpen = TRUE;
				SplDllNameCopy(_pIter->pszFoundName, _pIter->dirp->d_name);
				return SplErc_OK;
			}
        }

		SplFileFindClose(_pIter);
	}
	return errno;
}
    
#endif // _LINUX
    
#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    // no file system present on iOS
#endif // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    
#ifdef _WIN32    
    SplDllNameCopy(_pIter->szWildcard, _pszWildcard);
	SplDllNameCopy(_pIter->szFoundPath, _pszFolder);
    SplDllNameCat(_pIter->szFoundPath, SplGetPathDelimiterStr());
	_pIter->pszFoundName = _pIter->szFoundPath + SplDllNameLen(_pIter->szFoundPath);
    
	WIN32_FIND_DATAA findFileData;
	_pIter->hFind = FindFirstFileExA(_pIter->szWildcard, FindExInfoStandard, &findFileData, FindExSearchNameMatch, NULL, 0);
	if (_pIter->hFind == INVALID_HANDLE_VALUE)
		return SplGetLastError();

//	if (findFileData.nFileSizeLow && (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
		_pIter->bOpen = TRUE;
		SplDllNameCopy(_pIter->pszFoundName, findFileData.cFileName);
//		}

//		SplFileFindClose(_pIter);

	return SplErc_OK;
}
#endif


// SplFileFindNext
//
// Found path/file stored in Iterator.
//
// Do not need to close Iterator if Find returns FALSE
//
SOEXPORT
SplErc SplFileFindNext(SplFileIterator *_pIter)
{
    
#if defined(_LINUX) || (defined(__APPLE__) && !TARGET_OS_IPHONE)
	if ((_pIter->dirp = readdir(_pIter->dd)) != NULL) {
		if (fnmatch(_pIter->szWildcard, _pIter->dirp->d_name, FNM_PATHNAME) == 0) {
			SplDllNameCopy(_pIter->pszFoundName, _pIter->dirp->d_name);
			return TRUE;
        }
       
        SplFileFindClose(_pIter);
    }
    return FALSE;

#endif // _LINUX
    
#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    // no file system present on iOS
#endif // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    
#ifdef _WIN32
	WIN32_FIND_DATAA findFileData;

	if (!FindNextFileA(_pIter->hFind, &findFileData))
		return SplGetLastError();

	//if (findFileData.nFileSizeLow && (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
 		SplDllNameCopy(_pIter->pszFoundName, findFileData.cFileName);
	//}

	return SplErc_OK;
#endif
}


// SplFileEnumerate
//
SOEXPORT
BOOL SplFileEnumerate(const char *_pszDirectory, const char *_pszWildcard, FileNameMatchProcessor pfnCallback)
{
    
#if defined(_LINUX) || (defined(__APPLE__) && !TARGET_OS_IPHONE)
    // Enumerate all matching files in the directory
    DIR *dd;
    struct dirent *dirp;
    
    dd = opendir(_pszDirectory);
    if (dd != NULL) {
        while ((dirp = readdir(dd)) != NULL) {
            if (fnmatch(_pszWildcard, dirp->d_name, FNM_PATHNAME) == 0)
            {
                SplDllName aNameLib[256];
                SplDllNameCopy(aNameLib, _pszDirectory);
                SplDllNameCat(aNameLib, SplGetPathDelimiterStr());
                SplDllNameCat(aNameLib, dirp->d_name);
                pfnCallback(aNameLib);
            }
        }
        
        closedir(dd);
    }
    
#endif // _LINUX
    
#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    // no file system present on iOS
#endif // defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
    
#ifdef _WIN32
    // Enumerate all matching files in the directory
    
    SplDllName aSearch[MAX_PATH];
    SplDllNameCopy(aSearch, _pszDirectory);
    SplDllNameCat(aSearch, SplGetPathDelimiterStr());
    SplDllNameCat(aSearch, _pszWildcard);
    
    {
        WIN32_FIND_DATAA FindFileData;
        HANDLE hFindFile=FindFirstFileExA(aSearch, FindExInfoStandard, &FindFileData, FindExSearchNameMatch, NULL, 0);
        if (hFindFile != INVALID_HANDLE_VALUE)
            do {
                if (FindFileData.nFileSizeLow && (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0) {
                    SplDllName aPath[MAX_PATH];
                    SplDllNameCopy(aPath, _pszDirectory);
                    SplDllNameCat(aPath, SplGetPathDelimiterStr());
                    SplDllNameCat(aPath, FindFileData.cFileName);
                    pfnCallback(aPath);
                }
                
            } while (FindNextFileA(hFindFile, &FindFileData));
        
        FindClose(hFindFile);
    }
#endif
    
    return TRUE;
}

// SplFileGetWorkingFolder
//
// Get the executable's working folder 
//
SOEXPORT
char *SplFileGetWorkingFolder(char *pszPathRet, int32_t size)
{
#if defined(_WIN32)
	int32_t ret;
	ret = GetCurrentDirectoryA(size, pszPathRet);
	if (ret == 0)
		return NULL;
	
	return pszPathRet;

#elif defined(_LINUX) || defined(__APPLE__)
	return getcwd(pszPathRet, size);
#endif
}


// SplFileSetWorkingFolder
//
// Change the executable's working folder 
//
SOEXPORT
BOOL SplFileSetWorkingFolder(const char *_pszFolder)
{
	int ret;
#if defined(_WIN32)
	ret = SetCurrentDirectoryA(_pszFolder);
	if (ret != 0)
		return TRUE;	// We successfully changed the working folder of the executable
 
	// We couldn't change to the folder
	return FALSE;
	
#elif defined(_LINUX) || defined(__APPLE__)
	ret = chdir(_pszFolder);
	if (ret == 0) {
		// We successfully changed the working directory of the executable
		return TRUE;
	} else {
		// We couldn't change to the directory
		return FALSE;
	}
#endif
}

// SplRealPath -- Convert relative paths to an absolute path
//
// Windows fails to convert to an absolute path when ".." or "." are embedded in the path.
// ie: folder/../filename
//
SplErc SplRealPath(const char *_pszFilePath,char *_pszResolvedRet,size_t _sResolvedRet)
{
#if defined (_WIN32)
	//GetFullPathName(_pszFilePath,_sResolvedRet, _pszResolvedRet,NULL);
	_fullpath(_pszResolvedRet,_pszFilePath, _sResolvedRet);

#elif defined (_LINUX)
	char *pszResolved;
	
	if ((pszResolved = realpath(_pszFilePath,NULL)) != NULL) {
		strncpy(_pszResolvedRet,pszResolved,_sResolvedRet);
		_pszResolvedRet[_sResolvedRet-1] = 0;
	}
	free(pszResolved);
#endif

	return SplGetLastError();
}


// SplIsRelPath -- Determine is path is relative
//
// When no path components the path is also relative
//
int SplIsRelPath(const char *_pszFilePath)
{
#if defined (_WIN32)
	return PathIsRelativeA(_pszFilePath);

#elif defined (_LINUX)

	if (_pszFilePath != NULL)
		while (*_pszFilePath != 0) {
			if (*_pszFilePath == '/')
				break;	// Found leading '/'

			if (*_pszFilePath != ' ')
				return 1;
		}

	return 0;
#else

	return 0;
#endif
}
