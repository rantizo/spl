//  SplSemaphore.h -- Semaphores for counting lock synchronization.
//
// Standardized Platform Layer
//

#pragma once

#ifndef SPL_SplSemaphore_h
#define SPL_SplSemaphore_h

#include "SplTypes.h"
#include "SplExport.h"

// SplSemaphoreCreate -- Initialize a sempahore, with count 0 and max count INT_MAX.
//
SOEXPORT
SplErc SplSemaphoreCreate(SplSemaphore *semaphore, int initial);

// SplSemaphoreCreateEx -- Initialize a semaphore with custom initial count and max count.
//
SOEXPORT
SplErc SplSemaphoreCreateEx(SplSemaphore *semaphore, int initial, int max);

// SplSemaphoreDelete -- Delete a semaphore.
//
SOEXPORT
SplErc SplSemaphoreDelete(SplSemaphore *semaphore);

// SplSemaphoreAcquire -- Acquire a count lock on a semaphore
//
SOEXPORT
SplResult SplSemaphoreAcquire(SplSemaphore *semaphore);

// SplSemaphoreTryAcquire --Try to acquire a count on a semaphore.
//
SOEXPORT
SplResult SplSemaphoreTryAcquire(SplSemaphore *semaphore);

// SplSemaphoreGetCount -- Get the current count for a semaphore.
//
SOEXPORT
int SplSemaphoreGetCount(SplSemaphore *semaphore);

// SplSemaphoreRelease -- Release a count on a semaphore.
//
SOEXPORT
SplErc SplSemaphoreRelease(SplSemaphore *semaphore, int count);

#endif
