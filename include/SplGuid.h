//
// SplGuid.h
//

#pragma once

#include "SplTypes.h"
#include "SplErrors.h"

#if defined(_WIN32)

#include <guiddef.h>

#elif defined(_LINUX) || defined(__APPLE__) || defined(ESP_PLATFORM)

/*************************************************************/
/************* Linux/Mac guiddef.h compatibility *************/
/*************************************************************/
// Required for initial port, but please move to SPL functions
// at bottom when possible


// Requires libuuid
#if !defined( _ANDROID) && !defined(ESP_PLATFORM)
#include <linux/uuid.h>
#endif


//#include <xmmintrin.h>   // for __m128/mmx operations

#ifndef GUID_DEFINED
#define GUID_DEFINED

#pragma pack(push,1)
typedef struct _GUID {
	uint32_t	Data1;
	uint16_t	Data2;
	uint16_t	Data3;
	uint8_t		Data4[8];
} GUID;
#pragma pack(pop)

#endif // GUID_DEFINED

typedef GUID IID;

static const GUID GUID_NULL = {0, 0, 0, {0, 0, 0, 0, 0, 0, 0, 0}};
static const GUID GUID_FFFF = {0xFFFFFFFF, 0xFFFF, 0xFFFF, {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}};

#ifdef __cplusplus
#define REFGUID const GUID &

#if 0//defined(_LINUX)

// Maybe someday this can be called v1ti, for a single 128-bit value.  For now,
// the SSE 4.1 instruction set support gets us to two 64-bit values.  GCC built-ins
// will currently utilize XMM registers to handle as single 128-bit value though.
//typedef long long int v2di __attribute__((vector_size (16)));

static SPL_INLINE BOOL __attribute__ ((optimize("-fomit-frame-pointer"))) InlineIsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
	// Nice, but results in seg fault if things aren't aligned.
    //__m128i vcmp = (__m128i)_mm_cmpneq_ps(*(__m128*)&rguid1, *(__m128*)&rguid2);
    //return (0 == _mm_movemask_epi8(vcmp));
	// This one probably only does 64-bit regs at best, but alignment is not a problem.
	return (*(__uint128_t*)&rguid1 == *(__uint128_t*)&rguid2);
}

static SPL_INLINE BOOL __attribute__ ((optimize("-fomit-frame-pointer"))) IsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
	return InlineIsEqualGUID(rguid1, rguid2);
}

#else
// Fat but fast
static SPL_INLINE BOOL InlineIsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
#if defined(TARGET_OS_IPHONE)
	return !memcmp(&rguid1, &rguid2, sizeof(GUID));
#else
	return (
			((uint32_t *) &rguid1)[0] == ((uint32_t *) &rguid2)[0] &&
			((uint32_t *) &rguid1)[1] == ((uint32_t *) &rguid2)[1] &&
			((uint32_t *) &rguid1)[2] == ((uint32_t *) &rguid2)[2] &&
			((uint32_t *) &rguid1)[3] == ((uint32_t *) &rguid2)[3]);
#endif
}

static SPL_INLINE BOOL IsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
	return !memcmp(&rguid1, &rguid2, sizeof(GUID));
}
#endif // _LINUX

// C++ operators
static SPL_INLINE BOOL operator==(REFGUID rguid1, REFGUID rguid2)
{
	return IsEqualGUID(rguid1, rguid2);
}

static SPL_INLINE BOOL operator!=(REFGUID rguid1, REFGUID rguid2)
{
	return !(rguid1 == rguid2);
}

#else // C

#if defined(_LINUX)

// Maybe someday this can be called v1ti, for a single 128-bit value.  For now,
// the SSE 4.1 instruction set support gets us to two 64-bit values.  GCC built-ins
// will currently utilize XMM registers to handle as single 128-bit value though.
//typedef long long int v2di __attribute__((vector_size (16)));

static SPL_INLINE BOOL __attribute__ ((optimize("-fomit-frame-pointer"))) InlineIsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
	//return ((BOOL)__builtin_ia32_ptestz128(*(v2di*)&rguid1, *(v2di*)&rguid2));
	// Nice, but results in seg fault if things aren't aligned.
    //__m128i vcmp = (__m128i)_mm_cmpneq_ps(*(__m128*)&rguid1, *(__m128*)&rguid2);
    //return (0 == _mm_movemask_epi8(vcmp));
	// This one probably only does 64-bit regs at best, but alignment is not a problem.
	return (*(__uint128_t*)&rguid1 == *(__uint128_t*)&rguid2);
}

static SPL_INLINE BOOL __attribute__ ((optimize("-fomit-frame-pointer"))) IsEqualGUID(REFGUID rguid1, REFGUID rguid2)
{
	return InlineIsEqualGUID(rguid1, rguid2);
}

#else
// Fat but fast
static SPL_INLINE BOOL InlineIsEqualGUID(GUID *guid1, GUID *guid2)
{
	return (
			((uint32_t *) guid1)[0] == ((uint32_t *) guid2)[0] &&
			((uint32_t *) guid1)[1] == ((uint32_t *) guid2)[1] &&
			((uint32_t *) guid1)[2] == ((uint32_t *) guid2)[2] &&
			((uint32_t *) guid1)[3] == ((uint32_t *) guid2)[3]);
}

static SPL_INLINE BOOL IsEqualGUID(GUID *guid1, GUID *guid2)
{
	return !memcmp(guid1, guid2, sizeof(GUID));
}
#endif // _LINUX

#endif // __cplusplus

#endif // _LINUX || __APPLE__

/*****************************************************************/
/************* END Linux/Mac guiddef.h compatibility *************/
/*****************************************************************/

/******************************************************/
/************* Out SplGuidXxx definitions *************/
/******************************************************/

/***********************************/
/************* Common **************/
/***********************************/

typedef GUID SplGuid;

#define SPL_GUID_STRING_LENGTH 37

#if defined(_LINUX)
static SPL_INLINE BOOL __attribute__ ((optimize("-fomit-frame-pointer")))
SplGuidIsEqual(const SplGuid * const guid1, const SplGuid * const guid2) {
	return (IsEqualGUID(*guid1, *guid2));
}
#else
static SPL_INLINE BOOL
SplGuidIsEqual(const SplGuid * const guid1, const SplGuid * const guid2) {
#if defined(TARGET_OS_IPHONE)
	// mediate unaligned memory accesses on iOS
	return !memcmp(guid1, guid2, sizeof(SplGuid));
#else
	return (
			((uint32_t *) guid1)[0] == ((uint32_t *) guid2)[0] &&
			((uint32_t *) guid1)[1] == ((uint32_t *) guid2)[1] &&
			((uint32_t *) guid1)[2] == ((uint32_t *) guid2)[2] &&
			((uint32_t *) guid1)[3] == ((uint32_t *) guid2)[3]);
#endif
}
#endif

#ifdef __cplusplus
// Required for Map
// Return TRUE if rguid1 < rguid2, else FALSE
#if 0//defined(_LINUX)
static SPL_INLINE bool __attribute__ ((optimize("-fomit-frame-pointer"))) operator<(REFGUID rguid1, REFGUID rguid2)
{
	// This one probably only does 64-bit regs at best, but alignment is not a problem.
	return (*(__uint128_t*)&rguid1 < *(__uint128_t*)&rguid2);
}
#else
SPL_INLINE bool operator<(REFGUID rguid1, REFGUID rguid2)
{
	return memcmp(&rguid1, &rguid2, sizeof(GUID)) < 0;
}
#endif // _Linux
#endif // __cplusplus

// Generate a lowercase string from a GUID
// Must provide a valid char array of length >= SPL_GUID_STRING_LENGTH
// Returns the provided string on success
static SPL_INLINE char *
SplGuidToString(char *strBuf, const SplGuid * const guid) {
	
	uint8_t *guid8 = (uint8_t *)guid;

	strBuf[0] = 0;
	#ifdef _WIN32
	sprintf_s(strBuf, SPL_GUID_STRING_LENGTH,
	#else
	sprintf(strBuf,                           
	#endif
		"%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",			  
		guid8[0],
		guid8[1],
		guid8[2],
		guid8[3],

		guid8[4],
		guid8[5],

		guid8[6],
		guid8[7],

		guid8[8],
		guid8[9],

		guid8[10],
		guid8[11],
		guid8[12],
		guid8[13],
		guid8[14],
		guid8[15] );
	return strBuf;

}

// Generate an uppercase string from a GUID
// Must provide a valid char array of length >= SPL_GUID_STRING_LENGTH
// Returns the provided string on success
static SPL_INLINE char *
SplGuidToStringUppercase(char *strBuf, const SplGuid * const guid) {
	
	uint8_t *guid8 = (uint8_t *)guid;

	strBuf[0] = 0;
	#ifdef _WIN32
	sprintf_s(strBuf, SPL_GUID_STRING_LENGTH, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
	#else
	sprintf(strBuf, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
	#endif
					guid8[0],
					guid8[1],
					guid8[2],
					guid8[3],

					guid8[4],
					guid8[5],

					guid8[6],
					guid8[7],

					guid8[8],
					guid8[9],

					guid8[10],
					guid8[11],
					guid8[12],
					guid8[13],
					guid8[14],
					guid8[15] );
	return strBuf;

}

static BOOL
SplGuidIsValidHexChar( char c )
{
    return ( ( c >= '0' && c <= '9' ) ||
             ( c >= 'A' && c <= 'Z' ) ||
             ( c >= 'a' && c <= 'z' ) );
}

// Generate a GUID from a string
// returns SplErrorOk or SplErrorFailure
static inline SplErrorCode
SplGuidFromString(SplGuid *guid, const char * const strBuf)
{
	uint8_t *guid8 = (uint8_t *)guid;
	char tmpStr[3];
	int strPosition = 0;
	int tmpPosition = 0;
	int guidPosition = 0;
	int tmpVal;

	if (strlen(strBuf) != (SPL_GUID_STRING_LENGTH - 1)) {
		return SplErc_Failure;
	}

	tmpStr[2] = 0;

	// Convert a byte at a time
	while (strPosition < (SPL_GUID_STRING_LENGTH - 1)) {
		// Ensure this is a valid hex character
		if ( SplGuidIsValidHexChar(strBuf[strPosition]) )
		{
			tmpStr[tmpPosition++] = strBuf[strPosition];
		}

		if (tmpPosition == 2) {
			// Two characters, ready to process
#ifdef _WIN32
			sscanf_s(tmpStr, "%02x", &tmpVal);
#else
			sscanf(tmpStr, "%02x", &tmpVal);
#endif
			guid8[guidPosition++] = (uint8_t)tmpVal;
			tmpPosition = 0;
		}

		strPosition++;
	}

	if (guidPosition == sizeof(SplGuid)) {
		return SplErrorOk;
	}
    
    return SplErc_Failure;
}

static SPL_INLINE void
SplGuidCreate(SplGuid *guid) {
#if defined (_ANDROID)
	// No libuuid in Android, use a random number generator
	// NOTE: Random seeded in SplOpen()
	// This isn't a very good random uuid but should do for our purposes
	guid->Data1 = random();
	*(uint32_t *)&guid->Data2 = random();
	*(uint32_t *)&guid->Data4[0] = random();
	*(uint32_t *)&guid->Data4[4] = random();
#elif defined(_LINUX) || defined(__APPLE__)
	uuid_generate((uint8_t *)guid);
#elif defined (_WIN32)
	CoCreateGuid(guid);
#endif
}
