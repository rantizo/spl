// SplThread.h -- Thread management
// 
// Standardized Platform Layer
//

#pragma once

#ifndef __SplThread_h
#define __SplThread_h

#include "SplTypes.h"
#include "SplExport.h"
#include "SplErrors.h"


// SplThreadCreate -- Create a thread
//
SOEXPORT
SplErrorCode SplThreadCreate(SplThread **_ppThread, SplThreadProc _threadProc, void *_parameter, const char *_pszName, size_t _nStackSize);

SOEXPORT
void SplThreadSetName(SplThreadIdType threadId, const char* name);

// SplThreadGetCurrent -- Get the handle to the thread the invoking code is executing on
//
SOEXPORT
SplThread * SplThreadGetCurrent();

// SplThreadIsSelf -- Determine if a given thread handle represents the executing thread
//
SOEXPORT
int SplThreadIsSelf(SplThread *_ppThread);

// SplThreadGetName -- Get the string name for a given thread
//
SOEXPORT
void SplThreadGetName(SplThread *_ppThread, char* p, uint32_t cb);

// SplThreadGetID -- Get the ID of the given thread
//
SOEXPORT
int32_t SplThreadGetID(SplThread *_ppThread);

// SplThreadJoin -- Block until a thread completes
//
SOEXPORT
SplErrorCode SplThreadJoin(SplThread *_ppThread);

// SplThreadRelease -- Release an allocated thread pointer
//
SOEXPORT
SplErrorCode SplThreadRelease(SplThread *_ppThread);

// SplThreadSetPriority -- Set the execution priority for a given thread
//
SOEXPORT
SplErrorCode SplThreadSetPriority(SplThread *_ppThread, SplThreadPriority priority);

// SplThreadIsRunning -- Determine if a given thread is still running
//
SOEXPORT
int SplThreadIsRunning(SplThread *_ppThread);

#if	0	// No Threadmap support
	// Not exported, use SplOpen() and SplClose()

	SplErrorCode SplThreadOpen(); ///< @private
	SplErrorCode SplThreadClose(); ///< @private

#if 0
// SplThreadIdentStr -- Get the identification string for a given thread
//
	SOEXPORT
		SplErrorCode SplThreadIdentStr(SplThread *_ppThread, char* p, uint32_t cb);
#endif

#endif


#endif		// __SplThread_h
