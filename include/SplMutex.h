// SplMutex.h -- Mutexes for binary lock synchronization
//
//  Standardized Platform Layer
//

#pragma once

#ifndef SPL_SplMutex_h
#define SPL_SplMutex_h

#include "SplTypes.h"
#include "SplExport.h"


// SplMutexCreate -- Create a mutex
//
SOEXPORT
SplErrorCode SplMutexCreate(SplMutex *mutex);


// SplMutexDelete -- Delete a mutex
//
SOEXPORT
SplErrorCode SplMutexDelete(SplMutex *mutex);


// SplMutexLock -- Acquire a mutex lock, blocking until it's available
//
SOEXPORT
SplResult SplMutexLock(SplMutex *mutex, uint32_t timeout_ms);


// SplMutexTryLock -- Try to acquire a mutex lock without blocking
//
SOEXPORT
SplResult SplMutexTryLock(SplMutex *mutex);


// SplMutexUnlock -- Unlock a mutex
SOEXPORT
SplErrorCode SplMutexUnlock(SplMutex *mutex);

#endif
