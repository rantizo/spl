// SplAtomic.h -- Atomic operations
//
// Standardized Platform Layer
//

#pragma once
#include "SplTypes.h"

#if defined(ESP_PLATFORM)
// The portENTER_CRITICAL/PortEXIT_CRITCAL macros disable/enable interrupts on the current thread
// and will spin-lock any other threads that attempt to access the same portMUX_TYPE spinlock

static portMUX_TYPE splAtomic_spinlock = portMUX_INITIALIZER_UNLOCKED;

#endif

#if 0 //defined(_WIN32)
// Let's make sure we have intrinsic functions 
// available for all of these operations
// If so we probably have a good clue that Get and 
// Set operations can simply be assignments and variables
// accessess
// If the compiler reports them as unavailable we might
// need to add SplAtomicSet and SplAtomicGet
// If so, same code is #if 0'd at the bottom of the file
#pragma intrinsic(_InterlockedIncrement)
#pragma intrinsic(_InterlockedDecrement)
#pragma intrinsic(_InterlockedExchangeAdd)
#pragma intrinsic(_InterlockedCompareExchange)
#endif



// SplAtomicSet -- Set SplAtomic to a value
//
static SPL_INLINE
SplAtomic SplAtomicSet(SplAtomic *atomic, int32_t set)
{
#if defined (_WIN32)
	return InterlockedExchange(atomic, (LONG)set);

#elif defined(_LINUX) || defined(__APPLE__)
	return (*atomic = set);

#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	SplAtomic value = (*atomic);
	*atomic = set;
	portENTER_CRITICAL(&splAtomic_spinlock);
	return value;

#endif
}

// SplAtomicInc -- Increment the value of a SplAtomic by 1.
//
static SPL_INLINE
SplAtomic SplAtomicInc( SplAtomic *atomic )
{
#if defined (_WIN32)
	return InterlockedIncrement(atomic);

#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_add_and_fetch(atomic, 1);

#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	SplAtomic value = ++(*atomic);
	portENTER_CRITICAL(&splAtomic_spinlock);
	return value;

#endif
}

// SplAtomicDec -- Decrement the value of a SplAtomic by 1.
//
static SPL_INLINE
SplAtomic SplAtomicDec( SplAtomic *atomic )
{
#if defined (_WIN32)
	return InterlockedDecrement(atomic);

#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_sub_and_fetch(atomic, 1);

#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	SplAtomic value = --(*atomic);
	portENTER_CRITICAL(&splAtomic_spinlock);
	return value;

#endif
}

// SplAtomicAdd -- Add a value to a SplAtomic.
//
static SPL_INLINE
SplAtomic SplAtomicAdd( SplAtomic *atomic, int32_t _add )
{
#if defined (_WIN32)
	return InterlockedExchangeAdd(atomic, (LONG)_add);
	
#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_fetch_and_add(atomic, _add);
	
#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	*atomic += _add;
	SplAtomic value = *atomic;
	portENTER_CRITICAL(&splAtomic_spinlock);
	return value;

#endif
}

// SplAtomicZero -- Set a value of an SplAtomic to zero.
//
static SPL_INLINE
SplAtomic SplAtomicZero( SplAtomic *atomic )
{
#if defined (_WIN32)
	return InterlockedExchange(atomic, 0);
	
#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_fetch_and_and(atomic, 0);
	
#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	*atomic = 0;
	portENTER_CRITICAL(&splAtomic_spinlock);
	return 0;

#endif
}

// SplAtomicCompareExchange -- Compare and exchange values atomically.
//
static SPL_INLINE
SplAtomic SplAtomicCompareExchange( SplAtomic *destination, int32_t exchange, int32_t comparand )
{
#if defined (_WIN32)
	return InterlockedCompareExchange(destination, (long)exchange, (long)comparand);
	
#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_val_compare_and_swap_4(destination, comparand, exchange);
	
#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	SplAtomic inital = *destination;
	if (*destination == comparand)
		*destination = exchange;
	portENTER_CRITICAL(&splAtomic_spinlock);
	return inital;

#endif
}

// SplAtomicCompareExchangePtr -- Compare and exchange pointers atomically.
//
static SPL_INLINE
SplAtomicPtr SplAtomicCompareExchangePtr( SplAtomicPtr *destination, intptr_t exchange, intptr_t comparand )
{
#if defined (_WIN32)
	return (intptr_t)InterlockedCompareExchangePointer((PVOID volatile *)destination, (PVOID)exchange, (PVOID)comparand);
	
#elif defined(_LINUX) || defined(__APPLE__)
	return __sync_val_compare_and_swap(destination, comparand, exchange);
	
#elif defined(ESP_PLATFORM)
	portENTER_CRITICAL(&splAtomic_spinlock);
	SplAtomicPtr inital = *destination;
	if (*destination == comparand)
		*destination = exchange;
	portENTER_CRITICAL(&splAtomic_spinlock);
	return inital;

# endif
}

