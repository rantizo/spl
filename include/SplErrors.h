// SplErrors.h -- Standardized Platform Layer
// 
// SPL error codes
// Return codes for type SplErrorCode
//

#pragma once

#define SplErrorOk (0)
#define SplErrorFailure (-1)

#define SplErc_OK		(0)
#define SplErc_Failure	(-1)

#define SplErc_Signaled		(1)
#define SplErc_Timeout		(2)
#define SplErc_Cancelled	(3)
#define SplErc_Closed		(4)

#ifdef _WIN32
#define SplErrorIoPending (ERROR_IO_PENDING)
#endif

#if defined(_LINUX) || defined(__APPLE__)
#define SplErrorIoPending (0x0FFFFFFFF)
#endif

#define SplErrorNotEnoughMemory          (-8)
#define SplErrorInvalidBlock             (-9)
#define SplErrorInvalidServiceAccount    (-10)
#define SplErrorUnsupported              (-11)

#define SplErrorSocketSetFull (-1000)

// Key not found on key/value pair lookup
#define SplErrorAppdataKeyNotFound (-2000)
// Invalid data found in key or value
#define SplErrorAppdataInvalidData (-2001)
// Provided buffer not large enough to contain value
#define SplErrorAppdataInvalidSize (-2002)
// Key/value file not found
#define SplErrorAppdataInvalidFile (-2003)

#define SplErrorProcessInvalidChildArguments	(-3000)
#define SplErrorEof								(-3001)	// end of file
#define SplErrorTimeout							(-3002)
#define SplErrorCanceled						(-3003)
#define SplErrorPipeBroken						(-3004)
#define SplErrorBadParameter                    (-3005)
#define SplErrorPipeClosed                      (-3006)

