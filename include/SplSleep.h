// SplSleep.h
//
// Standardized Platform Layer
//

#pragma once

#ifndef __SplSleep_h
#define __SplSleep_h

#include "SplTypes.h"


// Sleep(0) must always do a thread yield
//

#if defined(_LINUX) || defined(__APPLE__)
/*********************************/
/************* Linux *************/
/*********** Mac OS X ************/
/*********************************/

static SPL_INLINE void SplSleep(uint32_t msSleep)
{
	struct timespec ts;
	struct timespec remaining;

	ts.tv_sec = msSleep / 1000;
	ts.tv_nsec = 1000000 * (msSleep % 1000);

	nanosleep(&ts, &remaining);
}

#elif (defined _WIN32 || defined __WIN32__)
/***********************************/
/************* Windows *************/
/***********************************/

// Maps directly to Sleep(dwMs)
#ifdef __cplusplus
	#define SplSleep(x) ::Sleep(x)
#else
	#define SplSleep(x) Sleep(x)
#endif


#elif defined(ESP_PLATFORM)
//********************************/
/************* ESP32 *************/
//********************************/

#include "lwip/sys.h"

static SPL_INLINE void SplSleep(uint32_t _ms)
{
	TickType_t nTicks = pdMS_TO_TICKS(_ms);

	if (nTicks > 0)
		vTaskDelay(nTicks);
	else
		taskYIELD();
}

#endif

#endif	// __SplSleep_h
