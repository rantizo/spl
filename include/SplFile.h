//  SplFile.h
//

#ifndef SPL_SplFile_h
#define SPL_SplFile_h

#ifdef _LINUX
__asm__(".symver fcntl,fcntl@GLIBC_2.4");
#endif

#include "SplTypes.h"
#include "SplExport.h"
#include <fcntl.h>

#if defined(_LINUX) || defined(__APPLE__)

#ifndef O_NOTRANS
# define O_NOTRANS 0
#endif
#include <stdbool.h>
#endif

enum {
	SplErc_FileAccess = EACCES,		// Exists but not accessable
	SplErc_FileExist  = EEXIST,		// File already exists
	SplErc_FileIntr   = EINTR,		// Operation was interrupted
	SplErc_FileIsDir  = EISDIR,		// Is a directory
	SplErc_FileMFile  = EMFILE,		// Max Files open
	SplErc_FileNFile  = ENFILE,		// No more files at the moment
	SplErc_FileNoEnt  = ENOENT,		// Does not exist
	SplErc_FileNoSpc  = ENOSPC,		// No Space
	SplErc_FileNxio   = ENXIO,		// Flags not compatible 
	SplErc_FileRofs   = EROFS,		// File on Read-only file system
	SplErc_FileBadF   = EBADF,		// Bad file descriptor
	SplErc_FileEIO	  = EIO,		// Hardware error
	SplErc_FileAgain  = EAGAIN,		// No data when non-blocking
	SplErc_FileInVal  = EINVAL,		// Unaligned offsets
	SplErc_FileSPipe  = ESPIPE,		// Positioning not allowed on pipes
	SplErc_FileFBig   = EFBIG,		// File would be too big
	SplErc_FilePipe   = EPIPE,		// Attempt to write pipe that is not opened for write
	SplErc_FileNoLck  = ENOLCK,		// Run out of file locking resources
	SplErc_FileDeadLk = EDEADLK,	// Locking dead-lock
	SplErc_FileEPerm  = EPERM,		// Not owner or priveleged
	SplErc_FileErofs  = EROFS		// File on a read-only file system
};

enum { // Open flags/Modes
	SplFileOpenCreat  = O_CREAT,
	SplFileOpenExcl   = O_EXCL,
	SplFileOpenTrunc  = O_TRUNC,
	SplFileOpenRdOnly = O_RDONLY,
	SplFileOpenWrOnly = O_WRONLY,
	SplFileOpenRdWr   = O_RDWR,
#if defined(_LINUX) || defined(__APPLE__)
	SplFileOpenBinary = O_NOTRANS
#elif defined(_WIN32)
	SplFileOpenBinary = O_BINARY
#endif
};

#if defined(_LINUX) || defined(__APPLE__)
enum { // Access Permissions
	SplFileAccessRead	  = S_IRUSR,
	SplFileAccessWrite  = S_IWUSR,
};

#elif defined(_WIN32)
enum { // Access Permissions
	SplFileAccessRead	= _S_IREAD,
	SplFileAccessWrite  = _S_IWRITE,
};
#endif

enum { // Whence
	SplFileSeekSet = SEEK_SET,
	SplFileSeekCur = SEEK_CUR,
	SplFileSeekEnd = SEEK_END
};


#if defined(_WIN32)
typedef __int64 off64_t;	// Windows
#endif

struct SplFileTimes {
	SplTime64 timeAccess;
	SplTime64 timeModified;
};

#if defined(_LINUX) || defined(__APPLE__)

#define SplFileStat64		stat64


#define SPL_ISDIR			S_ISDIR
#define SPL_ISREG			S_ISREG
#define SPL_ISCHR			S_ISCHR
#define SPL_ISFIFO			S_ISFIFO
#define SPL_ISLNK			S_ISLNK

// st_mode bits
#define _SPL_IFMT			S_IFMT		// File type mask
#define _SPL_IFDIR			S_IFDIR		// Directory
#define _SPL_IFCHR			S_IFCHR		// Character special
#define _SPL_IFIFO			S_IFIFO		// Pipe
#define _SPL_IFREG			S_IFREG		// Regular

#define _SPL_IREAD			S_IREAD 	// Read permission, owner
#define _SPL_IWRITE			S_IWRITE	// Write permission, owner
#define _SPL_IEXEC			S_IEXEC		// Execute/search permission, owner

#elif defined(_WIN32)
#define SplFileStat64		_stat64

#define SPL_ISDIR(mode)		((mode & _S_IFMT) == _S_IFDIR)
#define SPL_ISREG(mode)		((mode & _S_IFMT) == _S_IFDIR)
#define SPL_ISCHR(mode)		((mode & _S_IFMT) == _S_IFCHR)
#define SPL_ISFIFO(mode)	((mode & _S_IFMT) == _S_IFIFO)
#define SPL_ISLNK(mode)		FALSE

// st_mode bits
#define _SPL_IFMT			_S_IFMT		// File type mask
#define _SPL_IFDIR			_S_IFDIR	// Directory
#define _SPL_IFCHR			_S_IFCHR	// Character special
#define _SPL_IFIFO			_S_IFIFO	// Pipe
#define _SPL_IFREG			_S_IFREG	// Regular

#define _SPL_IREAD			_S_IREAD 	// Read permission, owner
#define _SPL_IWRITE			_S_IWRITE	// Write permission, owner
#define _SPL_IEXEC			_S_IEXEC	// Execute/search permission, owner

#endif


#if defined(_LINUX) || (defined(__APPLE__) && !TARGET_OS_IPHONE)
typedef struct _SplFileIterator {
	DIR *dd;
	struct dirent *dirp;
	bool bOpen;
	char szWildcard[SplMaxPath];
	char szFoundPath[SplMaxPath];
	char *pszFoundName;		// -> file name in szFoundPath
} SplFileIterator;

#elif defined(_WIN32)
typedef struct _SplFileIterator {
	HANDLE hFind;
	BOOL bOpen;
	char szWildcard[SplMaxPath];
	char szFoundPath[SplMaxPath];
	char *pszFoundName;		// -> file name in szFoundPath
} SplFileIterator;

#endif


//-------
// Files
//-------

SOEXPORT
SplErc SplFileOpen(int *_pFdRet,const char *_pszFileName,int _flags,int _permission);

SOEXPORT
SplErc SplFileClose(int _fd);

SOEXPORT
SplErc SplFileCommit(int _fd);

SOEXPORT
SplErc SplFileRead(int _fd,void *_pBuf,size_t _sBuf,size_t *_pcReadRet);

SOEXPORT
SplErc SplFileWrite(int _fd,const void *_pBuf,size_t _cBuf,size_t *_pcWriteRet);

SOEXPORT
SplErc SplFileSeek(int _fd,off_t  _offset, int _whence, off_t *_pOffsetRet);


SOEXPORT
SplErc SplFileSeek64(int _fd,off64_t  _offset, int _whence, off64_t *_pOffsetRet);

SOEXPORT
SplErc SplFileTell(int _fd,off_t *_pOffRet);

SOEXPORT
SplErc SplFileTell64(int _td,off64_t *_pOffRet);

SOEXPORT
SplErc SplFileDup(int _fd, int *_pFdNewRet);

SOEXPORT
SplErc SplFileDup2(int _fd, int _fd2, int *_pFdNewRet);

SOEXPORT
SplErc SplFileGetStatus(const char *_pszFileName, struct SplFileStat64 *_pStatRet);

SOEXPORT
SplErc SplFileGetFStatus(int _fd, struct SplFileStat64 *_pStatRet);

SOEXPORT
SplErc SplFileGetFSize(int _fd, off64_t *_pSizeRet);

SOEXPORT
SplErc SplFileGetSize(const char *_pszFileName, off64_t *_pSizeRet);

SOEXPORT
SplErc SplFileGetFTimes(int _fd, struct SplFileTimes *_pTimes);

SOEXPORT
SplErc SplFileSetFTimes(int _fd, struct SplFileTimes *_pTimes);

SOEXPORT
SplErc SplFileGetTimes(const char *_pszFileName,struct SplFileTimes *_pTimes);

SOEXPORT
SplErc SplFileSetTimes(const char *_pszFileName,struct SplFileTimes *_pTimes);

SOEXPORT
SplErc SplFileFTruncate(int _fd, off64_t _len);

SOEXPORT
SplErc SplFileTruncate(const char *_pszFileName, off64_t _len);


//---------
// Folders
//---------

SOEXPORT
SplErc SplFileFolderCreate(const char *_pszPath,BOOL _bCreateParentFolders);

SOEXPORT
SplErc SplFileFolderDelete(const char *_pszDirectory);

SOEXPORT
SplErc SplFileDelete(const char *_pszFileName);

SOEXPORT
SplErc SplFileMove(const char *_pszFrom, const char *_pszTo);

SOEXPORT
SplErc SplFileCopy(const char *_pszFrom, const char *_pszTo);

SOEXPORT
SplErc SplFileCopyToPath(const char *_pszFrom, const char *_pszToPath);

SOEXPORT
BOOL SplFileExists(const char *_pszFileName);

SOEXPORT
BOOL SplFolderExists(const char *_pszDirectory);

SOEXPORT
SplErc SplFileFindFirst(SplFileIterator *_pIter,const char *_pszFolder, const char *_pszWildcard);

SOEXPORT
SplErc SplFileFindNext(SplFileIterator *_pIter);

SOEXPORT
void SplFileFindInit(SplFileIterator *_pIter);

SOEXPORT
void SplFileFindClose(SplFileIterator *_pIter);

typedef void (*FileNameMatchProcessor)(const char *_pszFilePath);

SOEXPORT
BOOL SplFileEnumerate(const char *_pszDirectory, const char *_pszWildcard, FileNameMatchProcessor pfnCallback);

SOEXPORT
char *SplFileGetWorkingFolder(char *_pszPathRet, int32_t size);

SOEXPORT
BOOL SplFileSetWorkingFolder(const char *_pszDirectory);

SOEXPORT
SplErc SplRealPath(const char *_pszFilePath,char *_pszResolved,size_t _sResolvedRet);

SOEXPORT
int SplIsRelPath(const char *_pszFilePath);

#endif
