// SplInts.h -- Standardized Platform Layer
// 
// Integer types by platform
//

#pragma once

#ifndef __SplInts_h
#define __SplInts_h

// Basic integer types
// Defined here for forward declaration
#if defined(_LINUX) || defined(__APPLE__)

// C99 integer types and format conversions
// inttypes.h will bring in stdint.h as well
// Define __STDC_LIMIT_MACROS first so that inttypes.h will bring in min/max definitions
#define __STDC_LIMIT_MACROS
#include <inttypes.h>
#include <stddef.h>
//#include <semaphore.h>
//#include <math.h>

#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"

// Windows compatibility types
// Please use for legacy code only
typedef void *LPSECURITY_ATTRIBUTES;
typedef size_t SIZE_T;
typedef void *LPVOID;
typedef char CHAR;
typedef uint16_t WORD;
// Use 32 bit for all longs since that is what Windows uses and
// we just have these for compatibility
typedef int32_t LONG;
//typedef uint32_t ULONG;
typedef uint32_t DWORD;
typedef unsigned int UINT;
typedef DWORD *LPDWORD;
typedef void* HANDLE;
typedef int64_t __int64;
typedef int64_t LONGLONG;
typedef uint64_t ULONGLONG;
typedef const char *LPCTSTR;
typedef signed char BOOL;

typedef uintptr_t WPARAM;
typedef intptr_t  LPARAM;


#elif defined(_WIN32)
/*
	MSVC++ 14.1 _MSC_VER == 1911 (Visual Studio 2017)
	MSVC++ 14.1 _MSC_VER == 1910 (Visual Studio 2017)
	MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)
	MSVC++ 12.0 _MSC_VER == 1800 (Visual Studio 2013)
	MSVC++ 11.0 _MSC_VER == 1700 (Visual Studio 2012)
	MSVC++ 10.0 _MSC_VER == 1600 (Visual Studio 2010)
	MSVC++ 9.0  _MSC_VER == 1500 (Visual Studio 2008)
*/

#if (_MSC_VER >= 1900)
#include <inttypes.h>

#define C99_TYPES_

#else
// C99-lite compatibility
// Would be nice to bring in full stdint.h and inttypes.h
#define C99_TYPES_

// Exact-width width types
typedef signed char int8_t;
typedef short int int16_t;
typedef long int int32_t;
typedef long long int int64_t;
typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long int uint64_t;

// Greatest width types
typedef long long int intmax_t;
typedef unsigned long long int uintmax_t;

typedef INT_PTR  intptr_t;
typedef UINT_PTR uintptr_t;

//
// Min/Max type values
//
#define INT8_MIN        (-127i8 - 1)
#define INT16_MIN       (-32767i16 - 1)
#define INT_MIN         (-2147483647 - 1)
#define INT32_MIN       (-2147483647i32 - 1)
#define INT64_MIN       (-9223372036854775807i64 - 1)
#define INT128_MIN      (-170141183460469231731687303715884105727i128 - 1)

#define SHORT_MIN       (-32768)
#define LONG_MIN        (-2147483647L - 1)
#define LONGLONG_MIN    (-9223372036854775807i64 - 1)
#define LONG64_MIN      (-9223372036854775807i64 - 1)

#define INT8_MAX        127i8
#define UINT8_MAX       0xffui8
#define BYTE_MAX        0xff
#define SHORT_MAX       32767
#define INT16_MAX       32767i16
#define USHORT_MAX      0xffff
#define UINT16_MAX      0xffffui16
#define WORD_MAX        0xffff
#define INT_MAX         2147483647
#define INT32_MAX       2147483647i32
#define UINT_MAX        0xffffffff
#define UINT32_MAX      0xffffffffui32
#define INT64_MAX       9223372036854775807i64
#define UINT64_MAX      0xffffffffffffffffui64
#define INT128_MAX      170141183460469231731687303715884105727i128
#define UINT128_MAX     0xffffffffffffffffffffffffffffffffui128

#define LONG_MAX        2147483647L
#define ULONG_MAX       0xffffffffUL
#define DWORD_MAX       0xffffffffUL
#define LONGLONG_MAX    9223372036854775807i64
#define LONG64_MAX      9223372036854775807i64
#define ULONGLONG_MAX   0xffffffffffffffffui64
#define DWORDLONG_MAX   0xffffffffffffffffui64
#define ULONG64_MAX     0xffffffffffffffffui64
#define DWORD64_MAX     0xffffffffffffffffui64


/*
// Limits of exact-width types
#define INT8_MAX	127
#define INT16_MAX	32767
#define INT32_MAX	2147483647
#define INT64_MAX	9223372036854775807LL

#define INT8_MIN	-128
#define INT16_MIN	-32768
#define INT32_MIN	(-INT32_MAX-1)
#define INT64_MIN	(-INT64_MAX-1)

#define UINT8_MAX	255
#define UINT16_MAX	65535
#define UINT32_MAX	4294967295U
#define UINT64_MAX	18446744073709551615ULL

// Limits of greatest width types
#define INTMAX_MIN	INT64_MIN
#define INTMAX_MAX	INT64_MAX

#define UINTMAX_MAX	UINT64_MAX
*/
#endif


#elif defined(ESP_PLATFORM)

#include <stddef.h>
#include <inttypes.h>

// Windows compatibility types
// Please use for legacy code only

typedef void *LPSECURITY_ATTRIBUTES;
typedef size_t SIZE_T;
typedef void *LPVOID;
typedef char CHAR;
typedef uint16_t WORD;
// Use 32 bit for all longs since that is what Windows uses and
typedef int32_t LONG;
//typedef uint32_t ULONG;
typedef uint32_t DWORD;
typedef unsigned int UINT;
typedef DWORD *LPDWORD;
typedef void* HANDLE;
typedef int64_t __int64;
typedef int64_t LONGLONG;
typedef uint64_t ULONGLONG;
typedef const char *LPCTSTR;

typedef signed char BOOL;
typedef uintptr_t WPARAM;
typedef intptr_t  LPARAM;


#endif

#endif	// __SplInts_h
