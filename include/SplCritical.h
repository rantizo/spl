// SplCritical.h -- CriticalSections for binary lock synchronization.
//
//  Standardized Platform Layer
//

#pragma once

#ifndef __SplCritial_h
#define __SplCritial_h

#include "SplTypes.h"
#include "SplErrors.h"

#if defined(ESP_PLATFORM)

static portMUX_TYPE ctor = portMUX_INITIALIZER_UNLOCKED;

#endif


// SplCriticalCreate -- Create a critical section
//
static SPL_INLINE
SplErc SplCriticalCreate( SplCritical *_pCritical)
{
#if defined (_WIN32)
	InitializeCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	// Set as a recursive mutex, allowing a single thread to multiply lock
	// This will require the equivalent number of unlocks to release
	pthread_mutexattr_t mutex_attr;
	pthread_mutexattr_init(&mutex_attr);
	pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_RECURSIVE);
	return (pthread_mutex_init((pthread_mutex_t*)_pCritical, &mutex_attr) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	*_pCritical = (portMUX_TYPE*) malloc(sizeof(portMUX_TYPE));
	*(*_pCritical) = ctor;
	return SplErc_OK;

#endif
}


// SplCriticalDelete -- Delete a critical section
//
static SPL_INLINE
SplErc SplCriticalDelete( SplCritical *_pCritical)
{
#if defined (_WIN32)
	DeleteCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	return (pthread_mutex_destroy((pthread_mutex_t*)_pCritical) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	free(*_pCritical);
	return SplErc_OK;

#endif
}

// SplCriticalEnter -- Enter a critical section
//
static SPL_INLINE
SplErc SplCriticalEnter( SplCritical *_pCritical)
{
#if defined (_WIN32)
	EnterCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	return (pthread_mutex_lock((pthread_mutex_t*)_pCritical) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	taskENTER_CRITICAL(*_pCritical);
	return SplErc_OK;

#endif
}

// SplCriticalEnterFromISR -- Enter a critical section within an ISR
//
static SPL_INLINE
SplErc SplCriticalEnterFromISR(SplCritical *_pCritical)
{
#if defined (_WIN32)
	EnterCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	return (pthread_mutex_lock((pthread_mutex_t*)_pCritical) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	taskENTER_CRITICAL_ISR(*_pCritical);
	return SplErc_OK;

#endif
}

#if 0
// SplCriticalTryEnter --Try to enter a critical section without blocking
//
static SPL_INLINE
SplResult SplCriticalTryEnter( SplCritical *_pCritical )
{
	// Enter a critical section
#if defined (_WIN32)
	// TryEnterCriticalSection returns 
	// non-zero on success or if this thread already owns it
	// zero on fail
	return (TryEnterCriticalSection(_pCritical) ? SplLockAcquired : SplLockFailed);

#elif defined (_LINUX) || defined(__APPLE__)
	// pthread_mutex_trylock returns 
	// zero on success or if this thread already owns it
	// non-zero on fail
	return (pthread_mutex_trylock((pthread_mutex_t*)_pCritical) ? SplLockFailed : SplLockAcquired);

#elif defined(ESP_PLATFORM)
	return xSemaphoreTakeRecursive(*_pCritical, 0) == pdPASS ? SplLockAcquired : SplLockFailed;

#endif
}
#endif

// SplCriticalLeave -- Leave a critical section
//
static SPL_INLINE
SplErc SplCriticalLeave( SplCritical *_pCritical )
{
#if defined (_WIN32)
	LeaveCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	return (pthread_mutex_unlock((pthread_mutex_t*)_pCritical) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	taskEXIT_CRITICAL(*_pCritical);
	return SplErc_OK;
#endif
}

// SplCriticalLeaveFromISR -- Leave a critical section
//
static SPL_INLINE
SplErc SplCriticalLeaveFromISR(SplCritical *_pCritical)
{
#if defined (_WIN32)
	LeaveCriticalSection(_pCritical);
	return SplErc_OK;

#elif defined (_LINUX) || defined(__APPLE__)
	return (pthread_mutex_unlock((pthread_mutex_t*)_pCritical) ? SplErrorFailure : SplErc_OK);

#elif defined(ESP_PLATFORM)
	taskEXIT_CRITICAL_ISR(*_pCritical);
	return SplErc_OK;
#endif
}

#endif	// __SplCritial_h

