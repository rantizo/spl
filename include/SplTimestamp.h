// SplTimestamp.h --  Timestamp functions
// 
// Standardized Platform Layer
//
// Resolution will vary depending on system
//

#pragma once

#ifndef __SplTimestamp_h
#define __SplTimestamp_h

#include "SplTypes.h"

// Types

#if defined(_LINUX) || defined(__APPLE__)
typedef struct timeval SplTimestamp; 

#elif defined(_WIN32)
#include <time.h>
#include <WinSock2.h>
typedef struct timeval SplTimestamp; 

#endif

#if defined(_WIN32)

#if defined(_MSC_VER) || defined(_MSC_EXTENSIONS)
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000Ui64
#else
  #define DELTA_EPOCH_IN_MICROSECS  11644473600000000ULL
#endif

// This returns UNIX epoch time
// Not as efficient as possible in theory but not too bad
static SPL_INLINE
void gettimeofday( SplTimestamp *timestamp, void *timezone )
{
	FILETIME ft;
	uint64_t usecs;
	SPL_UNUSED(timezone);

	// The call to GetSystemTimeAsFileTime typically requires 10 ns to 15 ns.
	GetSystemTimeAsFileTime(&ft);
	// Windows 8 introduces the function GetSystemTimePreciseAsFileTime(). Counterpart to the linux gettimeofday() function?
	//GetSystemTimePreciseAsFileTime(&ft);

	usecs  = (((uint64_t)ft.dwHighDateTime) << 32);
	usecs |= ft.dwLowDateTime;

	// To microseconds
	usecs /= 10;

	// And to Epoch, why not
	usecs -= DELTA_EPOCH_IN_MICROSECS;

	timestamp->tv_sec  = (int32_t)(usecs / 1000000UL);
	timestamp->tv_usec = (int32_t)(usecs % 1000000UL);
}

#endif // _WIN32

// Clear the timestamp value
static SPL_INLINE
void SplTimestampClear( SplTimestamp *timestamp )
{
	memset(timestamp, 0, sizeof(SplTimestamp));
}

// set timestamp value to current time
static SPL_INLINE
void SplTimestampSet( SplTimestamp *timestamp )
{
	gettimeofday(timestamp, NULL);
}

// set timestamp value to current time plus provided offset in seconds (positive)
static SPL_INLINE
void SplTimestampSetOffset( SplTimestamp *timestamp, 
								   uint32_t offset_seconds )
{
	gettimeofday(timestamp, NULL);
	timestamp->tv_sec += offset_seconds;
}

// calculate the difference between two timestamps and return as a new value
// result = from - subtract
// assumes from is larger than subtract
static SPL_INLINE
void SplTimestampDelta( SplTimestamp *result, 
							   SplTimestamp *from, 
							   SplTimestamp *subtract )
{
	result->tv_sec  = from->tv_sec  - subtract->tv_sec;
	result->tv_usec = from->tv_usec - subtract->tv_usec;

	if (result->tv_usec < 0) {
		result->tv_sec--;
		result->tv_usec += 1000000;
	}
}

// calculate the difference between a (future) timestamp and the current time and return as a new value
// result = from - current time
// assumes from is larger than current time
static SPL_INLINE
void SplTimestampDeltaNow( SplTimestamp *result,
							   SplTimestamp *from)
{
    SplTimestamp tTimeNow;
    SplTimestampSet(&tTimeNow);
    SplTimestampDelta(result, from, &tTimeNow);
}

// subtract one timestamp from another 
// from = from - subtract
// updated value is returned in from
// assumes from is larger than subtract
static SPL_INLINE
void SplTimestampSubtract( SplTimestamp *from, 
							   SplTimestamp *subtract )
{
	from->tv_sec -= subtract->tv_sec;
	from->tv_usec -= subtract->tv_usec;

	if (from->tv_usec < 0) {
		from->tv_sec--;
		from->tv_usec += 1000000;
	}
}

// add one timestamp to another 
// add1 = add1 + add2
// updated value is returned in add1
static SPL_INLINE
void SplTimestampAdd( SplTimestamp *add1, 
							   SplTimestamp *add2 )
{
	add1->tv_sec += add2->tv_sec;
	add1->tv_usec += add2->tv_usec;

	if (add1->tv_usec > 999999) {
		add1->tv_sec++;
		add1->tv_usec -= 1000000;
	}
}

// returns TRUE if time has past
static SPL_INLINE
BOOL SplTimestampIsExpired( SplTimestamp *timestamp )
{
	SplTimestamp current;

	// Zero timestamps never expire in our world
	if ((timestamp->tv_sec == 0) && (timestamp->tv_usec == 0)) {
		return FALSE;
	}

	gettimeofday(&current, NULL);

	if ((current.tv_sec > timestamp->tv_sec) ||
		((current.tv_sec == timestamp->tv_sec) && (current.tv_usec > timestamp->tv_usec))) {
		return TRUE;
	} else {
		return FALSE;
	}

}

// returns TRUE if time is zero
static SPL_INLINE
BOOL SplTimestampIsClear( SplTimestamp *timestamp )
{
	if (timestamp->tv_sec == 0 && timestamp->tv_sec == 0)
		return TRUE;
	else
		return FALSE;

}

// Return current time following the ISO 8601 format (follow strict rule of capital T or Z)
// RFC 5424
static SPL_INLINE
size_t SplGetTimeStr(char * buf, size_t len)
{
#ifndef _LINUX
#if defined(_WIN32)
	SYSTEMTIME S;
#elif defined(__APPLE__)
    // get time in UTC
    struct timeval now;
    struct tm *tmp;
    char str[32];
#endif
    char sign;
    LONG Bias= 0, BiasH, BiasM;
    Bias  = 0; // we are going to use UTC for log time so the Bias is always zero

    if (Bias > 0) sign = '+'; else sign = '-';
    BiasH = Bias / 3600;
    BiasM = Bias - (BiasH * 3600);

#if defined(_WIN32)
	GetSystemTime(&S);

	sprintf_s(buf, len-1, "%d-%02d-%02dT%02d:%02d:%02d.%03d%c%02d:%02d,"
			, S.wYear, S.wMonth, S.wDay
			, S.wHour, S.wMinute, S.wSecond
			, S.wMilliseconds
			, sign
			, abs(BiasH), abs(BiasM)
			);
#elif defined(__APPLE__)
    if (0 == gettimeofday(&now, 0))
    {
        tmp = gmtime(&now.tv_sec);
        if (tmp)
        {
            strftime(str, sizeof(str)/sizeof(str[0]), "%Y-%m-%dT%H:%M:%S", tmp);
            sprintf_s(buf, len-1, "%s.%06d%c%02d:%02d,"
                      , str
                      , now.tv_usec
                      , sign
                      , abs(BiasH), abs(BiasM)
                      );
        }
    }
#elif defined(_LINUX)
    // not sure if we can just use the code I wrote for __APPLE__ above. Something to check on...
#endif
    return strlen(buf);
#else
    return 0;   // _LINUX
#endif
}

#endif		// __SplTimestamp_h
