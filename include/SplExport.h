//
// SplExport.h
//

#pragma once

// For GCC (Linux and Mac) compile libraries with
// -fvisibility=hidden option to only export SOEXPORT functions

#ifdef __cplusplus
#	define __CPREAMBLE extern "C" 
#else
#	define __CPREAMBLE 
#endif 

// WIN32
#if defined(_WIN32)
#define SplImport	__declspec(dllimport)
#define SplExport	__declspec(dllexport)

#if defined(_DLL)
#	define SPLEXPORT SplExport
#	define SPLEXPORTCLASS class SplExport

#	define SOEXPORT __CPREAMBLE SplExport	// For Spl LIB builds
#	define SOEXPORTCLASS class SplExport	// For Spl LIB builds
#else
#	define SPLEXPORT 				// App Use
#	define SPLEXPORTCLASS class		// App Use

#	define SOEXPORT	__CPREAMBLE		// For Spl LIB builds
#	define SOEXPORTCLASS class		// For Spl LIB builds
#endif

// LINUX
#elif defined(_LINUX)

#define SplExport __attribute__((visibility("default")))

#ifdef _DLL
#	define SPLEXPORT SplExport
#	define SPLEXPORTCLASS class SplExport

#	define SOEXPORT __CPREAMBLE SplExport
#	define SOEXPORTCLASS __CPREAMBLE class SplExport
#else
#	define SPLEXPORT				// App Use
#	define SPLEXPORTCLASS class		// App Use

#	define SOEXPORT	__CPREAMBLE		// For Spl LIB builds
#	define SOEXPORTCLASS class		// For Spl LIB builds
#endif

// APPLE
#elif defined(__APPLE__)
#define SplExport __attribute__((visibility("default")))
#define SOEXPORT __CPREAMBLE __attribute__((visibility("default")))
#define SOEXPORTCLASS(classname) class SOEXPORT classname

// ESP32
#elif defined(ESP_PLATFORM)
#define SplExport

#define SPLEXPORT
#define SPLEXPORTCLASS class

#define SOEXPORT	__CPREAMBLE		// For Spl LIB builds
#define SOEXPORTCLASS class			// For Spl LIB builds

#endif
