// SplDll.h
//

#pragma once

#ifdef __cplusplus
extern "C" {
#endif 

#ifdef __APPLE__
#import "TargetConditionals.h"
#endif
    
typedef void (*SplProcAddr)();


/*********************************/
/************* Linux *************/
/*********** Mac OS X ************/
/*********************************/
#if defined(_LINUX) || defined(__APPLE__)
	
typedef void * SplDll;
typedef char SplDllName;
#define SplDllNameCopy strcpy
#define SplDllNameLen strlen
#define SplDllNameCmp strcmp
#define SplDllNameCat strcat

#if defined(TARGET_OS_IPHONE) && TARGET_OS_IPHONE
// All functions are statically linked, return our own image
static SPL_INLINE SplDll SplDllLoad( SplDllName *pLibFileName ) { return RTLD_SELF; }
#else
static SPL_INLINE SplDll SplDllLoad( SplDllName *pLibFileName ) { return dlopen(pLibFileName, RTLD_NOW | RTLD_LOCAL); }
#endif
static SPL_INLINE SplProcAddr SplDllSym( SplDll hLib, const char *pSymbol) { void *pSym = dlsym(hLib, pSymbol); return *(SplProcAddr*)(void*)(&pSym); }
static SPL_INLINE int SplDllClose( SplDll pLib ) { return dlclose(pLib); }
static SPL_INLINE const char * SplDllError() { return dlerror(); }


#elif defined (_WIN32)

/***********************************/
/************* Windows *************/
/***********************************/

typedef HMODULE SplDll;
typedef char SplDllName;
#define SplDllNameCopy strcpy
#define SplDllNameLen strlen
#define SplDllNameCmp strcmp
#define SplDllNameCat strcat
#pragma warning(disable:996)
#pragma warning(disable:4995)

static SplDll SplDllLoad( const SplDllName *pLibFileName ) { return LoadLibraryA(pLibFileName); }
static SplProcAddr SplDllSym( SplDll hLib, const char *pSymbol) { return (SplProcAddr)GetProcAddress(hLib, pSymbol); }
static int SplDllClose( SplDll pLib ) { return !FreeLibrary(pLib); }
static char * SplDllError() 
{ 
	static char aError[MAX_PATH];

	FormatMessageA(
        0,
        NULL,
		GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        aError,
        0, NULL );

	SetLastError(0);

	return aError;
};

#elif defined(ESP_PLATFORM)
// Not supported


#endif

#ifdef __cplusplus
}
#endif 
