// SplElapsedTime.h --  Time since boot
// 
// Standardized Platform Layer
//
// Resolution will vary depending on system
//

#pragma once

#ifndef __SplUpTime_H
#define __SplUpTime_H

#include "SplTypes.h"

#if defined(_LINUX) || defined(__APPLE__)
#include "SplTimestamp.h"
#endif

// SplElepsedTime -- Returns relative system elapsed time in milliseconds
//
static SPL_INLINE
uint64_t SplElapsedTime()
{
#ifdef _WIN32
	return (uint64_t) GetTickCount64();

#elif defined(_LINUX)

	struct timespec tp;
	//int erc =
			clock_gettime(CLOCK_MONOTONIC,&tp);
///printf("erc=%d errno=%d secs=%lu nsec=%lu\n",erc,errno,tp.tv_sec,tp.tv_nsec);

	return (uint64_t) tp.tv_sec * 1000 + (tp.tv_nsec / 1000000);

#elif defined(__APPLE__)
	// Not reliable since time of day can be changed
	SplTimestamp ts;

	gettimeofday(&ts,0);

	return (uint64_t) ts.tv_sec * 1000 + (ts.tv_usec / 1000);

#elif defined(ESP_PLATFORM)
	return (uint64_t)xTaskGetTickCount() * (1000/configTICK_RATE_HZ);

#endif
}


// Returns relative elapsed time in milliseconds -- use as replacement for Windows GetTickCount()
//
// *DEPRECATED*
//
// Notes:
//  - Using time will cause a problem if the system time changes
//	- GetTickCount() returns a 32 bit DWORD which is only 49.7 days
//  - Windows Vista/Server2008 supports GetTickCount64()
//
// We need something more robust, and more involved, than what is in this method.
// This is not reliable on Mac & Linux since it is based on time-of-day which can be changed.
//
static SPL_INLINE
uint64_t SplElapsedCount()
{
#ifdef _WIN32
	return (uint64_t) GetTickCount64();

#elif defined(_LINUX) || defined(__APPLE__)
	SplTimestamp ts;
	
	gettimeofday(&ts,0);
	
	return (uint64_t) ts.tv_sec * 1000 + (ts.tv_usec / 1000);

#elif defined(ESP_PLATFORM)
	return (uint64_t)xTaskGetTickCount() * (1000/configTICK_RATE_HZ);

#endif
}

#endif	// __SplUpTime_H
