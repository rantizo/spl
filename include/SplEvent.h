// SplEvent.h -- Event Object Management
//
// Standardized Platform Layer
//

#pragma once

#ifndef SPL_SplEvent_h
#define SPL_SplEvent_h

#include "SplTypes.h"
#include "SplExport.h"
#include "SplErrors.h"

/// Auto-reset event signalling.


// SplEventCreate -- Create an auto-resetting event
//
SOEXPORT
SplErc SplEventCreate(SplEvent *_pEvent);

// SplEventDelete -- Delete an auto-resetting event
//
SOEXPORT
SplErrorCode SplEventDelete(SplEvent *_pEvent);


// SplEventWait -- Wait an amount of time for the event to be signaled
//
SOEXPORT
SplResult SplEventWait(SplEvent *_pEvent, uint32_t _timeout_ms);

// SplEventIsSignaled -- Check to see if an event is signaled
//
SOEXPORT
SplResult SplEventIsSignaled(SplEvent *_pEvent);

// SplEventSignal -- Set an event to the signaled state
//
SOEXPORT
SplErrorCode SplEventSignal(SplEvent *_pEvent);

// SplEventSignalFromISR -- Set an event to the signaled state from an ISR
//
SOEXPORT
SplErrorCode SplEventSignalFromISR(SplEvent *_pEvent);

/// SplEventReset --  Reset an event to an unsignaled state
//
SOEXPORT
SplErrorCode SplEventReset(SplEvent *_pEvent);


#endif
