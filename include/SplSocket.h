// SplSocket.h -- Standardized Platform Layer
// 
// Platform (OS) specific socket header includes
//

#pragma once

#include "SplTypes.h"
#include "SplExport.h"

/***********************************/
/************* COMMON **************/
/***********************************/

// RCB we may have to convert this to defines instead of an enum
// for the compiler to allow the bitwise operations
// Equivalent to FD_XXX from Windows WinSock2.h
#define SplSocketEventNone		0x00000000
#define SplSocketEventRead		0x00000001
#define SplSocketEventWrite		0x00000002
#define SplSocketEventException 0x00000004
#define SplSocketEventAccept	0x00000008
#define SplSocketEventConnect	0x00000010
#define SplSocketEventClose		0x00000020
/*  Future support 
#define SplNetworkEventOob 0x00000004 // FD_OOB
*/
typedef int SplSocketEvents;

enum {
	SplErc_SocketSetFull = -1000,
	SplErc_SocketSelectFailed = 0xFFFFFFFF,			//WAIT_FAILED
	SplErc_SocketSelectCanceled = 0x00000080,		//WAIT_ABANDONED
	SplErc_SocketSelectSignaled = 0x00000000,		//SplErc_OK
	SplErc_SocketSelectTimeout = 0x00000102			//WAIT_TIMEOUT
};

// Send flags
#define SplSocketSendFlagNoFrag 0x00000001


// Buffer structure, compatible with WSABUF
typedef struct _SplBuffer {
	uint32_t len;
	char *buf;
} SplBuffer;

#if !defined(_WIN32)
// In place temporarily, please convert to SplBuffer
typedef SplBuffer WSABUF;
#endif 


/*********************************/
/************* Linux *************/
/************ Mac OS X ***********/
/*********************************/
#if defined(_LINUX) || defined(__APPLE__)

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/tcp.h>

#define SplGetLastSocketError	SplGetLastError

#define SPL_SOCKET_ERROR	-1
#define SPL_INVALID_SOCKET	-1

// This minus one slot will be available for socket use
// The first (mapped_events[0]) is reserved for the cancelation signal
#define MAX_SOCKETS_PER_SET (32)

typedef void SplSocketSendBuffer;
typedef void SplSocketRecvBuffer;

typedef int SplSocket;
typedef void SplProtocolInfo;
typedef void* SplProtocolInfoP;
typedef struct _SplSocketSet {
	int maxSock;
    fd_set desiredReadSet;
	fd_set readset;
    fd_set desiredWriteSet;
	fd_set writeset;
	fd_set desiredExceptSet;	//LINUX only for now
	fd_set exceptset;
	int cancel_signal[2];	// pipe for canceling selects

    SplSocket socket_array[MAX_SOCKETS_PER_SET];
	SplSocketEvents events[MAX_SOCKETS_PER_SET];	// Enabled events
    uint32_t socket_count;
} SplSocketSet;

typedef struct addrinfo SplAddrInfo;

enum {
	SplErc_SocketNoBufs = ENOBUFS,
	SplErc_SocketWouldBlock = EWOULDBLOCK, // retry on this error
    SplErc_SocketConnectionReset = ECONNRESET,
	SplErc_SocketInterrupted = EINTR,
	SplErc_SocketNoData = ENODATA,

	SplErc_SocketError = -1,
	SplErc_SocketBadPort = -2,
	SplErc_SocketConnectionClosed = -3,

	SplErc_SocketInvalid = -4,
	SplErc_SocketInvalidArguments = -5
};

/***********************************/
/************* Windows *************/
/***********************************/
#elif defined(_WIN32)

#include <winsock2.h>
#include <MSWSock.h>
#include <ws2tcpip.h>

typedef ULONG in_addr_t;

#define SplGetLastSocketError	WSAGetLastError

#define SPL_SOCKET_ERROR	SOCKET_ERROR
#define SPL_INVALID_SOCKET	INVALID_SOCKET

// This minus one slot will be available for socket use
// The first (mapped_events[0]) is reserved for the cancelation signal
#define MAX_SOCKETS_PER_SET (32)

typedef const char	SplSocketSendBuffer;
typedef char		SplSocketRecvBuffer;

typedef SOCKET SplSocket;
typedef WSAPROTOCOL_INFOA SplProtocolInfo;
typedef LPWSAPROTOCOL_INFO SplProtocolInfoP;

typedef struct _SplSocketSet {
	SOCKET socket_array[MAX_SOCKETS_PER_SET];
	WSAEVENT mapped_events[MAX_SOCKETS_PER_SET];	// The mapped event for this set (tied to sockets)
	SplSocketEvents events[MAX_SOCKETS_PER_SET];	// Enabled events
	uint32_t socket_count;
} SplSocketSet;

typedef ADDRINFOA SplAddrInfo;

enum {
	SplErc_SocketNoBufs = WSAENOBUFS,
	SplErc_SocketWouldBlock = WSAEWOULDBLOCK, // retry on this error
	SplErc_SocketConnectionReset = WSAECONNRESET,
	SplErc_SocketInterrupted = WSAEINTR,
	SplErc_SocketNoData = ENODATA,
	SplErc_SocketInvalid = WSAENOTSOCK,
	SplErc_SocketInvalidArguments = WSAEINVAL,

	SplErc_SocketError = -1,
	SplErc_SocketBadPort = -2,
	SplErc_SocketConnectionClosed = -3
};

/********************************/
/************ ESP32 *************/
/********************************/
#elif defined(ESP_PLATFORM)

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <lwip/tcp.h>
#include <lwip/netdb.h>

#define SplGetLastSocketError	SplGetLastError

#define SPL_SOCKET_ERROR	-1
#define SPL_INVALID_SOCKET	-1

// This minus one slot will be available for socket use
// The first (mapped_events[0]) is reserved for the cancelation signal
#define MAX_SOCKETS_PER_SET (32)

typedef void SplSocketSendBuffer;
typedef void SplSocketRecvBuffer;

typedef int SplSocket;
typedef void SplProtocolInfo;
typedef void* SplProtocolInfoP;

typedef struct _SplSocketSet {
	int maxSock;
	fd_set desiredReadSet;
	fd_set readset;
	fd_set desiredWriteSet;
	fd_set writeset;

	SplSocket socket_array[MAX_SOCKETS_PER_SET];	// [0] not use for ESP32
	SplSocketEvents events[MAX_SOCKETS_PER_SET];	// Enabled events
	uint32_t socket_count;
	
	bool bCancelled;
} SplSocketSet;

typedef struct addrinfo SplAddrInfo;

enum {
	SplErc_SocketNoBufs           = ENOBUFS,
	SplErc_SocketWouldBlock       = EWOULDBLOCK, // retry on this error
	SplErc_SocketConnectionReset  = ECONNRESET,
	SplErc_SocketInterrupted      = EINTR,
	SplErc_SocketNoData           = ENODATA,

	SplErc_SocketError            = -1,
	SplErc_SocketBadPort          = -2,
	SplErc_SocketConnectionClosed = -3,

	SplErc_SocketInvalid          = -4,
	SplErc_SocketInvalidArguments = -5
};

#define SOMAXCONN		1	// Not defined for ESP32

#endif


/************* SplSocket.c *************/

SOEXPORT
SplErc SplSocketSetLastError(SplErc _erc);

SOEXPORT
SplErc SplSocketInit(  );

SOEXPORT
SplErc SplSocketCleanup(  );


SOEXPORT
SplErc SplSocketCreate( SplSocket *_pSocketRet, int32_t _family, int32_t _type, int32_t _protocol);

SOEXPORT
SplErc SplSocketSetNonBlocking( SplSocket _socket, BOOL _bNoBlocking );


SOEXPORT
SplErc SplSocketBind( SplSocket _socket, const struct sockaddr *_paddr, int32_t _addrlen );

SOEXPORT
SplErc SplSocketConnect( SplSocket _socket, const struct sockaddr *_paddr, int32_t addrlen );

SOEXPORT
SplErc SplSocketShutdown( SplSocket _socket, int32_t _how );

SOEXPORT
SplErc SplSocketClose( SplSocket _socket );


SOEXPORT
SplErc SplSocketListen( SplSocket _socket, int32_t _backlog );

SOEXPORT
SplErc SplSocketAccept( SplSocket *_pSocketRet, SplSocket _listeningSock, struct sockaddr *_paddr, socklen_t *_paddrlen );


SOEXPORT
SplResult SplSocketSend( SplSocket _socket, const void *_pBuffer, uint32_t _len, int32_t _flags);

SOEXPORT
SplResult SplSocketRecv( SplSocket _socket, void *_pBuffer, uint32_t _len, int32_t _flags);

SOEXPORT
SplResult SplSocketSendTo( SplSocket _socket, const void *_pBuffer, uint32_t _len, int32_t _flags, const struct sockaddr *pTo , socklen_t _tolen);

SOEXPORT
SplResult SplSocketRecvFrom( SplSocket _socket, void *_pBuffer, uint32_t _len, int32_t _flags, struct sockaddr *_pFrom, socklen_t *_pFromLenRet);

SOEXPORT
struct hostent* SplSocketGetHostByName( const char *_pszName );

#if !defined(_FreeRTOS)
SOEXPORT
struct hostent* SplSocketGetHostByAddr( const char *_pszAddr, int32_t _len, int32_t _type );
#endif

SOEXPORT
SplErc SplSocketGetAddrInfo( const char *_pHostName, const char *_pszServerName, const SplAddrInfo *_pHints, SplAddrInfo **_ppAddrInfoRet);

SOEXPORT
SplErc SplSocketGetPort( SplSocket _socketocket, uint16_t *_pnPortRet);

SOEXPORT
void SplSocketFreeAddrInfo( SplAddrInfo *ai );

SOEXPORT
SplErc SplSocketGetOpt( SplSocket _socket, int32_t level, int32_t optname, void* optval, socklen_t* optlen );

SOEXPORT
SplErc SplSocketSetOpt( SplSocket _socket, int32_t level, int32_t optname, const void* optval, socklen_t optlen );

SOEXPORT
SplErc SplSocketGetSockname( SplSocket _socket, struct sockaddr* name, socklen_t *namelen );

SOEXPORT
SplErc SplSocketGetPeername( SplSocket _socket, struct sockaddr* name, socklen_t *namelen );


// if timeout_ms is 0, wait infinitely - *TRN* Needs to be changed to (-1)
SOEXPORT
SplErc SplSocketSelect( SplSocketSet *_pSet, uint32_t _timeout_ms);

SOEXPORT
SplErc SplSocketTrySelect( SplSocketSet *_pSet);

SOEXPORT
SplErc SplSocketSelectCancel( SplSocketSet *_pSet );

SOEXPORT
SplErc SplSocketEnumEvents(SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents *_pfEventsRet);

SOEXPORT
SplErc SplSocketSetCreate( SplSocketSet **_ppSetRet );

SOEXPORT
SplErc SplSocketSetDelete( SplSocketSet *_pSet );

SOEXPORT
SplErc SplSocketSetClear( SplSocketSet *_pSet );

SOEXPORT
SplErc SplSocketSetAdd( SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents _fEventsRet);

SOEXPORT
SplErc SplSocketSetRemove( SplSocketSet *_pSet, SplSocket _socket );

SOEXPORT
BOOL SplSocketEventOccurred( SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents _fEvents );

SOEXPORT
SplErc SplSocketGetEvents( SplSocketSet *_pSet, SplSocket _socket, SplSocketEvents *_pEventsRet );
