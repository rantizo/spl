// SplPlatformIncludes.h
// 
// Platform (OS) specific header includes
//

#pragma once

/*********************************/
/************* Linux *************/
/*********************************/
#if defined(_LINUX)
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
//#define _LARGEFILE64_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <semaphore.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <wchar.h>
#include <dlfcn.h>
#include <sys/utsname.h>
#include <stdarg.h>
#include <dirent.h>
#include <net/if.h>
#include <ctype.h>
#include <sys/wait.h>

// Socket includes live in SplSocket.h, must be explicitly included

// Logging
#include <syslog.h>

// Windows API overrides
#define _declspec(dll)
//#define APIENTRY

#define IN
#define OUT
#define VOID void

#endif

/*********************************/
/************ Mac OS X ***********/
/*********************************/
#if defined(__APPLE__)

// RCB copied from Linux for now but will need tuning
#import <stdlib.h>
#import <stdio.h>
#import <unistd.h>
#import <string.h>
#import <pthread.h>
#import <semaphore.h>
#import <sys/time.h>
#import <sys/stat.h>
#import <signal.h>
#import <sys/ioctl.h>
#import <fcntl.h>
#import <errno.h>
#import <wchar.h>
#import <objc/objc.h>

#if TARGET_OS_IPHONE
    #include <MobileCoreServices/MobileCoreServices.h>
    // Added the following to for STRAW_iOS.cpp
    #include <sys/socket.h>
    #include <net/if.h>
    // Added the following for LibraryMgr.h
    #include <dirent.h>
    // Added the following for SplLog
    #include <asl.h>
    // Added the following for SplHttp (CFNetwork style)
    #include <CFNetwork/CFNetwork.h>
    // Added the following for CGRect (KVM)
    #include <CoreGraphics/CGGeometry.h>
#else // OS X
    #include <CoreServices/CoreServices.h>
    #include <Carbon/Carbon.h>
#endif

#include <dlfcn.h>
#include <sys/utsname.h>
#include <mach/mach.h>
#include <mach/mach_time.h>
#include <mach-o/dyld.h>

#undef Zone
#undef Style

// Windows API overrides
#define _declspec(dll)
//#define APIENTRY

#define IN
#define OUT
#define VOID void

#endif

/***********************************/
/************* Windows *************/
/***********************************/

// When building an MFC project you must include the MFC header files before including SPL.h

#if defined(_WIN32)

// 0x0601 is Windows 7

#ifndef NTDDI_VERSION
#define NTDDI_VERSION 0x06010000
#elif NTDDI_VERSION != 0x06010000 && !defined(_MFC_VER)
#pragma message( "Warning: conflicting NTDDI_VERSION detected in SplPlatformIncludes.h") 
#endif

#ifndef WINVER		
#define WINVER 0x0601		// Windows 7 or later
#elif WINVER != 0x0601 && !defined(_MFC_VER)
#pragma message( "Warning: conflicting WINVER detected in SplPlatformIncludes.h") 
#endif

#ifndef _WIN32_WINNT       
#define _WIN32_WINNT 0x0601
#elif _WIN32_WINNT != 0x0601 && !defined(_MFC_VER)
#pragma message( "Warning: conflicting _WIN32_WINNT detected in SplPlatformIncludes.h") 
#endif						

#ifndef _WIN32_WINDOWS
#define _WIN32_WINDOWS 0x0601
#elif _WIN32_WINDOWS != 0x0601
#pragma message( "Warning: conflicting _WIN32_WINDOWS detected in SplPlatformIncludes.h") 
#endif

// Note: AfxWin.h forces _WIN32_IE to 0x0700
#ifndef _WIN32_IE		
#define _WIN32_IE 0x0A00
#elif _WIN32_IE != 0x0A00 && !defined(_MFC_VER)
#pragma message( "Warning: conflicting _WIN32_IE detected in SplPlatformIncludes.h") 
#endif

#ifndef WIN32_LEAN_AND_MEAN		
#define WIN32_LEAN_AND_MEAN	// Exclude rarely-used stuff from Windows headers
#endif

// Windows Header Files:
#ifndef _OBJBASE_H_
#include <objbase.h>
#endif
#ifndef _WINDOWS_
#include <windows.h>
#endif
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <io.h>

#endif


/***********************************/
/************* ESP32 ***************/
/***********************************/
#if defined(ESP_PLATFORM)
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>
#include <esp_task.h>
#include <arch/sys_arch.h>
#include <string.h>

#endif

