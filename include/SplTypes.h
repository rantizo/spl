// SplTypes.h -- Standardized Platform Layer
// 
// Necessary type definitions for portability layer
// Note: some types may be defined in individual SplXxx.h files
// (i.e. SplGuid, SplTimestamp, etc)
//

#pragma once

#ifndef __SplTypes_h
#define __SplTypes_h

#include "SplInts.h"
#include "SplPlatformIncludes.h"

#ifndef TRUE
	#define TRUE (1)
#endif
#ifndef FALSE
	#define FALSE (0)
#endif

#if defined(__APPLE__) || defined(_LINUX) || defined(ESP_PLATFORM)
#define CALLBACK
#endif 

#ifndef EXTERN_C
#define EXTERN_C extern "C"
#endif

#if defined(_LINUX)

/*********************************/
/************* Linux *************/
/*********************************/

//------------
// API helpers

#define SplNoOp		(void)0

#define SplInvalidHandle	(-1)

// __stdcall is Win32 specific and meaningless to Linux
#ifndef __stdcall
#define __stdcall /* nothing */
#endif

#define SPL_INLINE inline
#define SPL_INLINE_FORCE __attribute__((always_inline))

typedef const char * SplStringConst;
typedef char * SplString;
#define strtoll _strtoui64

#define MAX_PATH 260
#define SplMaxPath		260
#define SPL_PLATFORM_PATH_DELIMITER			'/'
#define SPL_PLATFORM_PATH_DELIMITER_STR		"/"


//----------
// Time/Date
//#define SplTime32
#define SplTime64	time_t
#define SplTime		time_t

//---------------
// Error handling
typedef int SplErrno;	// Native Error code
static SPL_INLINE SplErrno SplGetLastError()	{ return errno; }

//---------------
// Syncronization
typedef sem_t *SplEvent;
#define SPL_INFINITE		0xFFFFFFFF

//for use in debugger - a non-opaque version of pthread_mutex_t
typedef pthread_mutex_t SplInternalPthreadMutex;

#ifdef SPL_USE_SEMAPHORES
	typedef sem_t *SplMutex;
#else
	typedef SplInternalPthreadMutex SplMutex;
#endif

typedef SplInternalPthreadMutex SplCritical;
typedef /*volatile*/ int32_t SplAtomic;
typedef /*volatile*/ intptr_t SplAtomicPtr;

//----------
// Processes
typedef pid_t SplProcess;

//--------
// Threads
typedef void *(*SplThreadProc) (void *);

typedef pthread_t SplThread;
typedef SplThread SplThreadIdType;

#define SPL_THREAD_STACKSIZE_DEFAULT		0

typedef int SplThreadPriority;

// Thread priority mapping
// Without superuser, the Linux POSIX implementation limits
// the use of the scheduler to SCHED_OTHER, not SCHED_FIFO or SCHED_RR
// SCHED_OTHER is a dynamic priority adjustment scheduler and does not
// allow explicit settings.  Thus all priorities must be 0.
#if 0
enum  {
	SplThreadPriorityNonCriticalRendering = 0,
	SplThreadPriorityCriticalRendering = 0,
	SplThreadPriorityBackground = 0,
	SplThreadPriorityDebug = 0,
	SplThreadPriorityGui = 0,
	SplThreadPriorityNetwork = 0,
	SplThreadPriorityAudio = 0
};
#endif

// Thread priority mapping
// Use -20 to 20 although it may depend on the kernel
//
enum {
	SplThreadPriorityCritical= -15,
	SplThreadPriorityHigh	 = -10,
	SplThreadPriorityAbove	 = -5,
	SplThreadPriorityNormal	 =  0,
	SplThreadPriorityBelow	 =  5,
	SplThreadPriorityLow	 =  10,
	SplThreadPriorityIdle	 =  16
};

#define SPL_THREAD_MODE_BACKGROUND_BEGIN (0x00010000)	// unsupported
#define SPL_THREAD_MODE_BACKGROUND_END (0x00020000) // unsupported

#if 0
#ifdef SPL_PROCESS_SCHEDULING
	#define SPL_THREAD_PRIORITY_IDLE (10)
	#define SPL_THREAD_PRIORITY_LOWEST (2)
	#define SPL_THREAD_PRIORITY_BELOW_NORMAL (1)
	#define SPL_THREAD_PRIORITY_NORMAL (0)
	#define SPL_THREAD_PRIORITY_ABOVE_NORMAL (-1)
	#define SPL_THREAD_PRIORITY_HIGHEST (-2)
	#define SPL_THREAD_PRIORITY_TIME_CRITICAL (-10)
#else
	#define SPL_THREAD_PRIORITY_IDLE (20)
	#define SPL_THREAD_PRIORITY_LOWEST (30)
	#define SPL_THREAD_PRIORITY_BELOW_NORMAL (40)
	#define SPL_THREAD_PRIORITY_NORMAL (50)
	#define SPL_THREAD_PRIORITY_ABOVE_NORMAL (60)
	#define SPL_THREAD_PRIORITY_HIGHEST (70)
	#define SPL_THREAD_PRIORITY_TIME_CRITICAL (80)
#endif
#endif


// APPLE not officially support as I have no way to test it again.
#elif defined(__APPLE__)

/*********************************/
/************* Apple *************/
/*********************************/

// API helpers

#define SplNoOp		(void)0

// __stdcall is Win32 specific and meaningless to Apple
#ifndef __stdcall
#define __stdcall
#endif

#define SPL_INLINE inline
#define SPL_INLINE_FORCE __attribute__((always_inline))

#define MAX_PATH 260
#define SplMaxPath		260
#define SPL_PLATFORM_PATH_DELIMITER			'/'
#define SPL_PLATFORM_PATH_DELIMITER_STR		"/"

//----------------
// Error Handling

typedef int SplErrno;	// Native Error code
static SPL_INLINE SplErrno SplGetLastError()	{ return errno; }

//-----------
// Time/Date

//#define SplTime32
#define SplTime64	time_t
#define SplTime		time_t


#include <CoreFoundation/CoreFoundation.h>
#include <SystemConfiguration/SystemConfiguration.h> 
#if TARGET_OS_IPHONE
#include <MobileCoreServices/MobileCoreServices.h>
#include <CFNetwork/CFNetwork.h>
#else
#include <CoreServices/CoreServices.h>
#endif

// C99 integer types and format conversions
// inttypes.h will bring in stdint.h as well
#include <inttypes.h>
#include <objc/objc.h>

#ifdef DEBUG_BUILD
//for use in debugger - a non-opaque version of pthread_mutex_t
typedef union {
    struct {
        int __m_reserved;
        int __m_count;
        struct _pthread_descr_struct *__m_owner;
        int __m_kind;
        struct {
            long int __status;
            int __spinlock;
        } __m_lock;
    } standard_mutex;
    pthread_mutex_t opaque_mutex;
} SplInternalPthreadMutex;
#else
typedef pthread_mutex_t SplInternalPthreadMutex;
#endif

// Options

// SPL types
typedef void (*SplThreadProc) (void *);

typedef pthread_t SplThread;
typedef SplThread SplThreadIdType;

#define SPL_THREAD_STACKSIZE_DEFAULT		0

typedef pid_t SplProcess;
typedef struct _SplEvent {
	SplInternalPthreadMutex lock;
	pthread_cond_t cond;
	BOOL signaled;
} * SplEvent;
//typedef _SplEvent * SplEvent;

#define SPL_INFINITE		0xFFFFFFFF

typedef SplInternalPthreadMutex SplMutex;
typedef SplInternalPthreadMutex SplCritical;
typedef volatile int32_t SplAtomic;
typedef volatile intptr_t SplAtomicPtr;

#ifdef SPL_OSX_THREAD_PRIO_USE_PTHREAD

typedef int SplThreadPriority;

// Thread priority mapping
// Use pthreads for now - we may switch to policies (see SplThread.c)
enum {///SplThreadPriority {
	SplThreadPriorityNonCriticalRendering = 31,
	SplThreadPriorityCriticalRendering = 50,
	SplThreadPriorityBackground = 10,
	SplThreadPriorityDebug = 10,
	SplThreadPriorityGui = 31,
	SplThreadPriorityNetwork = 60,
	SplThreadPriorityAudio = 70
};
#else
// For use with thread policies, map to struct array below
enum {///SplThreadPriority {
	SplThreadPriorityNonCriticalRendering = 0,
	SplThreadPriorityCriticalRendering = 1,
	SplThreadPriorityBackground = 2,
	SplThreadPriorityDebug = 3,
	SplThreadPriorityGui = 4,
	SplThreadPriorityNetwork = 5,
	SplThreadPriorityAudio = 6
};
#endif


#elif defined (_WIN32)

/***********************************/
/************* Windows *************/
/***********************************/

// API helpers
#define SplNoOp		__noop

#define SplInvalidHandle	INVALID_HANDLE_VALUE

// Inline declarations in Visual Studio C (not C++) files
// cause inconsistent problems when declared inline vs __inline
#define SPL_INLINE __inline
#define SPL_INLINE_FORCE __forceinline

#ifndef PATH_MAX
#define PATH_MAX 1024
#endif

#define strtoll _strtoui64
typedef const char * SplStringConst;
typedef char * SplString;

#define SplMaxPath		MAX_PATH
#define SPL_PLATFORM_PATH_DELIMITER			'\\'
#define SPL_PLATFORM_PATH_DELIMITER_STR		"\\"

//-----------------
// Error Handling
//
// Note that errno returns the last error for a std library function. This is not
// the same as the GetLastError() used by Windows functions. In addition, WinSock
// uses WSAGetLastError() which does not use errno either.

#include <errno.h>

typedef errno_t SplErrno;	// Native Error code
static SplErrno SplGetLastError()	{ return GetLastError(); }

//-----------
// Time/Date

//#define SplTime32
typedef __time64_t	SplTime64;
typedef __time64_t  SplTime;


//----------------
// Syncronization

typedef HANDLE SplEvent;
#define SPL_INFINITE		INFINITE

typedef HANDLE SplMutex;

typedef CRITICAL_SECTION SplCritical;

typedef volatile LONG SplAtomic;		// Note: stdint.h defined int32_t as int, not long!!!
typedef volatile intptr_t SplAtomicPtr;

//----------
// Processs

typedef HANDLE SplProcessHandle;

typedef struct _SplProcess {
	SplProcessHandle process_handle;
	DWORD process_id;
} SplProcess;

//--------
// Thread

typedef void (*SplThreadProc)(void *);

typedef HANDLE SplThreadHandle;
typedef LPDWORD SplThreadId;

typedef struct _SplThread {
	SplThreadHandle thread_handle;
	DWORD thread_id;
} SplThread;

typedef DWORD SplThreadIdType;

#define SPL_THREAD_STACKSIZE_DEFAULT		0

typedef int SplThreadPriority;

// Thread priority mapping
//
enum {
	SplThreadPriorityCritical= THREAD_PRIORITY_TIME_CRITICAL,
	SplThreadPriorityHigh	 = THREAD_PRIORITY_HIGHEST,
	SplThreadPriorityAbove	 = THREAD_PRIORITY_ABOVE_NORMAL,
	SplThreadPriorityNormal	 = THREAD_PRIORITY_NORMAL,
	SplThreadPriorityBelow	 = THREAD_PRIORITY_BELOW_NORMAL,
	SplThreadPriorityLow	 = THREAD_PRIORITY_LOWEST,
	SplThreadPriorityIdle	 = THREAD_PRIORITY_IDLE
};



#elif defined(ESP_PLATFORM)

//***********************************
//*********** ESP_PLATFORM **********
//***********************************


//-------------
// API helpers
#define SplNoOp		(void)0

#define SplInvalidHandle	(-1)

// __stdcall is Win32 specific and meaningless for us
#ifndef __stdcall
#define __stdcall /* nothing */
#endif

#define SPL_INLINE inline
#define SPL_INLINE_FORCE inline
//#define SPL_INLINE_FORCE __attribute__((always_inline))

typedef const char * SplStringConst;
typedef char * SplString;

#define strtoll _strtoui64

#define MAX_PATH 260
#define SplMaxPath		260
#define SPL_PLATFORM_PATH_DELIMITER			'/'
#define SPL_PLATFORM_PATH_DELIMITER_STR		"/"

//----------------
// Error Handling

#include <errno.h>
typedef int SplErrno;	// Native Error code
static SPL_INLINE SplErrno SplGetLastError() { return errno; }

//-----------
// Time/Date

#define SplTime32	time_t
#define SplTime		time_t

//----------------
// Syncronization

#define SPL_INFINITE		portMAX_DELAY
typedef SemaphoreHandle_t SplEvent;

typedef SemaphoreHandle_t SplMutex;

typedef portMUX_TYPE* SplCritical;
typedef /*volatile*/ int32_t SplAtomic;
typedef /*volatile*/ intptr_t SplAtomicPtr;

//---------
// Process

//typedef pid_t SplProcess;

//--------
// Thread

typedef void (*SplThreadProc)(void *);

typedef TaskHandle_t SplThreadHandle;

typedef struct _SplThread {
	SplThreadHandle thread_handle;
	uint32_t thread_id;
} SplThread;
typedef uint32_t SplThreadIdType;

#define SPL_THREAD_STACKSIZE_DEFAULT		4096


typedef int SplThreadPriority;

// Thread priority mapping
//
enum {	
	SplThreadPriorityCritical = ESP_TASK_PRIO_MAX - 5,

	SplThreadPriorityHigh	  = ESP_TASK_PRIO_MIN + 5,
	SplThreadPriorityAbove    = ESP_TASK_PRIO_MIN + 4,
	SplThreadPriorityNormal   = ESP_TASK_PRIO_MIN + 3,
	SplThreadPriorityBelow	  = ESP_TASK_PRIO_MIN + 2,
	SplThreadPriorityLow	  = ESP_TASK_PRIO_MIN + 1,

	SplThreadPriorityIdle	  = ESP_TASK_PRIO_MIN,

	SplThreadPriorityNetwork  = ESP_TASK_TCPIP_PRIO,
	SplThreadPrioritySerial   = ESP_TASK_TCPIP_PRIO
};


#endif

#endif	// platform


/***********************************/
/************* Common **************/
/***********************************/

typedef SplErrno SplErrorCode;	//**DEPRECATED** Use SplErc
typedef SplErrno SplErc;		// Error or Return Code
typedef SplErrno SplResult;		// ERC or other value such as byte count

typedef void * SplHandle;

// Agnostic types.
typedef struct _SplSemaphore {

#if defined(_WIN32)
	HANDLE semaphore;

#elif defined(_LINUX) || defined(__APPLE__)
	sem_t semaphore;

#elif defined(ESP_PLATFORM)
	SemaphoreHandle_t *semaphore;

#endif

	SplAtomic maxCount;
	SplAtomic count;
} SplSemaphore;


#define SPL_MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define SPL_MIN(a, b)  (((a) < (b)) ? (a) : (b))
#define SPL_ABS(a)	   (((a) < 0) ? -(a) : (a))
#define SPL_CLAMP(x, low, high)  (((x) > (high)) ? (high) : (((x) < (low)) ? (low) : (x)))

#define SPL_UNUSED(d) ((void)d)

#ifdef __cplusplus

template <class T1,class T2>
const T1 inline &SplMax(const T1 &t1, const T2 &t2) { return (t1 > t2 ? t1 : (const T1&) t2); }

template <class T1, class T2>
const T1 inline &SplMin(const T1 &t1, const T2 &t2) { return (t1 < t2 ? t1 : (const T1&) t2); }

template <class T1, class T2>
const T1 inline &SplAbs(const T1 &ta, T2 tb = 0) { return (ta < tb ? tb - ta : ta - tb); }

template <class T1, class T2, class T3>
const T1 inline &SplClamp(const T1 &t,const T2 &tLow, const T3 &tHigh) { return ( (t > tHigh) ? tHigh : ((t < tLow) ? tLow : t) ); }

#endif

static char spl_path_delimiter = SPL_PLATFORM_PATH_DELIMITER;
static const char *spl_path_delimiter_str = SPL_PLATFORM_PATH_DELIMITER_STR;

// Return the path delimiter (forward or backslash) for the current platform
static SPL_INLINE char SplGetPathDelimiter(void)			 { return spl_path_delimiter; }
static SPL_INLINE const char *SplGetPathDelimiterStr(void)	 { return spl_path_delimiter_str; }

//vv Extracted from SpoTypes.h
typedef int	   ERC;
typedef double REAL;

#ifndef OK
#define OK	(0)
#endif
#ifndef ERR
#define ERR	(-1)
#endif

// Declare typed handles using a dummy typed pointer
//
// Example:
//		SPL_DECLARE_HANDLE(HGIZMO);
// Instead of:
//		typedef int HGIZMO;
//
#ifndef SPL_DECLARE_HANDLE
#	define SPL_DECLARE_HANDLE(name) struct name##__ { int unused; }; typedef struct name##__ *name
#endif


#if defined(_UNICODE) && defined(_WIN32)
#	define _TT(x)	L##x
#	define TTCHAR	wchar_t
#	define xcslen	wcslen
#	define xcscpy	wcscpy
#else
#	define _TT(x)	x
#	define TTCHAR	char
#	define xcslen	strlen
#	define xcscpy	strcpy
#endif
//^^

typedef char utf8_t;


// SplWaitResult values are equivalent to Windows WAIT_xxx defines
enum  {
	SplWaitFailed = 0xffffffff,
	SplWaitAbandoned = 0x00000080,
	SplWaitAcquired = 0x00000000,
	SplWaitTimeout = 0x00000102,
	SplWaitIoCompletion = 0x000000C0L
};

// SplLockResult values are equivalent to Windows WAIT_xxx defines
enum  {
	SplLockFailed = 0xffffffff,
	SplLockAbandoned = 0x00000080,
	SplLockAcquired = 0x00000000,
	SplLockTimeout = 0x00000102
};

// SPL State is used for SPL itself (SplInit) as well as 
// various subsystems (logging, appdata, etc)
typedef enum  {
	SplStateUninitialized,
	SplStateOpening,
	SplStateOpen,
	SplStateClosing,
	SplStateClosed,
	SplStateEnd
} SplState;


// ThreadWrapper allows the same threadProc type on all platforms
// ThreadWrapper required for Apple to be able to set ThreadName

#define SPL_THREADNAME_MAX		32

typedef struct _SplThreadWrapperContext {
	SplThreadProc threadProc;
	void *parameter;
	char szName[SPL_THREADNAME_MAX];
} SplThreadWrapperContext;


typedef enum {
	SplEndianBig = 0,
	SplEndianLittle = 1
} SplEndianness;

typedef enum {
    SplCpu32Bit = 32,
    SplCpu64Bit = 64
} SplCpuBits;

typedef enum {
    SplCpuType_Unknown,
    SplCpuType_i386Generic,
    SplCpuType_x86,
    SplCpuType_x86_64,
    SplCpuType_ARMGeneric,
    SplCpuType_ARM6,
    SplCpuType_ARM7,
    SplCpuType_ARM7S,
    SplCpuType_MIPS,
    SplCpuType_Alpha,
    SplCpuType_PowerPC,
    SplCpuType_Itanium,
} SplCpuType;


#if 0
#define VALUE_TO_STRING(var) #var
#define VALUE(var) VALUE_TO_STRING(var)
#define VAR_NAME_VALUE(var) #var "=" VALUE(var)
//#pragma message VAR_NAME_VALUE(configMAX_TASK_NAME_LEN)
#endif	


#if 0//****

// Appdata key value pair file structure
typedef struct _SplAppdataFile {
	SplMutex fileMutex;
	char *filename;
	char *filename_temp;
} SplAppdataFile;

// SPL error codes included below
// For the ulimate in compatibility they are defines
//#include "SplErrors.h"




// SplSetResult values are equivalent to Windows WAIT_xxx defines
typedef enum  {
	SplSetFailed = 0xffffffff
} SplSetResult;

typedef enum _SplHttpMultipartFieldType {
    SplMultipartFile,      // file upload
    SplMultipartFormField, // form field
} SplHttpMultipartFieldType;

typedef struct _SplHttpMultipartField {
    SplHttpMultipartFieldType fieldType;
    SplStringConst name;
	SplStringConst value;
    SplStringConst contentType;
} SplHttpMultipartField;

typedef struct _SplWindowInfo {
	uint32_t windowId;
	uint32_t processId;
	uint32_t flags;
	char title[MAX_PATH];
	char className[MAX_PATH];
} SplWindowInfo;

typedef struct _SplProcessInfo {
	uint32_t id;
	char name[MAX_PATH];
	char exe[MAX_PATH];
	char path[MAX_PATH];
	char description[MAX_PATH];

	char appIdentifier[MAX_PATH]; // CFBundleIdentifier on Mac.. e.g. com.sococo.Sococo

	BOOL isBackgroundOnly;  // DM - currently Mac only
	void *icon;				// Icon associated with the .exe on Windows
} SplProcessInfo;

// SPL input events
typedef enum {
	SplInputEventClass_None = 0x00,
	SplInputEventClass_Mouse = 0x01,
	SplInputEventClass_Keyboard = 0x02
} SplInputEventClass;

typedef enum {
	SplInputEventAction_None,
	SplInputEventAction_LeftMouseDown,
	SplInputEventAction_LeftMouseUp,
	SplInputEventAction_MiddleMouseDown,
	SplInputEventAction_MiddleMouseUp,
	SplInputEventAction_RightMouseDown,
	SplInputEventAction_RightMouseUp,
	SplInputEventAction_MouseMoved,
	SplInputEventAction_LeftMouseDragged,
	SplInputEventAction_MiddleMouseDragged,
	SplInputEventAction_RightMouseDragged,
	SplInputEventAction_KeyDown,
	SplInputEventAction_KeyUp,
	SplInputEventAction_ScrollWheelMoved
} SplInputEventAction;

// Keyboard translation and vKey types
typedef enum {
	SplInputVkeyLetterA = 		0x00,
	SplInputVkeyLetterB = 		0x01,
	SplInputVkeyLetterC = 		0x02,
	SplInputVkeyLetterD = 		0x03,
	SplInputVkeyLetterE = 		0x04,
	SplInputVkeyLetterF = 		0x05,
	SplInputVkeyLetterG = 		0x06,
	SplInputVkeyLetterH = 		0x07,
	SplInputVkeyLetterI = 		0x08,
	SplInputVkeyLetterJ = 		0x09,
	SplInputVkeyLetterK = 		0x0A,
	SplInputVkeyLetterL = 		0x0B,
	SplInputVkeyLetterM = 		0x0C,
	SplInputVkeyLetterN = 		0x0D,
	SplInputVkeyLetterO = 		0x0E,
	SplInputVkeyLetterP = 		0x0F,
	SplInputVkeyLetterQ = 		0x10,
	SplInputVkeyLetterR = 		0x11,
	SplInputVkeyLetterS = 		0x12,
	SplInputVkeyLetterT = 		0x13,
	SplInputVkeyLetterU = 		0x14,
	SplInputVkeyLetterV = 		0x15,
	SplInputVkeyLetterW = 		0x16,
	SplInputVkeyLetterX = 		0x17,
	SplInputVkeyLetterY = 		0x18,
	SplInputVkeyLetterZ = 		0x19,
	SplInputVkeyNumber0 = 		0x1A,
	SplInputVkeyNumber1 = 		0x1B,
	SplInputVkeyNumber2 = 		0x1C,
	SplInputVkeyNumber3 = 		0x1D,
	SplInputVkeyNumber4 = 		0x1E,
	SplInputVkeyNumber5 = 		0x1F,
	SplInputVkeyNumber6 = 		0x20,
	SplInputVkeyNumber7 = 		0x21,
	SplInputVkeyNumber8 = 		0x22,
	SplInputVkeyNumber9 = 		0x23,
	SplInputVkeyEqual = 		0x24,
	SplInputVkeyMinus = 		0x25,
	SplInputVkeyLeftBracket =	0x26,
	SplInputVkeyRightBracket =	0x27,
	SplInputVkeyQuote = 		0x28,
	SplInputVkeySemicolon =		0x29,
	SplInputVkeyBackslash = 	0x2A,
	SplInputVkeyComma =			0x2B,
	SplInputVkeySlash =			0x2C,
	SplInputVkeyPeriod =		0x2D,
	SplInputVkeyGrave =			0x2E,

	SplInputVkeyKeypad0 = 		0x40,
	SplInputVkeyKeypad1 =	 	0x41,
	SplInputVkeyKeypad2 = 		0x42,
	SplInputVkeyKeypad3 = 		0x43,
	SplInputVkeyKeypad4 = 		0x44,
	SplInputVkeyKeypad5 = 		0x45,
	SplInputVkeyKeypad6 = 		0x46,
	SplInputVkeyKeypad7 = 		0x47,
	SplInputVkeyKeypad8 = 		0x48,
	SplInputVkeyKeypad9 = 		0x49,

	SplInputVkeyKeypadDecimal = 	0x4A,
	SplInputVkeyKeypadAdd = 		0x4B,
	SplInputVkeyKeypadSubtract = 	0x4C,
	SplInputVkeyKeypadMultiply = 	0x4D,
	SplInputVkeyKeypadDivide = 		0x4E,
	SplInputVkeyKeypadEnter = 		0x4F,

	SplInputVkeyF1 =			0x60,
	SplInputVkeyF2 =			0x61,
	SplInputVkeyF3 =			0x62,
	SplInputVkeyF4 =			0x63,
	SplInputVkeyF5 =			0x64,
	SplInputVkeyF6 =			0x65,
	SplInputVkeyF7 =			0x66,
	SplInputVkeyF8 =			0x67,
	SplInputVkeyF9 =			0x68,
	SplInputVkeyF10 =			0x69,
	SplInputVkeyF11 =			0x6A,
	SplInputVkeyF12 =			0x6B,
	SplInputVkeyF13 =			0x6C,
	SplInputVkeyF14 =			0x6D,
	SplInputVkeyF15 =			0x6E,
	SplInputVkeyF16 =			0x6F,
	SplInputVkeyF17 =			0x70,
	SplInputVkeyF18 =			0x71,
	SplInputVkeyF19 =			0x72,
	SplInputVkeyF20 =			0x73,
	SplInputVkeyF21 =			0x74,
	SplInputVkeyF22 =			0x75,
	SplInputVkeyF23 =			0x76,
	SplInputVkeyF24 =			0x77,

	SplInputVkeyEnter =			0x80,
	SplInputVkeyTab =			0x81,
	SplInputVkeySpace =			0x82,
	SplInputVkeyBackspace = 	0x83,
	SplInputVkeyDelete =		0x84,
	SplInputVkeyInsert =		0x85,
	SplInputVkeyEscape =		0x86,
	SplInputVkeyCapsLock =		0x87,
	SplInputVkeyShift =			0x88,
	SplInputVkeyControl =		0x89,
	SplInputVkeyAlt =			0x8A,
	SplInputVkeyRightShift =	0x8B,
	SplInputVkeyRightControl =	0x8C,
	SplInputVkeyRightAlt =		0x8D,
	SplInputVkeyPageUp =		0x8E,
	SplInputVkeyPageDown =		0x8F,
	SplInputVkeyHome =			0x90,
	SplInputVkeyEnd =			0x91,
	SplInputVkeyLeftArrow =		0x92,
	SplInputVkeyRightArrow =	0x93,
	SplInputVkeyDownArrow =		0x94,
	SplInputVkeyUpArrow =		0x95,

	SplInputVkeyLeftWindows =	0xA0,
	SplInputVkeyRightWindows =	0xA1,
	SplInputVkeyApps =		0xA2,

	SplInputVkeyUnknown = 	0xFFFFFFFF
} SplInputVkey;

// Modifier masks
typedef enum {
    SplInputEventModifierCapsLock = 1 << 0,
    SplInputEventModifierShift = 1 << 1,
    SplInputEventModifierControl = 1 << 2,
    SplInputEventModifierAlternateOption = 1 << 3, // Windows = Alt, Mac = Option
    SplInputEventModifierWindowsCommand = 1 << 4, // Windows = Windows, Mac = Command
    SplInputEventModifierApps = 1 << 5 // Apps or Menu key
} SplInputEventModifier;

// input event data
typedef struct _SplInputEvent {
	// Type of event
	SplInputEventClass eventClass;
	SplInputEventAction eventAction;

	// Decoded and normalized data

	// For SplInputEventClass_Mouse
	struct {
		int32_t x;
		int32_t y;
	} mouseEvent;
	// For SplInputEventClass_Keyboard
    struct {
		SplInputVkey keycodeVirtual;
    } keyboardEvent;
    
    // Modifiers, can be used for mouse or keyboard
    // Use SplInputEventModifier enum
    uint32_t modifierState;

	// Original OS specific data, for use in advanced applications
	struct {
#ifdef _WIN32
		int nCode;
		WPARAM wParam;
		LPARAM lParam;
#elif defined (__APPLE__)
        void *event; // NSEvent *, not available here
#endif
	} native;
} SplInputEvent;

typedef void (CALLBACK *SplInputCallback)( void *context, SplInputEvent *event);

// Structure used to track SplInput requests
typedef struct _SplInput {
	SplInputEventClass inputClasses;
	SplInputCallback inputCallbackFunction;
	void *inputCallbackContext;

#ifdef _WIN32
	SplState threadState;
	SplThread *thread;    
	HHOOK mouseHookHandle;
	HHOOK keyboardHookHandle;
#elif defined(__APPLE__)
	BOOL localMonitor;
	void *callbackHandle;
#endif
} SplInput;

// Agnostic types.
typedef struct _SplSemaphore {
#ifdef _WIN32
  HANDLE semaphore;
#elif _LINUX
  sem_t semaphore;
#else
  sem_t *semaphore;
#endif
  SplAtomic maxCount;
  SplAtomic count;
} SplSemaphore;

typedef struct _SplWait {
#ifdef _WIN32
  // The wait queue size
  SplAtomic waitQueue;
  /// The semaphore to wait on.
  HANDLE semaphore;
#else
  /// The posix condition variables implementation requires a Mutex to function, and it must
  /// obey strict locking rules during invocation, so this class manages the mutex itself.
  pthread_mutex_t mutex;
  /// The condition object to wait on.
  pthread_cond_t condition;
#endif
} SplWait;

typedef struct _SplConcurrentQueue {
  SplCritical lock;
  SplSemaphore wait;
  // Dealt with internally as queue type.  Don't expose that here.
  void *queue;
} SplConcurrentQueue;

typedef struct _SplThreadLocal {
#if defined(_WIN32)
  DWORD key;
#else
  pthread_key_t key;
#endif
} SplThreadLocal;


#endif	//__SplTypes_h
