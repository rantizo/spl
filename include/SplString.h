// SplString.h
//

#pragma once

#include "SplTypes.h"
#include "SplExport.h"

#include <string.h>

#if defined(_WIN32)
#	define SplStriCmp(x,y)			_stricmp(x,y)
#	define SplStrniCmp(x,y,n)		_strnicmp(x,y,n)

//*DEPRECATED* Renamed to above
#	define SplStrCaseCmp(x,y)		_stricmp(x,y)
#	define SplStrnCaseCmp(x,y,n)	_strnicmp(x,y,n)

#elif defined(_LINUX) || defined(__APPLE__) 
#	define SplStriCmp(x,y)			strcasecmp(x,y)
#	define SplStrniCmp(x,y,n)		strncasecmp(x,y,n)

//*DEPRECATED* Renamed to above
#	define SplStrCaseCmp(x,y)		strcasecmp(x,y)
#	define SplStrnCaseCmp(x,y,n)	strncasecmp(x,y,n)

#elif defined(ESP_PLATFORM)
#	include "wchar.h"

#	define SplStriCmp(x,y)			strcasecmp(x,y)
#	define SplStrniCmp(x,y,n)		strncasecmp(x,y,n)

//*DEPRECATED* Renamed to above
#	define SplStrCaseCmp(x,y)		strcasecmp(x,y)
#	define SplStrnCaseCmp(x,y,n)	strncasecmp(x,y,n)

#endif


#if 0
// Compare - specify length of first string
SOEXPORT
int SplnStrCmp(const char *_pszStr1,size_t _cStr1, const char *_pszStr2);

// Compare - ignore case and specify length of first string
SOEXPORT
int SplnStriCmp(const char *_pszStr1,size_t _cStr1, const char *_pszStr2);

// Compare - specify length of both strings
SOEXPORT
int SplnStrnCmp(const char *_pszStr1,size_t _cStr1, const char *_pszStr2,size_t _cStr2);

// Compare - ignore case and specify length of both strings
SOEXPORT
int SplnStrniCmp(const char *_pszStr1,size_t _cStr1, const char *_pszStr2,size_t _cStr2);

// Find char in string specify length of string
SOEXPORT
char *SplnStrChr(const char *_pszStr1,size_t _cStr1, char _chr);
#endif


// Find string specifying length of second string
SOEXPORT
char *SplStrnStr(const char *_pszStr,const char *_pszPat,  size_t _cStr);

// Find string specifying length of first string
SOEXPORT
char *SplStrStrn(const char *_pszStr, size_t _cStr, const char *_pszPat);

// Find string specifying length of both strings
SOEXPORT
char *SplStrnStrn(const char *_pszStr, size_t _cStr, const char *_pszPat, size_t _cPat);

SOEXPORT
size_t SplItoA(long _nVal, char *_pszDst, int _nBase);


#if 0
// Not good - too easy to forget to use uppercase name
// Nothing indicates these came from SPL
#if defined(_WIN32)
#define STRNCASECMP(x,y,s) _strnicmp(x,y,s)
#else
#define STRNCASECMP(x,y,s) strncasecmp(x,y,s)
#endif

#if defined(_WIN32)
#define STRCASECMP(x,y) _stricmp(x,y)
#else
#define STRCASECMP(x,y) strcasecmp(x,y)
#endif
#endif
