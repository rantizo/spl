// SplCoords.h
//
// Spatial functions which operate on SplPoint, SplSize, SplRect


#pragma once

#ifndef __SplCoords_H
#define __SplCoords_H

#include "SplTypes.h"
#include "SplDll.h"
#include "SplExport.h"

// Coordinates
//
#if defined(__APPLE__) || defined(_LINUX) || defined(_ESP32)
typedef struct _SplPoint {
	int32_t x;
	int32_t y;
} SplPoint;

typedef struct _SplSize {
	int32_t cx;
	int32_t cy;
} SplSize;

typedef struct _SplRect {
	int32_t    left;
	int32_t    top;
	int32_t    right;
	int32_t    bottom;
} SplRect;

#elif defined(_WIN32)
#define SplPoint	POINT
#define _SplPoint	tagPOINT
#define SplSize		SIZE
#define _SplSize	tagSIZE
#define SplRect		RECT
#define _SplRect	tagRECT

#endif


#if defined(_LINUX) || defined(__APPLE__) || defined(_ESP32)
SOEXPORT 
BOOL SplCopyRect(SplRect *_prcDst,const SplRect *_prcSrc);

SOEXPORT 
BOOL SplEqualRect(const SplRect *_prc1,const SplRect *_prc2);

SOEXPORT 
BOOL SplInflateRect(SplRect *_prc,int dx,int dy);

SOEXPORT 
BOOL SplIntersectRect(SplRect *_prcDst, const SplRect *_prcSrc1,const SplRect *_prcSrc2);

SOEXPORT 
BOOL SplIsRectEmpty(const SplRect *_prc);

SOEXPORT 
int  SplMulDiv(int nNumber,int nNumerator,int nDenominator);

SOEXPORT 
BOOL SplOffsetRect(SplRect *_prc,int dx,int dy);

SOEXPORT 
BOOL SplPtInRect(const SplRect *_prc,SplPoint pt);

SOEXPORT 
BOOL SplSetRect(SplRect *_prc,int xLeft,int yTop,int xRight,int yBottom);

SOEXPORT 
BOOL SplSetRectEmpty(SplRect *_prc);

SOEXPORT 
BOOL SplSubtractRect(SplRect *_prcDst,const SplRect *_prcSrc1,const SplRect *_prcSrc2);

SOEXPORT 
BOOL SplUnionRect(SplRect *_prcDst,const SplRect *_prcSrc1,const SplRect *_prcSrc2);

#elif defined(_WIN32)
// Uses Windows functions
#define SplCopyRect			CopyRect
#define SplEqualRect		EqualRect
#define SplInflateRect		InflateRect
#define SplIntersectRect	IntersectRect
#define SplIsRectEmpty		IsRectEmpty
#define SplMulDiv			MulDiv
#define SplOffsetRect		OffsetRect
#define SplPtInRect			PtInRect
#define SplSetRect			SetRect
#define SplSetRectEmpty		SetRectEmpty
#define SplSubtractRect		SubtractRect
#define SplUnionRect		UnionRect	

#endif	

#endif	// __SplCoords_H


