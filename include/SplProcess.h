//
// SplProcess.h
//

#pragma once
#include "SplTypes.h"
#include "SplExport.h"
#include "SplErrors.h"

#ifdef __cplusplus
extern "C" {
#endif 
	
/*********************************/
/************* Linux *************/
/*********** Mac OS X ************/
/*********************************/
// RCB I believe this works on Mac
#if defined(_LINUX) || defined(__APPLE__)

enum SplProcessClass
{
	SplProcessClassNormal,
	SplProcessClassIdle,
	SplProcessClassHigh,
	SplProcessClassRealtime
};

#endif // _LINUX

/***********************************/
/************* Windows *************/
/***********************************/
#if defined(_WIN32)

enum SplProcessClass
{
	SplProcessClassNormal = NORMAL_PRIORITY_CLASS,
	SplProcessClassIdle = IDLE_PRIORITY_CLASS,
	SplProcessClassHigh = HIGH_PRIORITY_CLASS,
	SplProcessClassRealtime = REALTIME_PRIORITY_CLASS
};

#endif // _WIN32

// Callback when a child process has been forked (used to handle cleanup
// of pipes, etc)
typedef void(CALLBACK *SplChildForkedCallback)(void *context);

// Enum of available setup types
// Can specify multiple as bitmap
// These will be called in this order (retain FDs, then callback, etc)
typedef enum
{
	SplProcessSetupInfoUndefined = 0,
	SplProcessSetupInfoRetainFDs = 0x01, // Call function to retain specified FDs
	SplProcessSetupInfoCallback = 0x02 // Callback after fork and before exec
} SplProcessSetupInfoType;

// Structure with possible information to process
typedef struct SplProcessSetupInfoStruct
{
	// Bitmap of SplProcessSetupInfoType enums
	int process_setup_purpose;

	// For SplProcessSetupInfoRetainFDs
#if defined(_LINUX) || defined(__APPLE__)
	int keep_fd1;
	int keep_fd2;
#endif

	// For SplProcessSetupInfoCallback
	SplChildForkedCallback cbChildForked;
	void *pChildForkedContext;
	
} SplProcessSetupInfoStruct;


SOEXPORT
SplErrorCode SplProcessCreate( SplProcess **process,
							const char *cmd_line,
							...);
#if 0
SOEXPORT
SplErrorCode SplProcessWithIpcPipeCreate( SplProcess **process,
							SplIpcPipe **ipc_pipe,
							const char *cmd_line,
							... );
#endif

SOEXPORT
SplErrorCode SplProcessCreateWithSetupInfo( SplProcess **process,
							SplProcessSetupInfoStruct *setup_info,
							const char *cmd_line,
							... );
#if 0
SOEXPORT
SplErrorCode SplProcessIpcPipeChildOpen( SplIpcPipe **ppIpcPipe, int *argc, char **argv);
#endif

SOEXPORT
SplProcess * SplProcessGetCurrent();

SOEXPORT
int SplProcessIsSelf(SplProcess *pProcess);

SOEXPORT
SplErrorCode SplProcessJoin( SplProcess *pProcess );

#define SplProcessJoinExNone    0x00000000
#define SplProcessJoinExNoHang  0x00000001

SOEXPORT
SplResult SplProcessJoinEx( SplProcess *pProcess, uint32_t nOptions);

#if defined(_LINUX) || defined(__APPLE__)
SOEXPORT
SplErrorCode SplProcessTerminateWithSignal( SplProcess *pProcess, uint32_t nSignal );
#endif

SOEXPORT
SplErrorCode SplProcessTerminate( SplProcess *pProcess );

SOEXPORT
void SplProcessExit(int exit_code);

SOEXPORT
SplErrorCode SplProcessRelease( SplProcess *pProcess );

SOEXPORT
SplErrorCode SplProcessSetClass( enum SplProcessClass pclass );
	
#ifdef __cplusplus
}
#endif 
